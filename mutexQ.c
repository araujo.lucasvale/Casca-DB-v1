#include <stdio.h>
#include <stdlib.h>
#include <sys/queue.h>
#include <pthread.h>

TAILQ_HEAD(tailhead, entry) head;

struct mutex_node {
  mutex_t c;
  TAILQ_ENTRY(mutex_node) entries;
};

void add_to_queue(char ch) {
  struct entry *elem;
  elem = malloc(sizeof(struct entry));
  if (elem) {
    elem->c = ch;
  }
  TAILQ_INSERT_HEAD(&head, elem, entries);
}

int main(int argc, char *argv[]) {
  char ch = 'A';
  int i;
  struct entry *elem;

  TAILQ_INIT(&head);
  for (i=0; i<4; i++) {
    add_to_queue(ch);
    ch++;
    add_to_queue(ch);

    elem = head.tqh_first;
    TAILQ_REMOVE(&head, head.tqh_first, entries);
    free(elem);
  }
  exit(0);
}

GET /datastores/inmetro_db/ HTTP/1.1
Accept: */*
Authorization: ApiKey 0131Byd7N220T32qp088kIT53ryT113i
Content-Type: application/json


curl -H "Authorization: ApiKey 53KHDb880G8JIlr43f1LDi3Lar6Vy4Ay" -X GET https://storage.securecloud.works:5002/datastores/consumption_test_tud/



CREATE TABLE `phasor` (id bigint(20) NOT NULL AUTO_INCREMENT,customer_id bigint(20) NOT NULL,  `date` date DEFAULT NULL,  `time` time(4) DEFAULT NULL,  `wday` varchar(3) DEFAULT NULL, `volt_A` decimal(10,2) DEFAULT NULL,  `volt_B` decimal(10,2) DEFAULT NULL,  `volt_C` decimal(10,2) DEFAULT NULL,  `volt_angle_A` decimal(10,2) DEFAULT NULL,
  `volt_angle_B` decimal(10,2) DEFAULT NULL,  `volt_angle_C` decimal(10,2) DEFAULT NULL,  `curr_A` decimal(8,2) DEFAULT NULL,  `curr_B` decimal(8,2) DEFAULT NULL,
  `curr_C` decimal(8,2) DEFAULT NULL,  `curr_angle_A` decimal(8,2) DEFAULT NULL,  `curr_angle_B` decimal(8,2) DEFAULT NULL,  `curr_angle_C` decimal(8,2) DEFAULT NULL,
  `power_A` decimal(10,2) DEFAULT NULL,  `power_B` decimal(10,2) DEFAULT NULL,  `power_C` decimal(10,2) DEFAULT NULL,  `power_fact_A` decimal(10,2) DEFAULT NULL,
  `power_fact_B` decimal(10,2) DEFAULT NULL,  `power_fact_C` decimal(10,2) DEFAULT NULL,  `volt_AB` decimal(10,2) DEFAULT NULL,  `volt_BC` decimal(10,2) DEFAULT NULL,
  `volt_AC` decimal(10,2) DEFAULT NULL,  `harm_dist` decimal(8,2) DEFAULT NULL,  `freq` decimal(8,2) DEFAULT NULL,  `seq` int(11) DEFAULT NULL,  `demand_1` decimal(10,2) DEFAULT NULL,
  `demand_2` decimal(10,2) DEFAULT NULL,  `demand_3` decimal(10,2) DEFAULT NULL,  `totalizer_1` decimal(15,2) DEFAULT NULL,  `totalizer_2` decimal(15,2) DEFAULT NULL,
  `totalizer_3` decimal(15,2) DEFAULT NULL,  `checkpoint` tinyint(1) DEFAULT NULL,  `inmetro_data_signed` varchar(172) DEFAULT NULL,
  PRIMARY KEY (id)
) ;



insert into phasor(customer_id,   date, time,                 volt_A, volt_B, volt_C,                 volt_AB, volt_BC, volt_AC,                curr_A, curr_B, curr_C,                power_A, power_B, power_C,                power_fact_A, power_fact_B, power_fact_C,                 volt_angle_A, volt_angle_B, volt_angle_C,                totalizer_1, totalizer_2, totalizer_3,                demand_1, demand_2, demand_3, inmetro_data_signed                 )  values (5000003,'2018-01-31','12:58:34',127.028038025,126.481811523,126.962104797,126.364677429,127.094215393,126.519592285,2.22842335701,0.740755021572,0.56471323967,190.550003052,190.550003052,190.550003052,0.00999999977648,0.00999999977648,0.00999999977648,0.00999999977648,0.00999999977648,0.00999999977648,179.00,45825.00,11521.00,74.00,14337.00,22017.00,'B/dBp86bhV1+vUnZKxjGDurxQk472mcD35ppbv9/4tFeJ1jP5k8okQ7mAyBivDVgNZaRC3a6Ih0grkLbfFBH+CxfO/GJgBb9Mm/7osMexNIoRvzuKCphTuiFrNRIc+nhJQXYNAmJdtr2iOyAYzw+9xPid9l9KHDhKGyp47uDTAQ=') ;
