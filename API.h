/**
 * @file API.h
 * @author Pedro
 * @date 8 Aug 2017
 * @brief Contains all API functions.
 *
 */

#include "socket.h"
#include <stdlib.h>
#include <pthread.h>
#include <semaphore.h>
#include "data_structs.h"
#include "threads.h"
#include "option.h"
#include <stdio.h>
#include "commands.h"
#include <syslog.h>
#include "SQLcallbacks.h"
#include "global_vars.h"

/**
 * @brief Function to initialize the Shell.
 *
 * Initializes all variables necessary to get Shell working based on the values in define.h header and some arguments passed by argv.
 *
 * \param argc integer that have the amount of arguments in argv.
 * \param argv array of strings that represents the arguments passed in main function.
 * \return Always return NULL.
 *
 * \b Code \b examples
 * \code
 * void main(int argc , char* argv[])
 * {
 *     API_Init(argc, argv);
 *
 *    //use API functions here
 *
 *     API_shutdown();
 *     return;
 * }
 * \endcode
*/
void API_Init(config_struct_t config);

/**
 * @brief Function to create a new server socket.
 *
 * Create the socket and put it on Shell internal list of server sockets, that is scanned periodically for new connections.
 *
 * \param port Integer with value of the port to be listened.
 * \param *send_done_func Pointer to a callback function wich is triggred when a message is sucessfully sent by a socket created by this server socket.
 * \param *process_func Pointer to a callback function wich is triggred when a message is recieved by a socket created by this server socket. The message itself and its size are passed as an argument.
 * \return A pointer to the info_socket_t created.
 * \return NULL if something goes wrong.
 *
 *\b Code \b examples
 * \code
 * void main(int argc , char* argv[])
 * {
 *     API_Init(argc, argv);
 *
 *     API_create_server(8181, &my_send_done_func, &my_process_func);
 *
 *     API_shutdown();
 *
 *     return;
 * }
 *
 * void my_send_done_func ()
 * {
 * 		printf("Message sent!\n");
 *
 * }
 *
 * long my_process_func (void* buffer, int size)
 * {
 *		if(size > 0)
 *		{
 *			printf("Message recieved is:  %s\n",(char *)buffer);
 *
 *			return 0; // message is complete
 *		}
 *		else
 *		{
 *			return -1; // message still incomplete
 *		}
 *
 * }
 *
 * \endcode
 * @warning If \b send_done_func or \b process_func are NULL, default functions will be used instead. This functions are just for DEBUGGING and should not be modified. The following code works with these default functions:
 *
 * \code
 * void main(int argc , char* argv[])
 * {
 *     API_Init(argc, argv);
 *
 *     API_create_server(8181, NULL, NULL);
 *
 *     API_shutdown();
 *
 *     return;
 * }
 * \endcode
*/
info_socket_t* API_create_server(int port, send_done_func_t *send_done_func, process_func_t process_func);

/**
 * @brief Function to create a new IO socket.
 *
 * Create the socket and put it on Shell internal list of IO sockets, that once connected calls \b connected_func and begin to be scanned periodically for recieved messages.

 * \param address string with the address to connect the socket.
 * \param port Stores the port to connect with in the address.
 * \param *send_done_func Pointer to a callback function wich is triggred when a message is sucessfully sent by this socket.
 * \param *process_func Pointer to a callback function wich is triggred when a message is recieved by this socket. The message itself and its size are passed as an argument.
 * \param *connected_func Pointer to a callback function wich is triggred when the socket connects with the adress and port especified.
 *
 * \return A pointer to the info_socket_t created.
 * \return NULL if something goes wrong.
 *
 * \b Code \b examples
 * \code
 *
 * bool connected;
 *
 * void main(int argc , char* argv[])
 * {
 *		API_Init(argc, argv);
 *
 *		API_create_socket("127.0.0.1",7272,&my_send_done_func,&my_process_func,&my_connected_func);
 *
 *		while(!connected){}
 *
 *		//Use socket here
 *
 *		API_shutdown();
 *
 *		return;
 * }
 *
 * void my_connected_func ()
 * {
 *		printf("Your socket is already connected\n");
 *		connected = true;
 *
 * }
 *
 * void my_send_done_func ()
 * {
 * 		printf("Message sent!\n");
 *
 * }
 *
 * long my_process_func (void* buffer, int size)
 * {
 *		if(size > 0)
 *		{
 *			printf("Message recieved is:  %s\n",(char *)buffer);
 *
 *			return 0; // message is complete
 *		}
 *		else
 *		{
 *			return -1; // message still incomplete
 *		}
 *
 * }
 *
 * \endcode
 * @warning If \b send_done_func,\b process_func or \b connected_func are NULL, default functions will be used instead. This functions are just for DEBUGGING and should not be modified. The following code works with these default functions:
 *
 * \code
 * void main(int argc , char* argv[])
 * {
 *     API_Init(argc, argv);
 *
 *     API_create_socket("127.0.0.1",7272,NULL,NULL,NULL);
 *
 *     API_shutdown();
 *
 *     return;
 * }
 * \endcode
*/
info_socket_t* API_create_socket(char*  address, int port,send_done_func_t* send_done_func, process_func_t* process_func, send_done_func_t* connected);

/**
 * @brief Function to write a message to a already connected info_socket_t.
 *
 * Writes the message to the socket buffer. when done, call send_done_func of socket passed.
 * \param socket A info_socket_t pointer to the socket which the message will be sent by.
 * \param *buffer pointer to the message which will be sent.
 * \param buffersize Size in bytes of the message pointed by buffer.
 * \return \b 0  On sucess.
 * \return \b -1 When something goes wrong.
 *
 * \b Code \b examples
 * \code
 * void main(int argc , char* argv[])
 * {
 *		API_Init(argc, argv);
 *
 *		info_socket_t iSocket = API_create_socket("127.0.0.1",7272,NULL,NULL,NULL);
 *
 *		API_write(iSocket,"Hello World",11);
 *
 *		API_shutdown();
 *
 *		return;
 * }
 * \endcode
*/
int API_write(info_socket_t* socket,void* buffer,long int buffersize,send_done_func_t send_done_func);

/**
 * @brief Function to stop all communications related to the info_socket_t passed.
 *
 * When this function is called, the socket no longer can recieve messages and will only finish sending the already queued messeges in buffer.
 * \param socket A info_socket_t pointer to the socket which will be closed.
 * \return \b 0  On sucess.
 * \return \b -1 When something goes wrong.
*/

//info_socket_t* API_create_server_TLS(int port, send_done_func_t *send_done_func, process_func_t *process_func);
//info_socket_t* API_create_socket_TLS(char*  address, int port,send_done_func_t* send_done_func, process_func_t* process_func, send_done_func_t* connected);

#ifdef TLS
SSL_CTX *API_create_context(char type);
void API_configure_context(SSL_CTX *ctx, char type);
info_socket_t* API_create_socket_TLS(char*  address,int porta,send_done_func_t* send_done_func, process_func_t* process_func,task_func* connected);
info_socket_t* API_create_server_TLS(int port, send_done_func_t* send_done_func, process_func_t process_func);
#endif

int API_close_socket(info_socket_t* socket);
/**
 * @brief Function to stop all communications of all sockets.
 *
 * When this function is called, all sockets no longer can recieve messages and will only finish sending the already queued messeges in their buffers.
 * \return \b  0  On sucess.
 * \return \b -1 When something goes wrong.
*/
int API_shutdown();


info_socket_t* API_create_SQLserver (int port);


void API_create_dummy_task();
