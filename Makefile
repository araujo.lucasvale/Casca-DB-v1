CC := gcc

NO_TLS := -lpthread -pthread -g -I./SQL/src -I./SQL/src/parser -lm -pg -DDEBUG -w

SCONE := -lpthread -pthread -g -I./SQL/src -I./SQL/src/parser -lm -w

TLS := -lpthread -pthread -g -I./SQL/src -I./SQL/src/parser -lm -pg -DTLS -DDEBUG -w -DSECURE_SQL -I./TLS -lssl -lcrypto

SQL_FILES := ./SQL/src/rest.c ./SQL/src/request.c ./SQL/src/sql_select.c ./SQL/src/sql_insert.c ./SQL/src/sql_query.c ./SQL/src/sql_create.c ./SQL/src/relational.c \
			 ./SQL/src/dic_dados.c ./SQL/src/db.c ./SQL/src/config.c ./SQL/src/my_lib.c ./SQL/src/results.c ./SQL/src/consulta_sql.c ./SQL/src/json_parser.c ./SQL/src/csv_parser.c \
			 ./SQL/lib/cJSON/cJSON.c

CASCA_FILES := global_data.c threads.c queue.c option.c socket.c commands.c API.c SQLcallbacks.c

FILES := $(CASCA_FILES)
FILES += $(SQL_FILES)

OUTPUT_NAME :=  casca
OUTPUT_NAME_TLS := casca_tls

all:
	$(CC)  $(FILES) $(NO_TLS) -o $(OUTPUT_NAME)
tls:
	$(CC)  $(FILES) $(TLS) -o $(OUTPUT_NAME_TLS)
scone:
	$(CC) $(FILES) $(SCONE) -o $(OUTPUT_NAME)
clean:
	rm $(OUTPUT_NAME) $(OUTPUT_NAME_TLS)
