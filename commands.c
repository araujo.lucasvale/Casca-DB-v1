#include "commands.h"


void cmd_dummy (info_cmd_t* current_task)
{
    sleep(1);
}



void cmd_read (info_cmd_t* current_task)
{ 
    current_task->socket->inRead = true;
    #ifdef DEBUG
	syslog(LOG_DEBUG, "[%lu]cmd_read() \n",pthread_self()%100);
    #endif
	int n;
	
    char buffer[MAX_READ_BYTES];

	bzero(buffer,MAX_READ_BYTES);
   

    #ifdef DEBUG
	syslog(LOG_DEBUG, "[%lu]cmd_read: Reading from socket %d \n",pthread_self()%100,current_task->socket->socket);
    #endif
	n = read(current_task->socket->socket,buffer,MAX_READ_BYTES);
 
    printf("buffer recebido:\n%s\n",buffer);

    #ifdef DEBUG
	syslog(LOG_DEBUG, "[%lu]cmd_read: Socket %lu reading completed, buffer='%s', n = %d\n",pthread_self()%100,current_task->socket->socket,(char*)buffer,n);
    #endif
	if (n < 0)
	{ 		
		perror("ERROR reading from socket:\n");
        #ifdef DEBUG
		syslog(LOG_ERR, "[%lu]cmd_read: Reading error, please refer to stderr for further details\n",pthread_self()%100);
        #endif

	}
     if((int)buffer[0] == 0&& n == 0)
    {
       
        #ifdef DEBUG
        syslog(LOG_ERR, "[%lu]cmd_read: Connection shuted down by other side, shutting down socket\n",pthread_self()%100);
        #endif
       
        current_task->socket->socket_data_input.process(current_task->socket,current_task->socket->socket_data_input.buffer,-1);
        current_task->socket->shutdown = 1;
        //free(current_task->socket->socket_data_input.buffer);
        current_task->socket->socket_data_input.buffer_size =0;
        // current_task->socket->inRead = false;
         return;
    }  

	
 
    //n = n-1;
    char* newBuffer=(char*) malloc(n+10*sizeof(char));
    //newBuffer[n+1] = '\0';

  
    memcpy (newBuffer,buffer,n);
   
    int buffSize = current_task->socket->socket_data_input.buffer_size;
    if(buffSize>0)
    {
   
        current_task->socket->socket_data_input.buffer =   realloc (current_task->socket->socket_data_input.buffer, (buffSize+n+2) * sizeof(char));
        //strcat(current_task->socket->socket_data_input.buffer,newBuffer);
        memcpy(&(current_task->socket->socket_data_input.buffer[buffSize]),newBuffer,n+1);
      
        
    }else
    {
       
        current_task->socket->socket_data_input.buffer = calloc(n+2,sizeof(char));
        memcpy(current_task->socket->socket_data_input.buffer,newBuffer,n+2);
        char* buf = current_task->socket->socket_data_input.buffer;
        buf[n]='\0';
        current_task->socket->socket_data_input.buffer_size=0;

    }
    current_task->socket->socket_data_input.buffer_size += n;
   
    free(newBuffer);
  
    #ifdef DEBUG
    syslog(LOG_DEBUG, "[%lu]cmd_read: Callback\n",pthread_self()%100);
    syslog(LOG_INFO, "%d bytes received from ip %s\n",current_task->socket->socket_data_input.buffer_size,inet_ntoa(current_task->socket->info.sin_addr));
    #endif

  
   
    int res = current_task->socket->socket_data_input.process(current_task->socket,current_task->socket->socket_data_input.buffer,current_task->socket->socket_data_input.buffer_size);

    while(res>0)
    {

        syslog(LOG_INFO, "%lu bytes recieved from ip %s are a complete message\n",current_task->socket->socket_data_input.buffer_size,inet_ntoa(current_task->socket->info.sin_addr));
        
        char* string = current_task->socket->socket_data_input.buffer;
        int newstringsize = strlen(string)-res;

        void * aux = calloc(newstringsize+2,1);

        memcpy(aux,&(current_task->socket->socket_data_input.buffer[res]),newstringsize);
        current_task->socket->socket_data_input.buffer = aux;



        //free(current_task->socket->socket_data_input.buffer);   

        current_task->socket->socket_data_input.buffer_size =strlen((char *)current_task->socket->socket_data_input.buffer);
       // printf("buffer depois do recorte: %s\n",current_task->socket->socket_data_input.buffer );
    
        res = current_task->socket->socket_data_input.process(current_task->socket,current_task->socket->socket_data_input.buffer,current_task->socket->socket_data_input.buffer_size);
        
        
        
       
    }
    if(res == 0)
    {
        syslog(LOG_INFO, "%lu bytes recieved from ip %s do not complete the message\n",current_task->socket->socket_data_input.buffer_size,inet_ntoa(current_task->socket->info.sin_addr) );
        
    }
    if(res == -1)
    {
        syslog(LOG_INFO, "The message of %lu bytes recieved from ip %s will be discarded\n",current_task->socket->socket_data_input.buffer_size,inet_ntoa(current_task->socket->info.sin_addr) );
        free(current_task->socket->socket_data_input.buffer);
        current_task->socket->socket_data_input.buffer_size = 0;
    }
    //printf("socket %d inread false\n",current_task->socket->socket);





    current_task->socket->inRead = false;
}

void cmd_accept (info_cmd_t* current_task)
{
    current_task->socket->select_ready =false;
    #ifdef DEBUG
    syslog(LOG_DEBUG, "[%lu]cmd_accept() \n",pthread_self()%100);
    #endif
    info_socket_t *master_info_socket = current_task->socket;

    struct sockaddr_in address = master_info_socket->info;
    int addrlen = sizeof(address);

    socket_t socket_to_accept = master_info_socket->socket;
    #ifdef DEBUG
    syslog(LOG_DEBUG, "[%lu]cmd_accept: Performing connection \n",pthread_self()%100);
    #endif
    int new_socket = accept(socket_to_accept, (struct sockaddr *)&address, (socklen_t*)&addrlen);

    if (new_socket < 0)
    {
        #ifdef DEBUG
      syslog(LOG_ERR, "[%lu]cmd_accept: Couldn't connect, removing socket  \n",pthread_self()%100);
        #endif
      shutdown(new_socket, SHUT_RDWR);

      return ;      
  }
    #ifdef DEBUG
  syslog(LOG_DEBUG, "[%lu]cmd_accept: New connection fd = %d , ip is: %s ,port : %d \n" ,pthread_self()%100,new_socket , inet_ntoa(address.sin_addr) , ntohs(address.sin_port));
    #endif
  syslog(LOG_INFO, "New connction on socket %d: %s\n" ,new_socket , inet_ntoa(address.sin_addr));
  accept_socket_data* data = (accept_socket_data*)current_task->data_slot;
  pthread_mutex_t *IOvec_mutex = data->IOvec_mutex;
  info_socket_t*  IOvec = data->IOvec;
    #ifdef DEBUG
  syslog(LOG_DEBUG, "[%lu]cmd_accept: Waiting access to IO vector \n",pthread_self()%100);
    #endif
  pthread_mutex_lock(IOvec_mutex);
    #ifdef DEBUG
  syslog(LOG_DEBUG, "[%lu]cmd_accept: Acess granted \n",pthread_self()%100);
    #endif

    //Arrumar para caso não haja slots
  for (int i = 0; i < MAX_IO_SOCKETS; ++i)
  {
      if(IOvec[i].socket==0)
      {
            #ifdef DEBUG
         syslog(LOG_DEBUG, "[%lu]cmd_accept: IO slot found \n",pthread_self()%100);
            #endif

         if(master_info_socket->type == SQL_SERVER)
         {
            IOvec[i].type = SQL_IO;
        }
        else
        {

            IOvec[i].type = IO;
        }


        IOvec[i].socket= new_socket;
        IOvec[i].socket_data_input.process = master_info_socket->socket_data_input.process;
        IOvec[i].inRead = false;
        IOvec[i].inWrite = false;
        IOvec[i].select_ready = true;
        IOvec[i].socket_data_output.message_queue =NULL;
        IOvec[i].info = address;
        IOvec[i].shutdown = 0; 
        IOvec[i].isConnected = true;
        pthread_mutex_unlock(IOvec_mutex);
        current_task->socket->select_ready =true;
            #ifdef DEBUG
        syslog(LOG_DEBUG, "[%lu]cmd_accept: Command executed with success \n",pthread_self()%100);
            #endif

        return ;
    }

}
pthread_mutex_unlock(IOvec_mutex);
    #ifdef DEBUG
syslog(LOG_DEBUG, "[%lu]cmd_accept: Couldn't find a IO slot\n",pthread_self()%100);
    #endif
shutdown(new_socket, SHUT_RDWR);
return ;
}


void cmd_write(info_cmd_t* current_task) 
{
    long msg_size =current_task->socket->socket_data_output.message_queue->buffer_size;
     #ifdef DEBUG
    syslog(LOG_DEBUG, "[%lu]cmd_write()\n",pthread_self()%100);
    #endif

     #ifdef DEBUG
    syslog(LOG_DEBUG, "[%lu]cmd_write: Writing '%s' on socket %d size = %lu\n",pthread_self()%100,(char *)current_task->socket->socket_data_output.message_queue->buffer,current_task->socket->socket,msg_size);
    #endif
    long sent = write(current_task->socket->socket ,(char *)current_task->socket->socket_data_output.message_queue->buffer , msg_size ) ;
    
    
    if( sent <= 0)
    {

        return ;
    }
    if(sent  == msg_size)
    {
         #ifdef DEBUG
        syslog(LOG_DEBUG, "[%lu]cmd_write: The message was successfully sent\n",pthread_self()%100);
        #endif
        syslog(LOG_INFO, "%lu bytes were sent to ip %s\n", msg_size,inet_ntoa(current_task->socket->info.sin_addr));
        current_task->socket->socket_data_output.message_queue->send_done();

        


        message_queue_t* message  = queue_remove( (queue_t **)&current_task->socket->socket_data_output.message_queue, (queue_t *)current_task->socket->socket_data_output.message_queue);
        //free(message->send_done);
        free(message->buffer);
        free(message);

        current_task->socket->inWrite = false;

        return;
    }
    else// otimizar futuramente
    {
         #ifdef DEBUG
        syslog(LOG_DEBUG, "[%lu]cmd_write: %lu bytes were sent\n",pthread_self()%100, sent);
        #endif
        char * aux = (char *)current_task->socket->socket_data_output.message_queue->buffer;

        memcpy(current_task->socket->socket_data_output.message_queue->buffer,&aux[sent],msg_size-sent );
        current_task->socket->socket_data_output.message_queue->buffer_size= msg_size-sent;

        current_task->socket->inWrite = false;
        return;
    }
}


//conecta com o endereço e port definidos na info do info socket
void cmd_connect(info_cmd_t* current_task){
    #ifdef DEBUG
	syslog(LOG_DEBUG, "[%lu]cmd_connect()\n",pthread_self()%100);
    #endif






	struct sockaddr_in server = current_task->socket->info;

    //printf("tentando conexão com ip =%d na porta = %d fd = %d  size do serv = %d\n",server.sin_addr.s_addr,server.sin_port,current_task->socket->socket ,sizeof(server));
    #ifdef DEBUG
	syslog(LOG_DEBUG, "[%lu]cmd_connect:Trying to connect\n",pthread_self()%100);
    #endif
   // printf("fd = %d\n", current_task->socket->socket);
	if (connect(current_task->socket->socket , (struct sockaddr *)&server , sizeof(server)) < 0)
    {
        perror("Error");
        #ifdef DEBUG
        syslog(LOG_ERR, "[%lu]cmd_connect: Couldn't connect, removing socket  \n",pthread_self()%100);
        #endif
        
        current_task->socket->shutdown = 1;
        current_task->socket->isConnected = false;
        return ;
    }	
    current_task->socket->select_ready = true;
    current_task->socket->isConnected = true;
    current_task->connected();
    #ifdef DEBUG
    syslog(LOG_DEBUG, "[%lu]cmd_connect: Connection successful\n",pthread_self()%100);
    #endif

}
#ifdef TLS
    void cmd_accept_tls(info_cmd_t* current_task)
    {
        info_socket_t *master_info_socket = current_task->socket;
        struct sockaddr_in address = master_info_socket->info;
        int addrlen = sizeof(address);
        socket_t socket_to_accept = master_info_socket->socket;

        int new_socket = accept(socket_to_accept, (struct sockaddr *)&address, (socklen_t*)&addrlen);
        if (new_socket < 0)
        {
            shutdown(new_socket, SHUT_RDWR);
            return ;
        }
        if ((master_info_socket->socket_tls = SSL_new(master_info_socket->socket_context)) == NULL) {
            SSL_free(master_info_socket->socket_tls);
            shutdown(new_socket, SHUT_RDWR);
            return;
        }
        SSL_set_fd(master_info_socket->socket_tls, new_socket);
        if(SSL_accept(master_info_socket->socket_tls) == -1) {
            SSL_free(master_info_socket->socket_tls);
            shutdown(new_socket, SHUT_RDWR);
            return;
        }
        accept_socket_data* data = (accept_socket_data*)current_task->data_slot;
        pthread_mutex_t *IOvec_mutex = data->IOvec_mutex;
        info_socket_t*  IOvec = data->IOvec;
        pthread_mutex_lock(IOvec_mutex);
        for (int i = 0; i < MAX_IO_SOCKETS; ++i)
        {
            if(IOvec[i].socket==0)
            {
                IOvec[i].socket= new_socket;
                IOvec[i].socket_data_input.process = master_info_socket->socket_data_input.process;
                IOvec[i].inRead = false;
                IOvec[i].inWrite = false;
                IOvec[i].select_ready = true;
                IOvec[i].socket_data_output.message_queue = NULL;
                 IOvec[i].socket_tls = master_info_socket->socket_tls;
                IOvec[i].type = TLS_IO; 

                IOvec[i].shutdown = 0;
                IOvec[i].isConnected = true;
                pthread_mutex_unlock(IOvec_mutex);
                current_task->socket->select_ready =true;
                return ;
            }

        }
        pthread_mutex_unlock(IOvec_mutex);
        SSL_free(master_info_socket->socket_tls);
        shutdown(new_socket, SHUT_RDWR);
        return ;
    }


    void cmd_read_tls(info_cmd_t* current_task)
    {
        current_task->socket->inRead = true;
        #ifdef DEBUG
        syslog(LOG_DEBUG, "[%lu]cmd_read_tls() \n",pthread_self()%100);
        #endif
        int n;
        char buffer[MAX_READ_BYTES];

        bzero(buffer,MAX_READ_BYTES);
        #ifdef DEBUG
        syslog(LOG_DEBUG, "[%lu]cmd_read_tls:  Reading from socket %d \n",pthread_self()%100,current_task->socket->socket);
        #endif
        n=SSL_read(current_task->socket->socket_tls,buffer,MAX_READ_BYTES);


        #ifdef DEBUG
        syslog(LOG_DEBUG, "[%lu]cmd_read_tls: Socket %lu reading completed, buffer=%s, n = %d\n",pthread_self()%100,current_task->socket->socket,(char*)buffer,n);
        #endif
        if (n < 0)
        { 
            
            perror("ERROR reading from socket:\n");
            #ifdef DEBUG
            syslog(LOG_ERR, "[%lu]cmd_read_tls: Reading error, please refer to stderr for further details\n",pthread_self()%100);
            #endif

        }
         if((int)buffer[0] == 0&& n == 0)
        {
            #ifdef DEBUG
            syslog(LOG_ERR, "[%lu]cmd_read_tls: Connection shuted down by other side, shutting down socket\n",pthread_self()%100);
            #endif
            current_task->socket->socket_data_input.process(current_task->socket,current_task->socket->socket_data_input.buffer,-1);
            current_task->socket->shutdown = 1;
            free(current_task->socket->socket_data_input.buffer);
            current_task->socket->socket_data_input.buffer_size =0;
            // current_task->socket->inRead = false;
             return;
        }  

        

        //n = n-1;
        char* newBuffer=(char*) malloc(n+10*sizeof(char));
        //newBuffer[n+1] = '\0';
        memcpy (newBuffer,buffer,n);
       
        int buffSize = current_task->socket->socket_data_input.buffer_size;
        if(buffSize>0)
        {
            current_task->socket->socket_data_input.buffer =   realloc (current_task->socket->socket_data_input.buffer, (buffSize+n+2) * sizeof(char));
            //strcat(current_task->socket->socket_data_input.buffer,newBuffer);
            memcpy(&(current_task->socket->socket_data_input.buffer[buffSize]),newBuffer,n+1);
            
        }else
        {
            current_task->socket->socket_data_input.buffer = calloc(n+2,sizeof(char));
            memcpy(current_task->socket->socket_data_input.buffer,newBuffer,n+2);
            char* buf = current_task->socket->socket_data_input.buffer;
            buf[n]='\0';
            current_task->socket->socket_data_input.buffer_size=0;

        }
        current_task->socket->socket_data_input.buffer_size += n;
        free(newBuffer);

        #ifdef DEBUG
        syslog(LOG_DEBUG, "[%lu]cmd_read_tls: Callback\n",pthread_self()%100);
        syslog(LOG_INFO, "%lu bytes recieved from ip %s are a complete message\n",current_task->socket->socket_data_input.buffer_size,inet_ntoa(current_task->socket->info.sin_addr));
        #endif

      
        
        int res = current_task->socket->socket_data_input.process(current_task->socket,current_task->socket->socket_data_input.buffer,current_task->socket->socket_data_input.buffer_size);

        while(res>0)
        {

            syslog(LOG_INFO, "%lu bytes recieved from ip %s do not complete the message\n",current_task->socket->socket_data_input.buffer_size,inet_ntoa(current_task->socket->info.sin_addr));
    
            char* string = current_task->socket->socket_data_input.buffer;
            int newstringsize = strlen(string)-res;

            /*
            Debbug if just ignore
            if(newstringsize<0)
            {
                    printf("buffer bugado:\n%s\ntam:\n%d\nstrlen:\n%d\n",current_task->socket,current_task->socket->socket_data_input.buffer,current_task->socket->socket_data_input.buffer_size,strlen(string));
                    printf("res =\n%d\n",res );      
            }*/
            void * aux = calloc(newstringsize+2,1);

            memcpy(aux,&(current_task->socket->socket_data_input.buffer[res]),newstringsize);
            current_task->socket->socket_data_input.buffer = aux;



            //free(current_task->socket->socket_data_input.buffer);   

            current_task->socket->socket_data_input.buffer_size =strlen((char *)current_task->socket->socket_data_input.buffer);
          
        
            res = current_task->socket->socket_data_input.process(current_task->socket,current_task->socket->socket_data_input.buffer,current_task->socket->socket_data_input.buffer_size);
            
            
            
           
        }
        if(res<=0)
        {

             syslog(LOG_INFO, "%lu bytes recieved from ip %s do not complete the message\n",current_task->socket->socket_data_input.buffer_size,inet_ntoa(current_task->socket->info.sin_addr) );
        }
       // printf("socket %d inread false\n",current_task->socket->socket);





        current_task->socket->inRead = false;
    }




    void cmd_write_tls(info_cmd_t* current_task)
    {
 
       long msg_size =current_task->socket->socket_data_output.message_queue->buffer_size;
        #ifdef DEBUG
       syslog(LOG_DEBUG, "[%lu]cmd_write()\n",pthread_self()%100);
        #endif
        #ifdef DEBUG
       syslog(LOG_DEBUG, "[%lu]cmd_write: Writing '%s' on socket %d size = %lu\n",pthread_self()%100,(char *)current_task->socket->socket_data_output.message_queue->buffer,current_task->socket->socket,msg_size);
        #endif
       long sent = SSL_write(current_task->socket->socket_tls ,(char *)current_task->socket->socket_data_output.message_queue->buffer , msg_size ) ;


       if( sent <= 0)
       {

        return ;
    }
    if(sent  == msg_size)
    {
            #ifdef DEBUG
        syslog(LOG_DEBUG, "[%lu]cmd_write: The message was successfully sent\n",pthread_self()%100);
            #endif

        current_task->socket->socket_data_output.message_queue->send_done();


        queue_remove( (queue_t **)&current_task->socket->socket_data_output.message_queue, (queue_t *)current_task->socket->socket_data_output.message_queue);

        current_task->socket->inWrite = false;
        
        return;
    }
        else// otimizar futuramente
        {
            #ifdef DEBUG
            syslog(LOG_DEBUG, "[%lu]cmd_write: %lu bytes were sent\n",pthread_self()%100, sent);
            #endif
            char * aux = (char *)current_task->socket->socket_data_output.message_queue->buffer;

            memcpy(current_task->socket->socket_data_output.message_queue->buffer,&aux[sent],msg_size-sent );
            current_task->socket->socket_data_output.message_queue->buffer_size= msg_size-sent;

            current_task->socket->inWrite = false;
            return;
        }
    }

    void cmd_connect_tls(info_cmd_t* current_task)
    {

        struct sockaddr_in server = current_task->socket->info;
        if (connect(current_task->socket->socket , (struct sockaddr *)&server , sizeof(server)) < 0)
        {
            current_task->socket->shutdown = 1;
            current_task->socket->isConnected = false;
            return ;
        }
        current_task->socket->isConnected = true;
        current_task->socket->socket_tls = SSL_new(current_task->socket->socket_context);
        SSL_set_fd(current_task->socket->socket_tls, current_task->socket->socket);
        if (SSL_connect(current_task->socket->socket_tls) == -1)
        {
            SSL_free(current_task->socket->socket_tls);
            current_task->socket->shutdown = 1;
            current_task->socket->isConnected = false;
            return ;
        }
        //printf("%s\n", SSL_get_cipher(current_task->socket->socket_tls));
        current_task->connected();

        current_task->socket->select_ready = true;

    }




#endif