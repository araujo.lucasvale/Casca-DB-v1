
#include <stdio.h>
#include "data_structs.h"
#include <pthread.h>
#include <semaphore.h>
#include "queue.h"
#include "SQLcallbacks.h"
#include "global_vars.h"
#include <unistd.h>
#include "API.h"

#include "global_vars.h"
//banco
#include "SQL/src/db.h"
#include "SQL/src/csv_parser.h"





void stop_all_SQL()
{
	running =0;


}

void print_elem22 (void *ptr)
{
	sql_req *elem = ptr ;

	if (!elem)
		return ;

	elem->prev ? printf ("%d", elem->outputsocket->socket) : printf ("*") ;
	printf ("<%d>", elem->outputsocket->socket) ;
	elem->next ? printf ("%d", elem->outputsocket->socket) : printf ("*") ;
}

long SQL_create_req(void* isocket,void* msg, long tam)
{
	usleep(100000);
	if (tam == -1)
	{
		return -1;
	}
		
	//regex
	int rv;
	int result;
	regex_t exp; 

	
	//assuring null end
	char* message = calloc(tam+1,1); 
	memcpy(message,msg,tam);
	message[tam-1] = '\0';
	//strtok(message, "\n");
	int cpyPointer = 0;
	for (int i = 0; i < tam+1; ++i)
	{
		
		if(!(message[i] == '\n'||message[i] == '\r'))
		{
			message[cpyPointer] = message[i];
			cpyPointer++;
			
		}

	}
	printf("message:\n%s\n",message );

	//points to start of first sql statement in msg
	char * firstOne = 0;

	//checking for what type of statement exists in msg
	char * ins = strcasestr(message,"INSERT INTO");
	char * sel = strcasestr(message,"SELECT");
	char * cre = strcasestr(message,"CREATE");

	//if none is found...
	if(ins == NULL && sel == NULL && cre == NULL)
	{

		bool discart = false;
		char* aux = msg;
		for(int i = 0; i < tam; i++)
		{
			if(aux[i] == '|')
			{
				discart = true;
				break;
			}
		}
		if(discart || tam >16000)
		{

			return -1;
		}
		return 0;


	}
	else
	{	
		if (ins ==NULL)
		{
			ins =message+tam;
		}
		if (sel ==NULL)
		{
			sel =message+tam;
		}
		if (cre ==NULL)
		{
			cre =message+tam;
		}
		int foundEnd =0;
		if (ins<sel && ins < cre)
		{
			//firstOne is a pointer to the start of statement in message
			firstOne = ins;
			//compile a regex verify it out
			regcomp(&exp, "^([i,I][n,N][s,S][e,E][r,R][t,T])\\s([i,I][n,N][t,T][o,O])\\s(\\S+)\\s\\(.*\\)\\s\\;",REG_EXTENDED|REG_NOSUB);
			//if it has something wrong
			if(regexec (&exp, firstOne, 0, NULL, 0))
			{
				//create a buff to send to outputsocket the problematic part
				char * buff;
				//allocate size for worst case
				buff = calloc(tam+1 , 1);
				//pos is the position of the detected start of statement
				int pos =(int)ins - (int)message; 
				
				int i;
				for (i = 0; i < strlen(message); ++i)
				{
					//if "i" is in the problematic region, print it in "buff" 
					if(i>=pos)
					{
						buff[i-pos] = message[i];
					}
					//if find end of statement marker, finish printing "buff" and send to output socket
					if(message[i]==';')
					{
						foundEnd=1;
						buff[i-pos] = '\0';
						API_write((info_socket_t*)isocket,"\nBad SQL: \"",strlen("\nBad SQL: \""),NULL);
						if(strlen(buff)>0)
						{
							API_write((info_socket_t*)isocket,buff,strlen(buff),NULL);
						}
						API_write((info_socket_t*)isocket,"\"\n",strlen("\"\n"),NULL);
						free(buff);
						regfree(&exp);
						return i+1;
					}
				}
				//if cannot find end of statement marker, inform output socket
				if (foundEnd == 0)
				{
					//API_write((info_socket_t*)isocket,"\nBad SQL: could not find \" ;\" separator\n",strlen("\nBad SQL: could not find \" ;\" separator\n"),NULL);
					free(buff);
					regfree(&exp);
					return 0;
				}
			}
		}
		else if (sel<ins && sel < cre)
		{
			//firstOne is a pointer to the start of statement in message
			firstOne = sel;
			//compile a regex verify it out
			regcomp(&exp, "^([s,S][e,E][l,L][e,E][c,C][t,T])\\s(\\S+)\\s([f,F][r,R][o,O][m,M])\\s(\\S+)(\\s[w,W][h,H][e,E][r,R][e,E]\\s.+)?\\s;",REG_EXTENDED|REG_NOSUB);
			//if it has something wrong
			if(regexec (&exp, firstOne, 0, NULL, 0))
			{
				//create a buff to send to outputsocket the problematic part
				char * buff;
				//allocate size for worst case
				buff = calloc(tam+1 , 1);
				//pos is the position of the detected start of statement
				int pos =(int)sel - (int)message; 
				
				int i;
				for (i = 0; i < strlen(message); ++i)
				{
					
					//if "i" is in the problematic region, print it in "buff" 
					if(i>=pos)
					{
						
						buff[i-pos] = message[i];
						
					}
					//if find end of statement marker, finish printing "buff" and send to output socket
					if(message[i]==';')
					{
						foundEnd=1;
						buff[i-pos] = '\0';
						printf("buff2\n%s\n",buff );
						API_write((info_socket_t*)isocket,"\nBad SQL: \"",strlen("\nBad SQL: \""),NULL);
						if(strlen(buff)>0)
						{
							API_write((info_socket_t*)isocket,buff,strlen(buff),NULL);
						}
						API_write((info_socket_t*)isocket,"\"\n",strlen("\"\n"),NULL);
						free(buff);
						regfree(&exp);
						return i+1;
					}
				}
				//if cannot find end of statement marker, inform output socket
				if (foundEnd == 0)
				{
					//API_write((info_socket_t*)isocket,"\nBad SQL: could not find \" ;\" separator\n",strlen("\nBad SQL: could not find \" ;\" separator\n"),NULL);
					free(buff);
					regfree(&exp);
					return 0;
				}
			}
			
		}
		else if (cre<sel && cre < ins)
		{
			//firstOne is a pointer to the start of statement in message
			firstOne = cre;
			//compile a regex verify it out
			regcomp(&exp, "^([c,C][r,R][e,E][a,A][t,T][e,E])\\s([t,T][a,A][b,B][l,L][e,E])\\s+(\\S+)\\s+\\(.+\\)\\s;",REG_EXTENDED|REG_NOSUB);
			//if it has something wrong
			if(regexec (&exp, firstOne, 0, NULL, 0))
			{
				//create a buff to send to outputsocket the problematic part
				char * buff;
				//allocate size for worst case
				buff = calloc(tam+1 , 1);
				//pos is the position of the detected start of statement
				int pos =(int)cre - (int)message; 
				
				int i;
				for (i = 0; i < strlen(message); ++i)
				{
					//if "i" is in the problematic region, print it in "buff" 
					if(i>=pos)
					{
						buff[i-pos] = message[i];
					}
					//if find end of statement marker, finish printing "buff" and send to output socket
					if(message[i]==';')
					{
						foundEnd=1;
						buff[i-pos] = '\0';
						API_write((info_socket_t*)isocket,"\nBad SQL: \"",strlen("\nBad SQL: \""),NULL);
						if(strlen(buff)>0)
						{
							API_write((info_socket_t*)isocket,buff,strlen(buff),NULL);
						}
						API_write((info_socket_t*)isocket,"\"\n",strlen("\"\n"),NULL);
						free(buff);
						regfree(&exp);
						return i+1;
					}
				}
				//if cannot find end of statement marker, inform output socket
				if (foundEnd == 0)
				{

					//API_write((info_socket_t*)isocket,"\nBad SQL: could not find \" ;\" separator\n",strlen("\nBad SQL: could not find \" ;\" separator\n"),NULL);
					free(buff);
					regfree(&exp);
					return 0;
				}

			}
		}

		int useless = 0;
		useless =  firstOne - message;
		


		
		
		int msgsize = 0;
		for (int i = 1; i < strlen(firstOne); ++i)
		{
			if(firstOne[i]==';')
			{
				msgsize = i;
				break;
			}
		}
		

		if(msgsize > 10)
		{
			sql_req* new_req = (sql_req* )calloc(1,sizeof(sql_req));
			new_req->prev =NULL;
			new_req->next =NULL;
			
			
			info_socket_t* current_socket = (info_socket_t*) isocket;
			new_req->outputsocket = current_socket;

			
			new_req->sql_statement = (char*)calloc(msgsize+1,sizeof(char));
			memcpy(new_req->sql_statement, firstOne, msgsize+1);
			new_req->sql_statement[msgsize] = '\0';
			new_req->sql_size = msgsize;
			free(message);

			add_req(new_req);
			//printf("mensagem recortada:\n%s\ntammsg:%d\ntam:%d\n",new_req->sql_statement,strlen(new_req->sql_statement),tam );
			return useless+msgsize;
		}
		bool discart = false;
		char* aux = msg;
		for(int i = 0; i < tam; i++)
		{
			if(aux[i] == '|')
			{
				discart = true;
				break;
			}
		}
		if(discart || tam > 16000)
		{

			return -1;
		}
		return 0;
	


	}
}



int add_req (sql_req* req_to_add )
{

#ifdef DEBUG
	syslog(LOG_DEBUG, "[%lu]add_req()\n",pthread_self()%100);
    #endif
	if(req_to_add == NULL)
	{
		#ifdef DEBUG
		syslog(LOG_ERR, "[%lu]add_req: Null req, aborting operation\n",pthread_self()%100);
        #endif
		return -1;
	}
	if(&req_queue_sem == NULL)
	{
		#ifdef DEBUG
		syslog(LOG_ERR, "[%lu]add_req: Null semaphore, aborting operation\n",pthread_self()%100);
        #endif
		return -1;
	}
	if(&req_queue_mutex == NULL)
	{
		#ifdef DEBUG
		syslog(LOG_ERR, "[%lu]add_req: Null Mutex, aborting operation\n",pthread_self()%100);
        #endif
		return -1;
	}
	pthread_mutex_lock(&req_queue_mutex);
	
	int err = queue_append ((queue_t **) &req_queue, (queue_t*) req_to_add);
	
	if(err<0)
	{
		#ifdef DEBUG
		syslog(LOG_ERR, "[%lu]add_req: Error inserting in queue, aborting operation\n",pthread_self()%100);
        #endif
		return -1;
	}
	sem_post(&req_queue_sem);
	pthread_mutex_unlock(&req_queue_mutex);
	#ifdef DEBUG
	syslog(LOG_DEBUG, "[%lu]add_req: req added successfully\n",pthread_self()%100);
    #endif
	return 0;




}





void* do_req()
{


	int num_reqs;
	#ifdef DEBUG
	syslog(LOG_DEBUG, "[%lu]do_req()\n",pthread_self()%100);
    #endif

	sql_req* current_req = NULL;
	#ifdef DEBUG
	syslog(LOG_DEBUG, "[%lu]do_req: Parameters received\n",pthread_self()%100);
    #endif
	/*Loop de busca e execução de comandos*/
	do
	{
		#ifdef DEBUG
		syslog(LOG_DEBUG, "[%lu]do_req: Waiting for req\n",pthread_self()%100);
        #endif
		sem_wait(&req_queue_sem);

		#ifdef DEBUG
		syslog(LOG_DEBUG, "[%lu]do_req: Req detected in queue\n",pthread_self()%100);
        #endif
		current_req =get_req(current_req);
		
		#ifdef DEBUG
		syslog(LOG_DEBUG, "[%lu]do_req: Req received\n",pthread_self()%100);
        #endif
		if(current_req != NULL)
		{
			#ifdef DEBUG
			syslog(LOG_DEBUG, "[%lu]do_req: Req not null, executing command\n",pthread_self()%100);
            #endif

			DBConnection* connection;

			RESTRequest* request=restRequest_init();

			char dBapiKey[256];
			sprintf(dBapiKey,"Authorization: ApiKey %s",apiKey);

			 restRequest_setUrl(&request, url);
			
			restRequest_appendHeader(&request, dBapiKey);
			
			 restRequest_appendHeader(&request, "Content-Type: application/json");

			db_connectRest(&connection,"./SQL/database.db", request);

			char* buf;

			buf = calloc(10000000,sizeof(char));
			syslog(LOG_INFO, "Starting query received from ip %s \n",inet_ntoa(current_req->outputsocket->info.sin_addr) );
		    query(connection, current_req->sql_statement,buf); //,buf pedro

		    syslog(LOG_INFO, "Query finished, sending result \n");
		    if (current_req->outputsocket->isConnected)
		    {
			    if(strlen(buf)>0)
			    {
			    	API_write(current_req->outputsocket,"\nStatement:\n",strlen("\nStatement:\n"),NULL);
			    	API_write(current_req->outputsocket,(void*)current_req->sql_statement,strlen(current_req->sql_statement),NULL);
			    	API_write(current_req->outputsocket,"\nResponse:\n",strlen("\nResponse:\n"),NULL);
			    	API_write(current_req->outputsocket,(void*) buf,strlen(buf),NULL);
			    	API_write(current_req->outputsocket,"\n",strlen("\n"),NULL);
			    }
			    else
			    {
			    	API_write(current_req->outputsocket,(void*)"Invalid query\n",15,NULL);
			    }
			}
		    free(buf);
			#ifdef DEBUG
		    syslog(LOG_DEBUG, "[%lu]do_req: Req finished\n",pthread_self()%100);
			#endif

		}
		sem_getvalue(&req_queue_sem,  &num_reqs);
		free(current_req->sql_statement);

		free(current_req);
		
	}while(running == 1 );
}





sql_req*
get_req(sql_req* current_req)
{

	#ifdef DEBUG
	syslog(LOG_DEBUG, "[%lu]get_req()\n",pthread_self()%100);
	syslog(LOG_DEBUG, "[%lu]get_req: Waiting req\n",pthread_self()%100);
    #endif
	pthread_mutex_lock(&req_queue_mutex);
	#ifdef DEBUG
	syslog(LOG_DEBUG, "[%lu]get_req: Detected req in queue\n",pthread_self()%100);
    #endif
	if(&req_queue!= NULL)
	{
		
		current_req = (sql_req *) queue_remove( (queue_t **)&req_queue, (queue_t *)req_queue);

		#ifdef DEBUG
		syslog(LOG_DEBUG, "[%lu]get_req: Retrieved req from queue\n",pthread_self()%100);
        #endif
	} 
	else
	{
		current_req = NULL;
		#ifdef DEBUG
		syslog(LOG_DEBUG, "[%lu]get_req: Error, null req\n",pthread_self()%100);
        #endif
	}
	pthread_mutex_unlock(&req_queue_mutex);
	#ifdef DEBUG
	syslog(LOG_DEBUG, "[%lu]get_req: Retrieved req from queue successfully\n",pthread_self()%100);
    #endif
	return current_req;
}
