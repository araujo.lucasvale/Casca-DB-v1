#ifndef __THREADS__
#define __THREADS__

#include "data_structs.h"
#include "queue.h"
#include <pthread.h>
#include <stdio.h>
#include <sys/types.h>
#include <unistd.h>
#include <semaphore.h>
#include <errno.h>
#include <syslog.h>


/*
	Função que as threads de execução vão ficar executando.
	O parametro tasks é o primeiro elemento da fila de execução
	O parametro mutex_tasks é o mutex de acesso à esse elemento
	Os parametros vec e vec_lenght eh um vetor de sockets
*/
typedef struct do_task_args {
	pthread_mutex_t * mutex_tasks;
	info_cmd_queue_t ** tasks; 	
	sem_t * semaphore; 
	info_socket_t * vec;
	int  vec_lenght;
} do_task_args_t;
void * 
do_task(void * arg);


/*
Função que pega o primeiro elemento da fila de tarefas e carrega em uma variavel
O parametro tasks é a fila de tarefas
O parametro current_task é o endereço onde será carregado a tarefa removida da fila
O parametro é um ponteiro para o mutex da fila de tarefas
*/
info_cmd_queue_t *
get_task(info_cmd_queue_t ** tasks, info_cmd_t ** current_task, pthread_mutex_t * mutex_tasks,info_cmd_queue_t ** queue_pointer);


/*
Função que adiciona uma nova task para fila de tarefas
O parametro task_queue é a fila de tarefas que sera adicionada a tarefa
O parametro task é a tarfa a ser adicionada
O parametro semaphore é o semafaro de acesso a fila
*/
int
add_task(info_cmd_queue_t ** task_queue, info_cmd_queue_t * task, sem_t * semaphore, pthread_mutex_t * tasks_mutex);

/*
Função que executa select sobre o vetor de info_socket_t passado, atualizando-o com o tipo de uso que o socket esta disponivel (type)
O parametro *vec é o vetor de sockets a ser avaliado
O parametro vecLenght é o tamanho do vetor de sockets
O parametro timeout é o tempo que a função select vai ser executada antes de retornar caso não encontre nenhuma atividade nos sockets 
passados (caso = NULL o tempo é infinito )
*/
typedef struct select_task_args {
	info_socket_t * vec;
	int  vec_lenght;
	int timeout;
	pthread_mutex_t * vec_mutex;
	info_cmd_queue_t ** tasks;
	pthread_mutex_t * tasks_mutex;
	sem_t * tasks_sem;
	pthread_mutex_t * IOvec_mutex;
	info_socket_t* IOvec;

} select_task_args_t;

void *
listen_select_task (void * arg);


void *
IO_select_task (void * arg);

void stop_all();
#endif