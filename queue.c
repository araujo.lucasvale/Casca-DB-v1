#include "queue.h"
#include <stdio.h>
#include <syslog.h>
#include "data_structs.h"
#ifdef DEBUG1
	openlog("Casca NON-SGX", LOG_USER, LOG_USER);
#endif


void print_elem2 (void *ptr)
{
   info_cmd_queue_t *elem = (info_cmd_queue_t *)ptr ;

   if (!elem)
      return ;

   elem->prev ? printf ("%d", elem->prev->id) : printf ("-") ;
   printf ("<%d>", elem->id) ;
   elem->next ? printf ("%d ", elem->next->id) : printf ("*") ;
}





int queue_append (queue_t **queue, queue_t *elem)
{
	// - a fila deve existir
	//Verificando se a fila existe
	if(queue == NULL)
	{
		#ifdef DEBUG1
			syslog (LOG_ERR, "Queue does not exist.\n");
		#endif
		#ifdef DEBUGP1
			printf("Queue does not exist.\n");
		#endif
		return -1;
	}

	// - o elemento deve existir
	//Verificando se o elemento existe
	if(elem == NULL)
	{
		#ifdef DEBUG1
			syslog (LOG_ERR, "The element does not exist.\n");
		#endif
		#ifdef DEBUGP1
			printf("The element does not exist.\n");
		#endif
		return -1;
	}

	// - o elemento nao deve estar em outra fila
	//Verificando se o elemento esta em outra fila
	if(elem->prev != NULL || elem->next != NULL)
	{
		#ifdef DEBUG1
			syslog (LOG_ERR, "The element is already in another queue.\n");
		#endif
		#ifdef DEBUGP1
			printf("The element is already in another queue.\n");
		#endif
		return -1;
	}

	//Adicionando o elemento na lista
	//Verificando se a fila esta vazia
	if((*queue) == NULL)
	{
		(*queue) = elem;
		elem->prev = elem;
		elem->next = elem;
	}
	else
	{
		 queue_t *aux;

		 //Adicionando um elemento no fim da lista
		 aux = (*queue)->prev;

		 aux->next = elem;
		 elem->prev = aux;

		 (*queue)->prev = elem;
		 elem->next = (*queue);

	}
	return 0;
}

queue_t *queue_remove (queue_t **queue, queue_t *elem)
{
	// - a fila deve existir
	//Verificando se a fila existe
	if(queue == NULL)
	{
		#ifdef DEBUG1
			syslog (LOG_ERR, "Queue does not exist.\n");
		#endif
		#ifdef DEBUGP1
			printf("Queue does not exist.\n");
		#endif
		return NULL;
	}
	//Verificando se a fila esta vazia
	// - a fila nao deve estar vazia
	if((*queue) == NULL)
	{
		#ifdef DEBUG1
			syslog (LOG_ERR, "The queue is empty.\n");
		#endif
		#ifdef DEBUGP1
			printf("The queue is empty.\n");
		#endif
		return NULL;
	}
	// - o elemento deve existir
	//Verificando se o elemento existe
	if(elem == NULL)
	{
		#ifdef DEBUG1
			syslog (LOG_ERR, "The element does not exist.\n");
		#endif
		#ifdef DEBUGP1
			printf("The element does not exist.\n");
		#endif
		return NULL;
	}

	// - o elemento deve pertencer a fila indicada
	//Verificando se o elemento pertence a fila escolhida
    queue_t *aux = *queue;
    int present = 0; 

    do
    {
        if(aux == elem)

		{
            present = 1;
        }
        aux = aux->next;
    }
    while((aux) != (*queue));

	if(!present)
    {
		return NULL;
    }
   
	//Finalmente removendo o elemento
	(elem->prev)->next = elem->next;
	(elem->next)->prev = elem->prev;

	//Caso o elemento seja o primeiro da fila
	if(elem == *queue && elem->next != elem)
    {
		(*queue) = (*queue)->next;    
    }
	//Caso a fila tenha um unico elemento
	if(elem->next == elem)
    {	
	    (*queue) = NULL;
    }

	elem->prev = NULL;
	elem->next = NULL;



	return elem;

}

int queue_size (queue_t *queue)
{
	if(queue==NULL)
		return 0;

	queue_t *aux = (*queue).next;
	int i = 1;

	while(aux != queue)
	{
		i++;

		aux = (*aux).next;

	}
	return i;
}

void queue_print (char *name, queue_t *queue, void print_elem (void*) )
{
    printf("%s",name);
    printf("[");
    
    //Pecorrendo toda a fila
    if(queue == NULL)
    {
        printf("]\n");
        return;
    }
    queue_t *aux = queue; 
    do
    {
        if(aux->next == NULL)
        {
            break;
        }
        print_elem(aux);
        aux = aux->next;
        
    }while(aux != queue);
        
        
    printf("]\n");
}
#ifdef DEBUG1
	//closelog ();
#endif