FROM sconecuratedimages/crosscompilers:runtime

MAINTAINER Douglas Amaral "dougamaral@me.com"

RUN mkdir /casca

RUN mkdir /casca/SQL

COPY casca /casca/

COPY ./SQL/database.db /casca/SQL

CMD SCONE_MODE=HW SCONE_ALPINE=1 SCONE_VERSION=1 /casca/casca
