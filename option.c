#include "option.h"

void
print_usage()
{
	printf("Using default configs.\n");
	printf("Usage: ./output -u<URL of DB>-k<Key to acess DB> -t<Number of threads> -p<The choosen port> -s<Number of SQL threads> \n");
}


/*
Função que analiza as opções/flags passadas pelo usuário na execução do programa
*/
void
use_getopt(int * argc, char *** argv, config_struct_t * config)
{
	int options;
	config->sql_port = DEFAULT_PORT_SQL;
	config->max_threads = MAX_THREADS_DEFAULT;
	config->max_sqlthreads = MAX_SQLTHREADS_DEFAULT;
	config->db_url = DB_URL;
	config->db_key = DB_KEY;
	//Flag q indica se o usuario digitou -t na execucao
	int tflag = 0;

	if(*argc <= 1)
	{
		config->sql_port = DEFAULT_PORT_SQL;
		config->max_threads = MAX_THREADS_DEFAULT;
		config->max_sqlthreads = MAX_SQLTHREADS_DEFAULT;
		config->db_url = DB_URL;
		config->db_key = DB_KEY;
		print_usage();		
	}

	
	


	while( ( options = getopt(*argc, *argv, "t::s::p::h::k:u:") ) != -1)
	{
		switch(options)
		{

			case 'k':
				//printf("A key eh: %s\n", optarg );	
				config->db_key = optarg;
			break;
			case 'u':
				//printf("O url eh: %s\n", optarg);
				config->db_url = optarg;
			break;
			case 'h':
				print_usage();
				exit(1);	
			break;
			case 't':
				if(optarg == NULL)
				{
					print_usage();
					exit(1);
				}
				tflag = 1;
				//obtem o argumento de -t
				errno = 0;
				char * aux;

				long t_arg_long = strtol(optarg, &aux, 10); 
				

				//Detectando falhas na conversao(usuario pode ter passado argumentos invalidos com -t)
				//Melhorar essa detecao de falhas
				if (aux == optarg)
    			{
    				print_usage();
    				exit(1);
    			}
				if ((t_arg_long == LONG_MAX || t_arg_long == LONG_MIN) && errno == ERANGE)  
    			{
    				print_usage();
					exit(1);
				}
				if ( (t_arg_long > INT_MAX) || (t_arg_long < INT_MIN) )
    			{
    				print_usage();
					exit(1);
				}
    			int t_arg = (int) (t_arg_long);
				//Numero de threads executando tarefas simultaneamente
				config->max_threads = t_arg;

			break;
			case 's':
				if(optarg == NULL)
				{
    				print_usage();
					exit(1);
				}
				
				//obtem o argumento de -t
				errno = 0;
				char * aux2;

				long s_arg_long2 = strtol(optarg, &aux2, 10); 
				

				//Detectando falhas na conversao(usuario pode ter passado argumentos invalidos com -t)
				//Melhorar essa detecao de falhas
				if (aux2 == optarg)
    			{
    				print_usage();
					exit(1);
				}
				if ((s_arg_long2 == LONG_MAX || s_arg_long2 == LONG_MIN) && errno == ERANGE)  
    			{
    				print_usage();
					exit(1);
				}
				if ( (s_arg_long2 > INT_MAX) || (s_arg_long2 < INT_MIN) )
    			{
    				print_usage();
					exit(1);
				}

    			int s_arg2 = (int) (s_arg_long2);
				//Numero de threads executando tarefas simultaneamente
				config->max_sqlthreads = s_arg2;

			break;
			case 'p':
				if(optarg == NULL)
				{
    				print_usage();
					exit(1);
				}
				
				//obtem o argumento de -t
				errno = 0;
				char * aux3;

				long s_arg_long3 = strtol(optarg, &aux3, 10); 
				

				//Detectando falhas na conversao(usuario pode ter passado argumentos invalidos com -t)
				//Melhorar essa detecao de falhas
				if (aux3 == optarg)
    			{
    				print_usage();
					exit(1);
				}
				if ((s_arg_long3 == LONG_MAX || s_arg_long3 == LONG_MIN) && errno == ERANGE)  
    			{
    				print_usage();
					exit(1);
				}
				if ( (s_arg_long3 > INT_MAX) || (s_arg_long3 < INT_MIN) )
    			{
    				print_usage();
					exit(1);
				}	

    			int s_arg3 = (int) (s_arg_long3);
				//Numero de threads executando tarefas simultaneamente
				config->sql_port = s_arg3;

			break;
			case '?':

				if(optopt == 't')
				{
					exit(1);
				}
				else
				{
					printf("Unknow input	 %c.\n", optopt);
					print_usage();
					exit(1);
				}
			break;
			default:
				printf("Invalid input.\n");
				print_usage();
				exit(1);
			break;
		}

	}


}