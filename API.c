/*
 * Copyright UTFPR
 * @documentation
 *
 *
 */

#include "API.h"
#include <unistd.h>


info_socket_t * IO_isocket_vec;
pthread_mutex_t * IO_isocket_vec_mutex ;
info_socket_t * SV_isocket_vec;
pthread_mutex_t * SV_isocket_vec_mutex;
info_cmd_queue_t* 	tp_msgs = NULL;
pthread_mutex_t     tp_mutex = PTHREAD_MUTEX_INITIALIZER;
sem_t 			 	tp_sem ;
sql_req* 	   req_queue;
pthread_mutex_t req_queue_mutex;
sem_t 		   req_queue_sem;
char*		   url;
char*           apiKey;
pthread_mutex_t dbfile_mutex = PTHREAD_MUTEX_INITIALIZER;
pthread_t *SQLthreads ;
pthread_t* thread;


/* @documentation process_default
 * Função padrão para ser utilizada ao receber um pacote de bytes caso o usuário não especifique outra função
 * Default function used to receive a bytes package if user doesn't specifies another function
 */
long process_default(void*  socket,void* msg, long tam)
{
	//syslog(LOG_WARNING, "process_default: Essa função é um placeholder");
	printf("\n\n\n\n\n\n\n\n");
	printf("Process default message!(%lu): %s\n",tam,(char*)msg );
	printf("\n\n\n\n\n\n\n\n");
	if(strcmp((char*)msg,"12345"))
	{
		return 1;
	}
	return 0;
}	
/* @documentation send_done_default
 * Função para ser utilizada ao terminar de enviar bytes caso o usuario não especifique uma
 * Function used when you finished send data bytes if user doesn't specifies another function
 */
void send_done_default()
{
	//syslog(LOG_WARNING, "send_done_default: Essa função é um placeholder");

	//printf("Send done default message!\n");

	return;
}	

/* @documentation connect_sucessful_default
 *  descrever pt-br
 *  describe en-us
 */
void connect_sucessful_default()
{
	//syslog(LOG_WARNING, "connect_sucessful_default: Essa função é um placeholder");

	//printf("Connected default message!\n");

	return;
}



void API_Init(config_struct_t config)
{

	setbuf(stdout, NULL);
	

	syslog(LOG_INFO, "API: Initializing Casca ...\n");
	syslog(LOG_DEBUG, "[%lu]API_Init()",(long unsigned int)pthread_self()%100);

	sem_init(&tp_sem, 1, 0);
	
	sem_init(&req_queue_sem, 1, 0);

	url = config.db_url;
	
	apiKey = config.db_key;
	pthread_mutex_init(&(dbfile_mutex), NULL);
	
	IO_isocket_vec_mutex = malloc(sizeof(pthread_mutex_t));
	pthread_mutex_init(IO_isocket_vec_mutex, NULL);
	//pthread_mutex_unlock(IO_isocket_vec_mutex);
	IO_isocket_vec = malloc(MAX_IO_SOCKETS*sizeof(info_socket_t));

	SV_isocket_vec_mutex = malloc(sizeof(pthread_mutex_t));
	pthread_mutex_init(SV_isocket_vec_mutex, NULL);
	//pthread_mutex_unlock(SV_isocket_vec_mutex);
	SV_isocket_vec = malloc(MAX_SV_SOCKETS*sizeof(info_socket_t));

    #ifdef TLS
	syslog(LOG_INFO, "API: Initializing TLS...\n");
	
	#ifdef SGX
	initialize_library();
	#endif
	OpenSSL_add_ssl_algorithms();
	SSL_CTX* API_IO_context;
	API_IO_context = API_create_context('I');
	API_configure_context(API_IO_context,'I');
	SSL_CTX* API_server_context;
	API_server_context = API_create_context('S');
	API_configure_context(API_server_context,'S');
    #endif

	for (int i = 0; i < MAX_IO_SOCKETS; ++i)
	{
		IO_isocket_vec[i].socket=0;
		#ifdef TLS
		IO_isocket_vec[i].socket_tls=NULL;
		#endif		
		IO_isocket_vec[i].shutdown=0;
		IO_isocket_vec[i].socket_data_input.buffer_size= 0;

		 IO_isocket_vec[i].GPsem = malloc(sizeof(sem_t));
		sem_init(IO_isocket_vec[i].GPsem, 1, 0);

        #ifdef TLS
		IO_isocket_vec[i].socket_context=API_IO_context;
        #endif
	}
	for (int i = 0; i <MAX_SV_SOCKETS; ++i)
	{
		SV_isocket_vec[i].socket=0;
		#ifdef TLS
		SV_isocket_vec[i].socket_tls=NULL;
		#endif
		SV_isocket_vec[i].shutdown=0;
		SV_isocket_vec[i].select_ready=false;
		 SV_isocket_vec[i].GPsem = malloc(sizeof(sem_t));
		sem_init(SV_isocket_vec[i].GPsem, 1, 0);
		

        #ifdef TLS
		SV_isocket_vec[i].socket_context=API_server_context;
        #endif
	}

	config.max_threads   +=2;
	
	thread =(pthread_t*) malloc(config.max_threads*sizeof(pthread_t));
	SQLthreads = (pthread_t*) malloc(config.max_sqlthreads*sizeof(pthread_t));
	syslog(LOG_DEBUG, "[%lu]API_Init: Memory sucessful allocated",(long unsigned int)pthread_self()%100);

	process_func_t p_func = &process_default;

	select_task_args_t select_args;

	select_args.vec = SV_isocket_vec;
	select_args.vec_lenght = MAX_SV_SOCKETS;
	select_args.timeout =  SV_TIMEOUT_USECONDS;
	select_args.vec_mutex = SV_isocket_vec_mutex;
	select_args.tasks = &tp_msgs;
	select_args.tasks_mutex = &tp_mutex;
	select_args.tasks_sem = &tp_sem;
	select_args.IOvec =IO_isocket_vec;
	select_args.IOvec_mutex = IO_isocket_vec_mutex;

	syslog(LOG_DEBUG, "[%lu]API_Init: Initializing server listen thread",(long unsigned int)pthread_self()%100);
		syslog(LOG_INFO, "API_Init: Initializing thread %d (Sever select)\n", 0);

	pthread_create( &thread[0], NULL, &listen_select_task, (void*) &select_args );

	syslog(LOG_DEBUG, "[%lu]API_Init: Server listen thread initialized",(long unsigned int)pthread_self()%100);

	select_task_args_t IO_select_args;
	IO_select_args.vec = IO_isocket_vec;
	IO_select_args.vec_lenght = MAX_IO_SOCKETS;
	IO_select_args.timeout = IO_TIMEOUT_USECONDS;
	IO_select_args.vec_mutex = SV_isocket_vec_mutex;
	IO_select_args.tasks = &tp_msgs;
	IO_select_args.tasks_mutex = &tp_mutex;
	IO_select_args.tasks_sem = &tp_sem;
	IO_select_args.IOvec =IO_isocket_vec;
	IO_select_args.IOvec_mutex = IO_isocket_vec_mutex;

	syslog(LOG_DEBUG, "[%lu]API_Init: Initializing IO listen thread",(long unsigned int)pthread_self()%100);
	syslog(LOG_INFO, "API_Init: Initializing thread %d (IO select)\n", 1);
	pthread_create( &thread[1], NULL, &IO_select_task, (void*) &IO_select_args );

	syslog(LOG_DEBUG, "[%lu]API_Init: IO listen thread initialized",(long unsigned int)pthread_self()%100);

	do_task_args_t task_args;

	for(int i = 2; i <config.max_threads; i++ )
	{		
		syslog(LOG_INFO, "API_Init: Initializing thread %d (worker)\n", i);
		
		task_args.tasks = &tp_msgs;
		task_args.mutex_tasks = &tp_mutex;
		task_args.semaphore = &tp_sem;

		pthread_create( &thread[i], NULL, &do_task, (void*) &task_args );
	}

	

	for(int i = 0; i <config.max_sqlthreads; i++ )
	{		
		syslog(LOG_INFO, "API_Init: Initializng SQLthread %d (SQLworker)\n", i);

		
		pthread_create( &SQLthreads[i], NULL, &do_req,NULL);
	}



	sleep(1);

	syslog(LOG_INFO, "API: Initialized Casca sucessfully\n");
}




/* @documentation API_create_server
 * Função para criar um socket que escuta a porta passada por referencia e cria sockets de IO que chamam as funções passadas ao receber/enviar dados
 * This function creates a socket and listen to a port passed by reference and also creates IO socket, IO socket can receive commands to call internal functions of the API
 */
info_socket_t* API_create_server(int port, send_done_func_t* send_done_func, process_func_t process_func)
{

	syslog(LOG_INFO, "API: Creating server socket\n");
	
	syslog(LOG_DEBUG, "[%lu]API_create_server() ",(long unsigned int)pthread_self()%100);

	if(port <= 0)
	{
		syslog(LOG_ERR, "[%lu]API_create_server: invalid port",(long unsigned int)pthread_self()%100);
		return NULL;
	}

	syslog(LOG_DEBUG, "[%lu]API_create_server: Waiting access to vector",(long unsigned int)pthread_self()%100);

	pthread_mutex_lock(SV_isocket_vec_mutex);
	
	syslog(LOG_DEBUG, "[%lu]API_create_server: Access to vector granted",(long unsigned int)pthread_self()%100);
	
	for (int i = 0; i < MAX_SV_SOCKETS; ++i)
	{

		if(SV_isocket_vec[i].socket==0)
		{

			if(process_func == NULL)
			{
				syslog(LOG_WARNING, "[%lu]API_create_server: Function 'process_func' null, default function will be used instead",(long unsigned int)pthread_self()%100);
				SV_isocket_vec[i].socket_data_input.process = &process_default;
			}
			else
			{
				SV_isocket_vec[i].socket_data_input.process = process_func;
			}

			if(send_done_func == NULL)
			{
				syslog(LOG_WARNING, "[%lu]API_create_server: Function 'send_done_func' null, default function will be used instead",(long unsigned int)pthread_self()%100);
				SV_isocket_vec[i].socket_data_output.send_done = &send_done_default;
			}
			else
			{
				SV_isocket_vec[i].socket_data_output.send_done = *send_done_func;
			}

			syslog(LOG_DEBUG, "[%lu]API_create_server: Initializing server socket",(long unsigned int)pthread_self()%100);

			SV_isocket_vec[i].type = SERVER;

			int error = socket_init(&SV_isocket_vec[i],port);
			
			if(error < 0)
			{
				syslog(LOG_ERR, "[%lu]API_create_server: Error in socket initialization, aborting",(long unsigned int)pthread_self()%100);
				pthread_mutex_unlock(SV_isocket_vec_mutex);
				return NULL;
			}

			syslog(LOG_DEBUG, "[%lu]API_create_server: Socket initialized ",(long unsigned int)pthread_self()%100);	

			pthread_mutex_unlock(SV_isocket_vec_mutex);

			syslog(LOG_DEBUG, "[%lu]API_create_server: New server socket in %d fd = %d",(long unsigned int)pthread_self()%100,i,SV_isocket_vec[i].socket);

			syslog(LOG_INFO, "API: Server socket created sucessfully\n");

			return &SV_isocket_vec[i];
		}

	}

	pthread_mutex_unlock(SV_isocket_vec_mutex);
	
	syslog(LOG_ERR, "[%lu]API_create_server: Couldn't find a slot for new socket, aborting\n",(long unsigned int)pthread_self()%100);

	return NULL;
}

/* @documentation
 * API_create_socket
 *	  Cria um socket e o conecta ao endereço desejado na porta referenciada caso exista espaço no vetor,
 *	  Quando o socket receber algum dado será chamada a função "process_func" do tipo: long (* process_func_t)(void * buffer, long buffer_used)
 *	  Quando o socket terminar de mandar os dados será chamada a função "send_done_func" do tipo: void (* send_done_func_t)();
 *	  Caso as funções sejam nulas, são atribuidas funções padrão de processamento
 *	  Caso não exista espaço retorna NULL
 *	  Caso não consiga conectar ...
 *	  Caso consiga criar o socket corretamente retorna um ponteiro para o mesmo
 */
info_socket_t* API_create_socket(char*  address,int porta,send_done_func_t* send_done_func, process_func_t* process_func,task_func* connected)
{

	syslog(LOG_INFO, "API: Creating IO socket\n");

	syslog(LOG_DEBUG, "[%lu]API_create_socket()",(long unsigned int)pthread_self()%100);

	if(address == NULL)
	{
		syslog(LOG_ERR, "[%lu]API_create_socket: Null adress",(long unsigned int)pthread_self()%100);
		return NULL;
	}
	if(porta <= 0)
	{
		syslog(LOG_ERR, "[%lu]API_create_socket: Invalid port",(long unsigned int)pthread_self()%100);
		return NULL;
	}

	pthread_mutex_lock(IO_isocket_vec_mutex );

	for (int i = 0; i < MAX_IO_SOCKETS; ++i)
	{

		if(IO_isocket_vec[i].socket==0)
		{

			syslog(LOG_DEBUG, "[%lu]API_create_socket: IO slot found",(long unsigned int)pthread_self()%100);
			if(process_func == NULL)
			{
				syslog(LOG_WARNING, "[%lu]API_create_socket: Function 'process_func' null, default function will be used instead",(long unsigned int)pthread_self()%100);
				IO_isocket_vec[i].socket_data_input.process = &process_default;
			}
			else
			{
				IO_isocket_vec[i].socket_data_input.process = process_func;
			}

			if(send_done_func == NULL)
			{
				syslog(LOG_WARNING, "[%lu]API_create_socket: Function 'send_done_func'null, default function will be used instead",(long unsigned int)pthread_self()%100);
				IO_isocket_vec[i].socket_data_output.send_done = &send_done_default;
			}
			else
			{
				IO_isocket_vec[i].socket_data_output.send_done = *send_done_func;
			}

			

			IO_isocket_vec[i].type =IO;

			int err = socket_init(&IO_isocket_vec[i] , 0);

			if(err < 0)
			{
				syslog(LOG_ERR, "[%lu]API_create_socket: Error in socket initialization, aborting",(long unsigned int)pthread_self()%100);
				return NULL;
			}

			syslog(LOG_DEBUG, "[%lu]API_create_socket: Socket initialized",(long unsigned int)pthread_self()%100);

			IO_isocket_vec[i].isConnected = false;
			IO_isocket_vec[i].inRead = false;
			IO_isocket_vec[i].inWrite = false;
			IO_isocket_vec[i].info.sin_addr.s_addr = inet_addr(address);
			IO_isocket_vec[i].info.sin_family = AF_INET;
			IO_isocket_vec[i].shutdown = 0;

			IO_isocket_vec[i].info.sin_port = htons( porta);

			IO_isocket_vec[i].select_ready = false;
			IO_isocket_vec[i].socket_data_output.message_queue =NULL;

			info_cmd_queue_t* new_task = malloc(sizeof(info_cmd_queue_t));

			new_task->cmd.cmd =&cmd_connect;
			new_task->cmd.socket = &IO_isocket_vec[i];
			new_task->next =NULL;
			new_task->prev =NULL;

			if(connected == NULL)
			{
				new_task->cmd.connected = &connect_sucessful_default;
			}
			else
			{
				new_task->cmd.connected = *connected;
			}



			pthread_mutex_unlock(IO_isocket_vec_mutex );

			syslog(LOG_DEBUG, "[%lu]API_create_socket: Socket created sucessfully",(long unsigned int)pthread_self()%100);

			syslog(LOG_DEBUG, "[%lu]API_create_socket: Inserting new connection task in queue",(long unsigned int)pthread_self()%100);

			int error = add_task(&tp_msgs, new_task, &tp_sem, &tp_mutex);

			if(error < 0 )
			{
				syslog(LOG_ERR, "[%lu]API_create_socket: Failed to instert new task in the queue, aborting",(long unsigned int)pthread_self()%100);
				return NULL;
			}

			syslog(LOG_INFO, "API: IO Socket created with sucess fd = %d \n", IO_isocket_vec[i].socket);

			return &IO_isocket_vec[i];
		}

	}

	pthread_mutex_unlock(IO_isocket_vec_mutex );

	syslog(LOG_ERR, "[%lu]API_create_socket: Couldn't find a slot for new socket, aborting",(long unsigned int)pthread_self()%100);

	return NULL;
}


#ifdef TLS



info_socket_t* API_create_server_TLS(int port, send_done_func_t* send_done_func, process_func_t process_func)
{

	syslog(LOG_INFO, "API: Creating TLS server socket\n");
	
	syslog(LOG_DEBUG, "[%lu]API_create_server_TLS() ",(long unsigned int)pthread_self()%100);

	if(port <= 0)
	{
		syslog(LOG_ERR, "[%lu]API_create_server_TLS: invalid port",(long unsigned int)pthread_self()%100);
		return NULL;
	}

	syslog(LOG_DEBUG, "[%lu]API_create_server_TLS:  Waiting access to vector",(long unsigned int)pthread_self()%100);

	pthread_mutex_lock(SV_isocket_vec_mutex);
	
	syslog(LOG_DEBUG, "[%lu]API_create_server_TLS: Access to vector granted",(long unsigned int)pthread_self()%100);
	
	for (int i = 0; i < MAX_SV_SOCKETS; ++i)
	{

		if(SV_isocket_vec[i].socket==0)
		{

			if(process_func == NULL)
			{
				syslog(LOG_WARNING, "[%lu]API_create_server_TLS: Function 'process_func' null, default function will be used instead",(long unsigned int)pthread_self()%100);
				SV_isocket_vec[i].socket_data_input.process = &process_default;
			}
			else
			{
				SV_isocket_vec[i].socket_data_input.process = process_func;
			}

			if(send_done_func == NULL)
			{
				syslog(LOG_WARNING, "[%lu]API_create_server_TLS:  Function 'send_done_func'null, default function will be used instead",(long unsigned int)pthread_self()%100);
				SV_isocket_vec[i].socket_data_output.send_done = &send_done_default;
			}
			else
			{
				SV_isocket_vec[i].socket_data_output.send_done = *send_done_func;
			}

			syslog(LOG_DEBUG, "[%lu]API_create_server_TLS: Initilizing server socket",(long unsigned int)pthread_self()%100);

			SV_isocket_vec[i].type = TLS_SERVER;

			int error = socket_init(&SV_isocket_vec[i],port);
			
			if(error < 0)
			{
				syslog(LOG_ERR, "[%lu]API_create_server_TLS:  Error in socket initialization, aborting",(long unsigned int)pthread_self()%100);
				pthread_mutex_unlock(SV_isocket_vec_mutex);
				return NULL;
			}

			syslog(LOG_DEBUG, "[%lu]API_create_server_TLS: Socket initialized ",(long unsigned int)pthread_self()%100);	

			pthread_mutex_unlock(SV_isocket_vec_mutex);

			syslog(LOG_DEBUG, "[%lu]API_create_server_TLS: New server socket in %d fd = %d",(long unsigned int)pthread_self()%100,i,SV_isocket_vec[i].socket);

			syslog(LOG_INFO, "API: Server socket created with sucess\n");

			return &SV_isocket_vec[i];
		}

	}

	pthread_mutex_unlock(SV_isocket_vec_mutex);
	
	syslog(LOG_ERR, "[%lu]API_create_server_TLS: Couldn't find a slot for new socket, aborting\n",(long unsigned int)pthread_self()%100);

	return NULL;
}



info_socket_t* API_create_socket_TLS(char*  address,int porta,send_done_func_t* send_done_func, process_func_t* process_func,task_func* connected)
{
	syslog(LOG_INFO, "API: Creating IO socket\n");

	syslog(LOG_DEBUG, "[%lu]API_create_socket()",(long unsigned int)pthread_self()%100);

	if(address == NULL)
	{
		syslog(LOG_ERR, "[%lu]API_create_socket: Null adress",(long unsigned int)pthread_self()%100);
		return NULL;
	}
	if(porta <= 0)
	{
		syslog(LOG_ERR, "[%lu]API_create_socket: Invalid port",(long unsigned int)pthread_self()%100);
		return NULL;
	}

	pthread_mutex_lock(IO_isocket_vec_mutex );

	for (int i = 0; i < MAX_IO_SOCKETS; ++i)
	{

		if(IO_isocket_vec[i].socket==0)
		{

			syslog(LOG_DEBUG, "[%lu]API_create_socket: IO slot found",(long unsigned int)pthread_self()%100);
			if(process_func == NULL)
			{
				syslog(LOG_WARNING, "[%lu]API_create_socket:  Function 'process_func' null, default function will be used instead",(long unsigned int)pthread_self()%100);
				IO_isocket_vec[i].socket_data_input.process = &process_default;
			}
			else
			{
				IO_isocket_vec[i].socket_data_input.process = *process_func;
			}

			if(send_done_func == NULL)
			{
				syslog(LOG_WARNING, "[%lu]API_create_socket: Function 'send_done_func'null, default function will be used instead",(long unsigned int)pthread_self()%100);
				IO_isocket_vec[i].socket_data_output.send_done = &send_done_default;
			}
			else
			{
				IO_isocket_vec[i].socket_data_output.send_done = *send_done_func;
			}

			

			IO_isocket_vec[i].type = IO;

			int err = socket_init(&IO_isocket_vec[i] , 0);

			if(err < 0)
			{
				syslog(LOG_ERR, "[%lu]API_create_socket: Error in socket initialization, aborting",(long unsigned int)pthread_self()%100);
				return NULL;
			}

			syslog(LOG_DEBUG, "[%lu]API_create_socket: Socket initialized",(long unsigned int)pthread_self()%100);


			IO_isocket_vec[i].inRead = false;
			IO_isocket_vec[i].inWrite = false;
			IO_isocket_vec[i].info.sin_addr.s_addr = inet_addr(address);
			IO_isocket_vec[i].info.sin_family = AF_INET;
			IO_isocket_vec[i].info.sin_port = htons( porta);
			IO_isocket_vec[i].select_ready = false;
			IO_isocket_vec[i].socket_data_output.message_queue =NULL;

			//IO_isocket_vec[i].select_ready = true;

			info_cmd_queue_t* new_task = malloc(sizeof(info_cmd_queue_t));


			new_task->cmd.cmd =&cmd_connect_tls;

			new_task->cmd.socket = &IO_isocket_vec[i];
			new_task->next =NULL;
			new_task->prev =NULL;

			if(connected == NULL)
			{
				new_task->cmd.connected = &connect_sucessful_default;
			}
			else
			{
				new_task->cmd.connected = *connected;
			}



			pthread_mutex_unlock(IO_isocket_vec_mutex );

			syslog(LOG_DEBUG, "[%lu]API_create_socket: Socket created",(long unsigned int)pthread_self()%100);

			syslog(LOG_DEBUG, "[%lu]API_create_socket: Inserting new connection task in queue",(long unsigned int)pthread_self()%100);

			int error = add_task(&tp_msgs, new_task, &tp_sem, &tp_mutex);

			if(error < 0 )
			{
				syslog(LOG_ERR, "[%lu]API_create_socket: Failed to instert new task in the queue, aborting",(long unsigned int)pthread_self()%100);
				return NULL;
			}

			syslog(LOG_INFO, "API: IO Socket created with sucess\n");



			return &IO_isocket_vec[i];
		}

	}

	pthread_mutex_unlock(IO_isocket_vec_mutex );

	syslog(LOG_ERR, "[%lu]API_create_socket: Couldn't find a slot for new socket, aborting",(long unsigned int)pthread_self()%100);

	return NULL;
}
#endif

int API_write(info_socket_t* socket,void* msg,long int msg_size,send_done_func_t send_done_func)
{
	if(socket->shutdown == 1)
	{
		syslog(LOG_ERR, "[%lu]API_write: Socket in shutdown process, the message will not be sent",(long unsigned int)pthread_self()%100);
		return -1;
	}
	
	if(socket->isConnected == 0)
	{
		syslog(LOG_ERR, "[%lu]API_write: Socket disconnected, could not send message",(long unsigned int)pthread_self()%100);
		return -1;
	}
	

	syslog(LOG_DEBUG, "API: Writing message\n");

	syslog(LOG_DEBUG, "[%lu]API_write()",(long unsigned int)pthread_self()%100);
	
	if(socket == NULL)
	{
		syslog(LOG_ERR, "[%lu]API_write: Null socket, aborting",(long unsigned int)pthread_self()%100);
		return -1;
	}
	if(msg == NULL)
	{
		syslog(LOG_ERR, "[%lu]API_write: Null message, aborting",(long unsigned int)pthread_self()%100);
		return -1;
	}
	if(msg_size < 0)
	{
		syslog(LOG_ERR, "[%lu]API_write: Negative message size, aborting",(long unsigned int)pthread_self()%100);
		return -1;		
	}

	message_queue_t* new_message = (message_queue_t *) malloc(sizeof(message_queue_t));
	new_message->next =NULL;
	new_message->prev =NULL;
	new_message->buffer_size = msg_size;
	new_message->free_index = msg_size-1;
	new_message->buffer = malloc(msg_size+1 * sizeof(char));

	if(send_done_func == NULL)
	{
		syslog(LOG_WARNING, "[%lu]API_Write: Function 'send_done_func'null, default function will be used instead",(long unsigned int)pthread_self()%100);
		new_message->send_done = send_done_default;

	}
	else
	{
		
		new_message->send_done = send_done_func;
	}



	bzero(new_message->buffer,msg_size+1);

	char * aux = (char*)new_message->buffer;
	memcpy (aux, msg, msg_size+1 ); 
	

	int err = queue_append ((queue_t **)&(socket->socket_data_output.message_queue), (queue_t*) new_message);
	if(err<0)
	{
		//syslog(LOG_ERR, "[%lu]API_write: Erro na inserção na fila, abortando operação\n",pthread_self()%100);
		return -1;
	}
	
	
	socket->select_ready= true;

	syslog(LOG_DEBUG, "[%lu]API_write: Output buffer updated",(long unsigned int)pthread_self()%100);

	syslog(LOG_DEBUG, "API: Message inserted in queue with sucess\n");

	return 0;
}


int API_close_socket(info_socket_t* socket)
{	
	syslog(LOG_INFO, "API: Closing socket %d\n", socket->socket);

	syslog(LOG_DEBUG, "[%lu]API_close_socket()",(long unsigned int)pthread_self()%100);

	if(socket == NULL)
	{
		syslog(LOG_ERR, "[%lu]API_close_socket: Null socket, aborting",(long unsigned int)pthread_self()%100);
		return -1;
	}

	socket->shutdown = true;

	syslog(LOG_DEBUG, "[%lu]API_close_socket: The socket %d was shutdowned",(long unsigned int)pthread_self()%100, socket->socket);

	syslog(LOG_INFO, "API: The socket will be removed from the list in next cycle\n");
}
#ifdef TLS
SSL_CTX *API_create_context(char type)
{
	const SSL_METHOD *method;
	SSL_CTX *ctx;
	switch(type)
	{
		case 'I':
		method = SSLv23_client_method();
		break;

		case 'S':
		method = SSLv23_server_method();
		break;
	}
	ctx = SSL_CTX_new(method);
	if (!ctx) {
		perror("Unable to create SSL context");
		exit(EXIT_FAILURE);
	}
	return ctx;
}

void API_configure_context(SSL_CTX *ctx, char type)
{
	switch(type)
	{
		case 'I':
		if (SSL_CTX_use_certificate_chain_file(ctx, CERT_IO_FILE) <= 0) {
			ERR_print_errors_fp(stderr);
			exit(EXIT_FAILURE);
		}

		if (SSL_CTX_use_PrivateKey_file(ctx, KEY_IO_FILE, SSL_FILETYPE_PEM) <= 0 ) {
			ERR_print_errors_fp(stderr);
			exit(EXIT_FAILURE);
		}
		break;
		case 'S':
		if (SSL_CTX_use_certificate_chain_file(ctx, CERT_SERV_FILE) <= 0) {
			ERR_print_errors_fp(stderr);
			exit(EXIT_FAILURE);
		}

		if (SSL_CTX_use_PrivateKey_file(ctx, KEY_SERV_FILE, SSL_FILETYPE_PEM) <= 0 ) {
			ERR_print_errors_fp(stderr);
			exit(EXIT_FAILURE);
		}
		break;
	}
}
#endif

int API_shutdown()
{
	pthread_mutex_lock(IO_isocket_vec_mutex );
	
	for (int i = 0; i < MAX_IO_SOCKETS; ++i)
	{
		IO_isocket_vec[i].shutdown = true;
	}
	
	pthread_mutex_unlock(IO_isocket_vec_mutex );
	
	pthread_mutex_lock(SV_isocket_vec_mutex );
	
	for (int i = 0; i <MAX_SV_SOCKETS; ++i)
	{
		SV_isocket_vec[i].shutdown = true;
	}
	
	pthread_mutex_unlock(SV_isocket_vec_mutex );

	

	bool still_not_empty = 1;

	while (still_not_empty)
	{
		still_not_empty = 0;
		
		for (int i = 0; i < MAX_IO_SOCKETS; ++i)
		{
			if(IO_isocket_vec[i].socket!=0)
			{

				if(IO_isocket_vec[i].socket_data_output.message_queue!=NULL)
				{
					still_not_empty = 1;
				}	
			}
		}
	}

	


	stop_all();
	
	int* ret = NULL;

	for(int i = 0; i < 10; i++ )
	{		
		printf("cancel thread%d\n",i );
		pthread_cancel(thread[i]);

		pthread_join( thread[i], (void*)ret);
	}

	stop_all_SQL();
	for (int i = 0; i < 1; ++i)
	{

		
			pthread_cancel(SQLthreads[i]);
			pthread_join(SQLthreads[i], (void*)ret);
		
	}


		
	/**Pedro?**/
	//Aqui eu dou free nas estruturas que foram alocadas durante a execução
	//Não tenho certeza se quando você da shutdown, ainda usa essa estruturas dnv
	//tipo recriando uma nova fila de sockets, etc... mas acho não, apague esse comentario se a resposta for nao

	free(IO_isocket_vec_mutex);
	free(SV_isocket_vec_mutex);
	free(IO_isocket_vec);
	free(SV_isocket_vec);
	free(thread);
	free(SQLthreads);

	}


	info_socket_t* API_create_SQLserver (int port)
	{
	//start

		syslog(LOG_INFO, "API: Creating SQL socket\n");

		syslog(LOG_DEBUG, "[%lu]API_create_SQLserver() ",(long unsigned int)pthread_self()%100);

		if(port <= 0)
		{
			syslog(LOG_ERR, "[%lu]API_create_SQLserver: Invalid port",(long unsigned int)pthread_self()%100);
			return NULL;
		}

		syslog(LOG_DEBUG, "[%lu]API_create_SQLserver: Waiting acess to vector",(long unsigned int)pthread_self()%100);

		pthread_mutex_lock(SV_isocket_vec_mutex);


		syslog(LOG_DEBUG, "[%lu]API_create_SQLserver: Acess to vector granted",(long unsigned int)pthread_self()%100);

	//end

		for (int i = 0; i < MAX_SV_SOCKETS; ++i)
		{

			if(SV_isocket_vec[i].socket==0)
			{


				SV_isocket_vec[i].socket_data_input.process = &SQL_create_req;





				syslog(LOG_DEBUG, "[%lu]API_create_SQLserver: Initializing server socket",(long unsigned int)pthread_self()%100);
				SV_isocket_vec[i].type = SERVER;
				#ifdef SECURE_SQL
				SV_isocket_vec[i].type = TLS_SERVER;
				#endif
				int error = socket_init(&SV_isocket_vec[i],port);

				if(error < 0)
				{
					syslog(LOG_ERR, "[%lu]API_create_SQLserver: Error in socket initialization, aborting",(long unsigned int)pthread_self()%100);
					return NULL;
				}

				syslog(LOG_DEBUG, "[%lu]API_create_SQLserver: Socket initialized ",(long unsigned int)pthread_self()%100);	

				pthread_mutex_unlock(SV_isocket_vec_mutex);

				syslog(LOG_DEBUG, "[%lu]API_create_SQLserver: New server socket in %d fd = %d",(long unsigned int)pthread_self()%100,i,SV_isocket_vec[i].socket);

				
				#ifdef SECURE_SQL
				syslog(LOG_INFO, "API: Created TLS SQL socket  in port %d\n",port);
				#else
				syslog(LOG_INFO, "API: Created SQL Socket in port %d\n",port);
				#endif
				return &SV_isocket_vec[i];
			}

		}

		pthread_mutex_unlock(SV_isocket_vec_mutex);

		syslog(LOG_ERR, "[%lu]API_create_SQLserver: Couldn't find a slot for new socket, aborting\n",(long unsigned int)pthread_self()%100);

		return NULL;
	}


	void API_create_dummy_task()
	{
		
		info_cmd_queue_t* new_task = malloc(sizeof(info_cmd_queue_t));

		new_task->cmd.cmd =&cmd_dummy;
		new_task->cmd.socket = NULL;
		new_task->next =NULL;
		new_task->prev =NULL;

		add_task(&tp_msgs, new_task, &tp_sem, &tp_mutex);

		return;
	}
