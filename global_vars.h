
#include <pthread.h>
#ifndef GVARS
#define GVARS

extern sql_req* 	   req_queue;
extern pthread_mutex_t req_queue_mutex;
extern sem_t 		   req_queue_sem;
extern int             running;
extern char*		   url;
extern char*           apiKey;
extern pthread_mutex_t dbfile_mutex ;
#endif