#ifndef CONSULTA_SQL_H
#define CONSULTA_SQL_H


#include <stdio.h>
#include <stdlib.h>
#include <regex.h>
#include <string.h>
#include <assert.h>
#include <string.h>
#define _GNU_SOURCE
char *strcasestr(const char *haystack, const char *needle);
#include "../dic_dados.h"

#define MAX_ATRIBUTES_MATCH 4
#define MAX_TABLES_MATCH 4
#define MAX_CONDITIONS_MATCH 4

enum type
{
    SQL_SELECT = 0,
    SQL_INSERT,
    SQL_CREATE_TABLE,
    SQL_DELETE,
    SQL_NO_MATCH

};

typedef struct sql_select{

    //Vari�vel: Atributos
    //Vetor de strings, cada posi��o cont�m um atributo extra�do do SQL-> SELECT x1,x2,x3...
    //No formato Tabela.Atributo
    char** attributes;
    int numberOfAttributes;
    //Vari�vel: Tabelas
    //Vetor de strings, cada posi��o cont�m uma tabela extra�da do SQL-> FROM x1,x2,x3...
    char** tables;
    int numberOfTables;
    //Vari�vel: Condicoes
    //Primeira posi��o: Atributo da primeira tabela
    //Segunda posi��o: Condi��o
    //Terceira posi��o: Atributo da segunda tabela
    char** conditions;
    int numberOfConditions;

}consulta_select;

typedef struct sql_insert{

    //Vari�vel: Tabela
    //String contendo a tabela candidata para a insercao de dados
    char* table;
    //Vari�vel: Valores
    //Vetor de strings, cada posi��o cont�m um valor extra�do do SQL-> Values ('x1','x2','x3'...)
    char** values;
    int numberOfValues;

}consulta_insert;

consulta_select* parse_sql_select(char* sql);

consulta_insert* parse_sql_insert(char* sql);

Table* parse_sql_create_table(char* sql);

void print_consulta_select(consulta_select c);

void print_consulta_insert(consulta_insert c);

int check_type(char* sql);

char* str_between_delimiter(char* str, const char delim);

#endif // CONSULTA_SQL_H
