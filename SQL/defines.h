/** 
 * @file 
 * @author Davi Boberg
 * @date 24/01/2018
 * 
 * \brief Some dfines.
 *
 */

#define MAX_STRING_SIZE 1024

#define MAX_TABLES 5

#define MAX_DATA_CURSOR 50

#define MAX_REST_HEADERS 4

//#define DEBUG_SELECT

//#define PRINT_RESULTS_SELECT

//#define MEASURE_TIME_SELECT

//#define DEBUG_SELECT

//#define MEASURE_TIME_INSERT

//#define PRINT_RESULTS_INSERT