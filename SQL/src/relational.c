#include "relational.h"


int projection(QueryTable** ret, QueryTable* queryTable, char* attributes[MAX_ATTRIBUTES_MATCH], int numberOfAttributes)
{
    int headerSize = 0;
    int collumnsToSave[numberOfAttributes];
    int collumnsToSaveIndex = 0;
    for(int i = 0; i < numberOfAttributes; i++)
    {
        int attributeIndex = -1;
        for(int j = 0; j < queryTable->header->length; j++)
        {
            int size = strlen(queryTable->header->values[j]);
            if(strncmp(queryTable->header->values[j], attributes[i], size) == 0 )
            {
                attributeIndex = j;
                break;
            }
        }

        if(attributeIndex >= 0)
        {
            collumnsToSave[collumnsToSaveIndex] = attributeIndex;
            collumnsToSaveIndex++;
            headerSize++;
        }
    }

    QueryTable* newQueryTable = malloc(sizeof(QueryTable));
    //newQueryTable->header = malloc(sizeof(Row));
    //newQueryTable->header->values = malloc(sizeof(char*)*collumnsToSaveIndex);

    int size = strlen(queryTable->tableName);
    newQueryTable->tableName = malloc(sizeof(char)*(size + 1));
    strncpy(newQueryTable->tableName, queryTable->tableName, size);
    newQueryTable->tableName[size] = '\0';

    // Cria o novo header
    newQueryTable->header = NULL;
    row_modify(&newQueryTable->header, queryTable->header, collumnsToSave, collumnsToSaveIndex);
    newQueryTable->header->length = headerSize;

    newQueryTable->type = NULL;
    row_modify(&newQueryTable->type, queryTable->type, collumnsToSave, collumnsToSaveIndex);
    newQueryTable->type->length = headerSize;

    if(queryTable->numberOfRows == 0)
    {
        newQueryTable->rows = NULL;
        newQueryTable->numberOfRows = 0;
    }
    else
    {
        newQueryTable->rows = malloc(sizeof(Row*)*queryTable->numberOfRows);
        for(int i = 0; i < queryTable->numberOfRows; i++)
        {
            newQueryTable->rows[i] = NULL;
            row_modify(&newQueryTable->rows[i], queryTable->rows[i], collumnsToSave, headerSize);
        }
        newQueryTable->numberOfRows = queryTable->numberOfRows;
    }


    int newPrimaryKeys[numberOfAttributes];
    int numberOfPKs = 0;
    for(int i = 0; i < queryTable->numberOfPrimaryKeys; i++)
    {
        for(int j = 0; j < collumnsToSaveIndex; j++)
        {
            if(collumnsToSave[j] == queryTable->primaryKeysIndex[i])
            {
                newPrimaryKeys[numberOfPKs] = queryTable->primaryKeysIndex[i];
                numberOfPKs++;
            }
        }
    }

    newQueryTable->primaryKeysIndex = malloc(sizeof(int)*numberOfPKs);
    for(int i = 0; i < numberOfPKs; i++)
        newQueryTable->primaryKeysIndex[i] = newPrimaryKeys[i];
    newQueryTable->numberOfPrimaryKeys = numberOfPKs;

    queryTable_free(queryTable);
    queryTable = newQueryTable;

    *ret = queryTable;
    return 0;
}


int selection(QueryTable** ret, QueryTable* queryTable, char* conditions[MAX_CONDITIONS_MATCH][3], int numberOfConditions)
{
    // indica qual é a coluna a ser comparada para cada condição (condição1, condição2)
    int conditionsIndex[MAX_CONDITIONS_MATCH][2];

    //QueryTable* newQueryTable = queryTable_copy(queryTable);

    // Se não tiverem condições, salva todas as linhas
    if(numberOfConditions == 0)
    {
        *ret = queryTable;
        return 0;
    }
    else
    {

        for(int i = 0; i < numberOfConditions; i++)
        {
            conditionsIndex[i][0]=-1;
            conditionsIndex[i][1]=-1;
        }


        for(int i = 0; i < queryTable->header->length; i++)
        {
            for(int j = 0; j < numberOfConditions; j++)
            {
                // Se a condição corresponder a alguma coluna da tabela, salva o indice.
                if(strncmp(queryTable->header->values[i], conditions[j][0], MAX_STRING_SIZE) == 0)
                    // salva a coluna com o atributo da condição 1 de comparação
                    conditionsIndex[j][0] = i;

                if(strncmp(queryTable->header->values[i], conditions[j][2], MAX_STRING_SIZE) == 0)
                    // salva a coluna com o atributo da condição 2 de comparação
                    conditionsIndex[j][1] = i;
            }
        }
    }

    int rowsToSave[queryTable->numberOfRows];
    for(int i = 0; i < queryTable->numberOfRows; i++)
        rowsToSave[i] = -1;

    int nRows = 0;
    int saveRow = 1;

    // Realiza a seleção
    // Para todas as linhas
    for(int i = 0; i < queryTable->numberOfRows; i++)
    {
        saveRow = 1;

        // Para todas as condições que se apliquem a uma linha (Row)
        for(int j = 0; j < numberOfConditions; j++)
        {
            // Se uma das condições for um literal (não for uma coluna da tabela)
            if(conditionsIndex[j][1] == -1)
            {

                // Compara a a condição[0] com o literal da condição[2], usando o operador da condição[1]
                if(operation(queryTable->rows[i]->values[conditionsIndex[j][0]], conditions[j][2],
                        conditions[j][1], queryTable->type->values[conditionsIndex[j][0]]) == 0)
                {
                    // Se a condicao for falsa, não salva
                    saveRow = 0;
                    break;
                }

            }
            // Se for para comparar atributo de uma coluna com atributo de outra coluna
            else
            {
                // Compara uma coluna com outra coluna
                if(operation(queryTable->rows[i]->values[conditionsIndex[j][0]], queryTable->rows[i]->values[conditionsIndex[j][1]],
                        conditions[j][1], queryTable->type->values[conditionsIndex[j][0]]) == 0)
                {
                    // Se a condicao for falsa, não salva
                    saveRow = 0;
                    break;
                }
            }
        }

        if(saveRow)
        {
            rowsToSave[nRows] = i;
            nRows++;
        }
    }

    Row **newRows = NULL;
    if(nRows != queryTable->numberOfRows)
    {
        newRows = malloc(sizeof(Row*)*nRows);
        int index = 0;
        for(int i = 0; i < queryTable->numberOfRows; i++)
        {
            if(i == rowsToSave[index])
            {
                //newRows[index] = row_copy(queryTable->rows[rowsToSave[index]]);
                newRows[index] = queryTable->rows[rowsToSave[index]];
                index++;
            }
            else
                free(queryTable->rows[i]);

        }

        free(queryTable->rows);
        queryTable->rows = newRows;
        queryTable->numberOfRows = nRows;
    }

    *ret = queryTable;
    return 0;
}


int operation(char *value1, char *value2, char *sqlOperation, char* type)
{
    double num1, num2;
    char numerico = 0;

    /*
    DATATIME definido da seguinte maneira:
    aaaa-mm-dd hh:mm:ss
    ou
    aaaa/mm/dd hh:mm:ss
    */
    if(strncasecmp(type, "DATETIME", strlen("DATETIME")) == 0)
    {
        numerico = 1;
        char* str = eliminateCharacter(value1, ':');
        str = eliminateCharacter(str, ' ');
        str = eliminateCharacter(str, '/');
        str = eliminateCharacter(str, '-');
        num1 = atof(str);

        str = eliminateCharacter(value2, ':');
        str = eliminateCharacter(str, ' ');
        str = eliminateCharacter(str, '/');
        str = eliminateCharacter(str, '-');
        num2 = atof(str);
    }
    else if(strncasecmp(type, "DATE", strlen("DATE")) == 0)
    {
        numerico = 1;
        char* str = eliminateCharacter(value1, ':');
        str = eliminateCharacter(str, ' ');
        str = eliminateCharacter(str, '/');
        str = eliminateCharacter(str, '-');
        num1 = atof(str);

        str = eliminateCharacter(value2, ':');
        str = eliminateCharacter(str, ' ');
        str = eliminateCharacter(str, '/');
        str = eliminateCharacter(str, '-');
        num2 = atof(str);
    }
    else if(strncasecmp(type, "TIME", strlen("TIME")) == 0)
    {
        numerico = 1;
        char* str = eliminateCharacter(value1, ':');
        str = eliminateCharacter(str, ' ');
        str = eliminateCharacter(str, '/');
        str = eliminateCharacter(str, '-');
        num1 = atof(str);

        str = eliminateCharacter(value2, ':');
        str = eliminateCharacter(str, ' ');
        str = eliminateCharacter(str, '/');
        str = eliminateCharacter(str, '-');
        num2 = atof(str);
    }
    else if(strncasecmp(type, "FLOAT", strlen("FLOAT")) == 0)
    {
        numerico = 1;
        num1 = atof(value1);
        num2 = atof(value2);
    }
    else if(strncasecmp(type, "VARCHAR", strlen("VARCHAR")))
    {
        numerico = 0;
    }

    if(numerico)
    {
        if(strcmp(sqlOperation, "<") == 0)
        {
            if(num1 < num2)
                return 1;
        }
        else if(strcmp(sqlOperation, "<=") == 0)
        {
            if(num1 <= num2)
                return 1;
        }
        else if(strcmp(sqlOperation, ">") == 0)
        {
            if(num1 > num2)
                return 1;
        }
        else if(strcmp(sqlOperation, ">=") == 0)
        {
            if(num1 >= num2)
                return 1;
        }
        else if(strcmp(sqlOperation, "=") == 0)
        {
            if(num1 == num2)
                return 1;
        }
    }
    else
    {
        int length = strlen(value2);
        if(strcmp(sqlOperation,"=") == 0)
        {
            if(strncmp(value2, value1, length) == 0)
                return 1;
        }
    }

    return 0;
}

