#include "sql_select.h"


#ifdef MEASURE_TIME_SELECT
    double request_time;
    struct timeval begin_request, end_request;
    struct timeval begin_select, end_select;
#endif // MEASURE_TIME_SELECT


STATUS_DB sql_select(Results** ret, DBConnection* connection, char* sql)
{


#ifdef MEASURE_TIME_SELECT
    request_time = 0;
    gettimeofday(&begin_select, NULL);
#endif // MEASURE_TIME_SELECT


    // Parse sql
    consultaSelect* consulta = NULL;
    parse_sql_select(&consulta, sql);
    //print_consulta_select(*consulta);

    Results* select_results;
    select_results = malloc(sizeof(Results));
    select_results->message = NULL;
    select_results->status = NULL;
    stringAppend(&select_results->status, "ERROR");
    select_results->affected_rows = 0;
    select_results->results = NULL;
    select_results->has_more = NULL;
    stringAppend(&select_results->has_more, "False");

    int loading = 0;
    char* newCursor = NULL;
    int nRowsToLoad = 0;
    char* newSql = NULL;

    if(consulta->numberOfConditions > 0)
    {
        if(strncmp(consulta->conditions[0][0], "continue", strlen("continue")) == 0)
        {
            if(strncmp(consulta->conditions[0][1], "=", strlen("=")) != 0)
            {
                printf("\nInvalid cursor operation.\n");
                stringAppend(&select_results->message, "Invalid cursor");
                *ret = select_results;
                return SELECT_LOADING_INVALID_CURSOR;
            }

            if(strncasecmp(consulta->conditions[0][2], "true", strlen("true")) == 0)
            {
                int retSelect = selectFromJSON("select.data", &newCursor, &nRowsToLoad, &newSql); // values é somente para testes
                if(retSelect == FILE_NOT_FOUND || nRowsToLoad <= 0)
                {
                    printf("\nNo more data to return.\n");

                    stringAppend(&select_results->message, "No more data to return");
                    *ret = select_results;
                    return SELECT_LOADING_NO_MORE_DATA;
                }

                loading = 1;
                restRequest_setCursor(&connection->request, newCursor);

                sql = newSql;

                consultaSelect* newConsulta = NULL;
                parse_sql_select(&newConsulta, newSql);

                for(int i = 0; i < newConsulta->numberOfAttributes; i++)
                {
                    if(strlen(newConsulta->attributes[i]) != strlen(consulta->attributes[i]))
                    {
                        //printf("\nNew statement tables are different from the last.\n");
                        consultaSelect_free(consulta);

                        stringAppend(&select_results->message, "No Continuation available for the statement");
                        *ret = select_results;
                        return ERROR;
                    }
                    if(strncmp(newConsulta->attributes[i], consulta->attributes[i], strlen(consulta->attributes[i])) != 0)
                    {
                        //printf("\nNew statement conditions are different the one to continue.\n");
                        consultaSelect_free(consulta);

                        stringAppend(&select_results->message, "No Continuation available for the statement");
                        *ret = select_results;
                        return ERROR;
                    }
                }

                for(int i = 0; i < newConsulta->numberOfTables; i++)
                {
                    if(strlen(consulta->tables[i]) != strlen(newConsulta->tables[i]))
                    {
                        //printf("\nNew statement tables are different from the last.\n");
                        consultaSelect_free(consulta);

                        stringAppend(&select_results->message, "No Continuation available for the statement");
                        *ret = select_results;
                        return ERROR;
                    }
                    if(strncmp(newConsulta->tables[i], consulta->tables[i], strlen(consulta->tables)) != 0)
                    {
                        //printf("\nNew statement tables are different from the last.\n");
                        consultaSelect_free(consulta);

                        stringAppend(&select_results->message, "No Continuation available for the statement");
                        *ret = select_results;
                        return ERROR;
                    }
                }

                consultaSelect_free(consulta);
                consulta = newConsulta;

            }
        }
    }
    else if(selectFromJSON("select.data", &newCursor, &nRowsToLoad, &newSql) == 0)
    {
        free(newSql);
        free(newCursor);
        remove("select.data");
    }


    QueryTable* queryTable = NULL;
    QueryTable* tempQueryTable = NULL;

    Table* table = NULL;


    // Verifica se a tabela da consulta existe no banco de dados
    findTable(&table, connection->database, consulta->tables[0]);
    if(table == NULL)
    {
        stringAppend(&select_results->message, "Table ");
        stringAppend(&select_results->message, consulta->tables[0]);
        stringAppend(&select_results->message, " not found");
        select_results->affected_rows = 0;
        select_results->results = NULL;
        free(select_results->has_more);
        select_results->has_more = NULL;
        stringAppend(&select_results->has_more, "False");

        consultaSelect_free(consulta);

        *ret = select_results;
        return TABLE_NOT_FOUND;

    }

    int attributePK = 0; // Indica se todos os atributos são Primary Keys
    int conditionPK = 0; // Indica se todos as condições são Primary Keys
    int indexPKs[table->numberOfPrimaryKeys];
    int selectAll = 0;   // Indica se é Select *
    int nPK = 0;
    int indexCondition[table->numberOfPrimaryKeys];

    if(consulta->numberOfAttributes == 1)
    {
        if(strncmp(consulta->attributes[0], "*", strlen("*")) == 0)
        {
            selectAll = 1;
        }
    }

    // verifica se todos os atributos são primary keys.
    for(int i = 0; i < consulta->numberOfAttributes; i++)
    {
        for(int j = 0; j < table->numberOfPrimaryKeys; j++ )
        {
            attributePK = 0;
            if(strncmp(table->attributes[table->indexPrimaryKeys[j]], consulta->attributes[i], strlen(table->attributes[j])) == 0 )
            {
                attributePK = 1;
                break;
            }
        }

        if(attributePK == 0)
            break;
    }



    // Verifica se todas as condições pertencem as Primary Keys

    // Se for igual a zero, basta salvar os atributos selecionados
    if(consulta->numberOfConditions > 0)
    {
        // Verifica se todas as condições são primary keys.
        for(int i = 0; i < consulta->numberOfConditions; i++)
        {
            for(int j = 0; j < table->numberOfPrimaryKeys; j++)
            {
                conditionPK = 0;
                if(strncmp(table->attributes[table->indexPrimaryKeys[j]], consulta->conditions[i][0], strlen(table->attributes[j])) == 0 )
                {
                    conditionPK = 1;

                    if(strncmp(consulta->conditions[i][1], "=", strlen("=")) == 0)
                    {
                        indexPKs[nPK] = table->indexPrimaryKeys[j];
                        indexCondition[nPK] = i;
                        nPK++;
                    }
                    break;
                }
            }

            if(conditionPK == 0)
                break;

            int attributeIndex2 = 0;
            table_getAttributeIndex(&attributeIndex2, table, consulta->conditions[i][2]);
            // Se a segunda condição for uma coluna da tabela
            if(attributeIndex2 >= 0)
            {
                for(int j = 0; j < table->numberOfPrimaryKeys; j++)
                {
                    conditionPK = 0;
                    if(attributeIndex2 == table->indexPrimaryKeys[j])
                    {
                        conditionPK = 1;
                        break;
                    }
                }
                if(conditionPK == 0)
                    break;
            }
        }
    }
    else
        conditionPK =1;



    // Verifica otimizacao de buscar por prefixo
    // Verify the sequence of primary keys used as conditions
    int optimizeN = 0;
    char* optimizeUrl = NULL;
    if(nPK > 0)
    {
        for(int i = 0; i < nPK; i++)
        {
            for(int j = 0; j < nPK; j++)
            {
                if(indexPKs[j] == optimizeN)
                {
                    optimizeN++;
                    stringAppend(&optimizeUrl, ";");
                    stringAppend(&optimizeUrl, consulta->conditions[indexCondition[j]][2]);

                }
            }
        }
    }


    // Se attributePK = 1, signfica que todos os atributos da seleção são PrimaryKeys
    // Se attributePK = 0, significa que algum atributo não é PK ou é Select *
    // Se conditionPK = 1, todas as condições que não sao literais são da PK
    // Se conditionPK = 0, alguma ou todas as condições não são PK

    // Se os dois são compostos apenas de PK, significa que só é necessário pegar do banco as keys
    if(attributePK == 1 && conditionPK == 1)
    {

        QueryTable* tempQueryTableProjection;


        queryTable_create(&tempQueryTable, table, NULL, 0);
        projection(&tempQueryTable, tempQueryTable, table->primaryKeys, table->numberOfPrimaryKeys);

        queryTable_copy(&queryTable, tempQueryTable);
        projection(&queryTable, queryTable, consulta->attributes, consulta->numberOfAttributes);

#ifdef DEBUG_SELECT
            printf("\nNew Header.\n");
            queryTable_print(queryTable);
#endif

        do
        {
            char* previousCursor = NULL;
            if(connection->request->cursor != NULL)
                copyString(&previousCursor, connection->request->cursor);

            // Pega chaves do banco
            getKeys(&tempQueryTable, connection, consulta->tables[0]);
#ifdef DEBUG_SELECT
            printf("\n\nPegou keys.\n\n");
            queryTable_print(tempQueryTable);
#endif

            selection(&tempQueryTable, tempQueryTable, consulta->conditions, consulta->numberOfConditions);

#ifdef DEBUG_SELECT
            printf("\n\nApos selection.\n\n");
            queryTable_print(tempQueryTable);
#endif

            // Copia a tempQueryTable
            // não pode destruir tempQueryTable, pois é necessária para saber quais sao as chaves primarias
            // durante o getKeys

            queryTable_copy(&tempQueryTableProjection, tempQueryTable);
            for(int i = 0; i < tempQueryTable->numberOfRows; i++)
                row_free(tempQueryTable->rows[i]);
            free(tempQueryTable->rows);
            tempQueryTable->rows = NULL;


            projection(&tempQueryTableProjection, tempQueryTableProjection, consulta->attributes, consulta->numberOfAttributes);
#ifdef DEBUG_SELECT
            printf("\n\nApos projection.\n\n");
            queryTable_print(tempQueryTableProjection);
#endif


            int maxReturnElements = MAX_DATA_CURSOR;
            if(loading == 1)
            {
                int ret = analyseLoading(tempQueryTableProjection, &queryTable, connection, select_results, sql, nRowsToLoad, maxReturnElements);
                loading = 0;
                if(ret == 1)
                    break;
            }
            else
            {
                int ret = analyseSaving(tempQueryTableProjection, &queryTable, connection, select_results, sql, maxReturnElements, previousCursor);
                if(ret == 1)
                {
                    if(previousCursor != NULL)
                        free(previousCursor);
                    break;
                }
            }

#ifdef DEBUG_SELECT_TABLE
            printf("\n\nTabela final até o momento.\n\n");
            queryTable_print(queryTable);
#endif

        }while(connection->request->cursor != NULL);

#ifdef PRINT_RESULTS_SELECT
        printf("\n\nTabela final.\n\n");
        queryTable_print(queryTable);
#endif

        queryTable_free(tempQueryTable);

    }

    // Se algum atributo não é PK, mas as condições são, podemos fazer a
    else if(attributePK == 0 && conditionPK == 1)
    //seleção das linhas apenas utilizando as chaves, e depois pegar somente os valores ques corresponderem àquelas chave
    {
        QueryTable* tempQueryTableProjection;
        queryTable_create(&tempQueryTable, table, NULL, 0);
        queryTable_copy(&queryTable, tempQueryTable);

        if(selectAll == 0)
            projection(&queryTable, queryTable, consulta->attributes, consulta->numberOfAttributes);
        else
            projection(&queryTable, queryTable, table->attributes, table->numberOfAttributes);


#ifdef DEBUG_SELECT
        printf("\nNew Header.\n");
        queryTable_print(queryTable);
#endif

        do
        {
            char* previousCursor = NULL;
            if(connection->request->cursor != NULL)
                copyString(&previousCursor, connection->request->cursor);
            // Pega chaves do banco
            getKeys(&tempQueryTable, connection, consulta->tables[0]);
#ifdef DEBUG_SELECT
            printf("\n\nPegou keys.\n\n");
            queryTable_print(tempQueryTable);
#endif


            selection(&tempQueryTable, tempQueryTable, consulta->conditions, consulta->numberOfConditions);
#ifdef DEBUG_SELECT
            printf("\n\nApos selection.\n\n");
            queryTable_print(tempQueryTable);
#endif

            getValues(&tempQueryTable, connection);
#ifdef DEBUG_SELECT
            printf("\n\nPegou valores.\n\n");
            queryTable_print(tempQueryTable);
#endif

            // Copia a tempQueryTable
            // não pode destruir tempQueryTable, pois é necessária para saber quais sao as chaves primarias
            // durante o getKeys

            queryTable_copy(&tempQueryTableProjection, tempQueryTable);
            for(int i = 0; i < tempQueryTable->numberOfRows; i++)
                row_free(tempQueryTable->rows[i]);
            free(tempQueryTable->rows);
            tempQueryTable->rows = NULL;


            if(selectAll == 0)
                projection(&tempQueryTableProjection, tempQueryTableProjection, consulta->attributes, consulta->numberOfAttributes);
            else
                projection(&tempQueryTableProjection, tempQueryTableProjection, table->attributes, table->numberOfAttributes);
#ifdef DEBUG_SELECT
            printf("\n\nApos projection.\n\n");
            queryTable_print(tempQueryTable);
#endif


            int maxReturnElements = MAX_DATA_CURSOR;
            if(loading == 1)
            {
                int ret = analyseLoading(tempQueryTableProjection, &queryTable, connection, select_results, sql, nRowsToLoad, maxReturnElements);
                loading = 0;
                if(ret == 1)
                    break;
            }
            else
            {
                int ret = analyseSaving(tempQueryTableProjection, &queryTable, connection, select_results, sql, maxReturnElements, previousCursor);
                if(ret == 1)
                {
                    if(previousCursor != NULL)
                        free(previousCursor);
                    break;
                }
            }

#ifdef DEBUG_SELECT_TABLE
            printf("\n\nTabela final até o momento.\n\n");
            queryTable_print(queryTable);
#endif

        }while(connection->request->cursor != NULL);

        queryTable_free(tempQueryTable);

    }

    else
    {
        QueryTable* tempQueryTableProjection;
        queryTable_create(&tempQueryTable, table, NULL, 0);
        queryTable_copy(&queryTable, tempQueryTable);
        if(selectAll == 0)
            projection(&queryTable, queryTable, consulta->attributes, consulta->numberOfAttributes);
        else
            projection(&queryTable, queryTable, table->primaryKeys, table->numberOfPrimaryKeys);

#ifdef DEBUG_SELECT
        printf("\nNew Header.\n");
        queryTable_print(queryTable);
#endif

        do
        {
            char* previousCursor = NULL;
            if(connection->request->cursor != NULL)
                copyString(&previousCursor, connection->request->cursor);
            // Pega chaves do banco
            getKeys(&tempQueryTable, connection, consulta->tables[0]);

#ifdef DEBUG_SELECT
            printf("\n\nPegou keys.\n\n");
            queryTable_print(tempQueryTable);
#endif
            getValues(&tempQueryTable, connection);

#ifdef DEBUG_SELECT
            printf("\n\nPegou valores.\n\n");
            queryTable_print(tempQueryTable);
#endif

            selection(&tempQueryTable, tempQueryTable, consulta->conditions, consulta->numberOfConditions);

#ifdef DEBUG_SELECT
            printf("\n\nApos selection.\n\n");
            queryTable_print(tempQueryTable);
#endif

            // Copia a tempQueryTable
            // não pode destruir tempQueryTable, pois é necessária para saber quais sao as chaves primarias
            // durante o getKeys

            queryTable_copy(&tempQueryTableProjection, tempQueryTable);
            for(int i = 0; i < tempQueryTable->numberOfRows; i++)
                row_free(tempQueryTable->rows[i]);
            free(tempQueryTable->rows);
            tempQueryTable->rows = NULL;

            if(selectAll == 0)
                projection(&tempQueryTableProjection, tempQueryTableProjection, consulta->attributes, consulta->numberOfAttributes);
            else
                projection(&tempQueryTableProjection, tempQueryTableProjection, table->attributes, table->numberOfAttributes);

#ifdef DEBUG_SELECT
            printf("\n\nApos projection.\n\n");
            queryTable_print(tempQueryTableProjection);
#endif

            int maxReturnElements = MAX_DATA_CURSOR;
            if(loading == 1)
            {
                int ret = analyseLoading(tempQueryTableProjection, &queryTable, connection, select_results, sql, nRowsToLoad, maxReturnElements);
                loading = 0;
                if(ret == 1)
                    break;
            }
            else
            {
                int ret = analyseSaving(tempQueryTableProjection, &queryTable, connection, select_results, sql, maxReturnElements, previousCursor);
                if(ret == 1)
                {
                    if(previousCursor != NULL)
                        free(previousCursor);
                    break;
                }
            }


#ifdef DEBUG_SELECT_TABLE
            printf("\n\nTabela final até o momento.\n\n");
            queryTable_print(queryTable);
#endif

        }while(connection->request->cursor != NULL);

        queryTable_free(tempQueryTable);

    }



#ifdef PRINT_RESULTS_SELECT
    printf("\n\nFinal QueryTable.\n\n");
    queryTable_print(queryTable);
#endif

    freeTable(table);
    consultaSelect_free(consulta);

    select_results->message = NULL;
    free(select_results->status);
    select_results->status = NULL;
    stringAppend(&select_results->status, "OK");
    select_results->affected_rows = queryTable->numberOfRows;
    select_results->results = queryTable;

    // O resultado do select esta em select_result
    char* select_result = NULL;
    JSONFromResults(&select_result, select_results);

#ifdef PRINT_RESULTS
    printf("\n\nSelect Results:\n\n %s", select_result);
#endif //PRINT_RESULTS


#ifdef MEASURE_TIME_SELECT

    gettimeofday(&end_select, NULL);

    double total_time = (end_select.tv_sec - begin_select.tv_sec) +
              ((end_select.tv_usec - begin_select.tv_usec)/1000000.0);
    double select_time = total_time - request_time;

    //printf("\nSelect Time: %f\n", select_time);
    //printf("Request Time: %f\n", request_time);
    //printf("Total Time: %f\n", request_time);

    char time_select[20];
    sprintf(time_select, "%f", select_time);

    char time_request[20];
    sprintf(time_request, "%f", request_time);

    char time_n_keys[20];
    sprintf(time_n_keys, "%d", select_results->results->numberOfRows);


    char* select_file = "select_selectTime.txt";

    char* select_n = NULL;
    stringAppend(&select_n, time_n_keys);
    stringAppend(&select_n, " : ");
    stringAppend(&select_n, time_select);

    // gera uma resposta da forma:
    // numero_linhas_selecao:tempo_selecao
    append_string_to_file(select_file, select_n);

    free(select_n);

    char* request_file = "select_requestTime.txt";

    char* request_n = NULL;
    stringAppend(&request_n, time_n_keys);
    stringAppend(&request_n, " : ");
    stringAppend(&request_n, time_request);

    // gera uma resposta da forma:
    // numero_linhas_selecao:tempo_total_requisicao
    append_string_to_file(request_file, request_n);

    free(request_n);

#endif // MEASURE_TIME_SELECT


    free(select_result);

    *ret = select_results;
    return SELECT_NO_ERROR;
}

int separeteKeys(QueryTable** ret, QueryTable* queryTable, char keys[MAX_DATA_CURSOR][MAX_STRING_SIZE], int nKeysToGet, int toGET[MAX_DATA_CURSOR])
{
    Row** rows = malloc(sizeof(Row*)*nKeysToGet);
    int nRows = 0;

    for(int i = 0; i < nKeysToGet; i++)
    {
        if(keys[toGET[i]] != NULL)
        {
            rows[i] = NULL;
            row_create(&rows[i], queryTable, keys[toGET[i]], NULL);
            nRows++;
        }
    }

   queryTable_addNRows(&queryTable, queryTable, rows, nRows);

    *ret = queryTable;
    return 0;
}

// Adiciona os valores obtidos nessa consulta a QueryTable
STATUS_DB getValues(QueryTable** queryTable, DBConnection* connection)
{
    //char keys[MAX_DATA_CURSOR][MAX_STRING_SIZE], int nKeysToGET, int toGET[MAX_DATA_CURSOR]
    if((*queryTable)->numberOfRows == 0)
    {
        return NO_ERROR;
    }
    MemoryStruct chunk[(*queryTable)->numberOfRows];
    Row** rows = malloc(sizeof(Row*)*(*queryTable)->numberOfRows);
    int nRows = 0;

    char* key[(*queryTable)->numberOfRows];
    RESTRequest* newRequest[(*queryTable)->numberOfRows];
    for(int i = 0; i < (*queryTable)->numberOfRows; i++)
    {
        restRequest_copy(&newRequest[i], connection->request);
        key[i] = NULL;
    }

    for(int i = 0; i < (*queryTable)->numberOfRows; i++)
    {
        stringAppend(&key[i], (*queryTable)->tableName);
        for(int j = 0; j < (*queryTable)->numberOfPrimaryKeys; j++)
        {
            stringAppend(&key[i] , ";");
            stringAppend(&key[i], (*queryTable)->rows[i]->values[(*queryTable)->primaryKeysIndex[j]]);
        }

        // Replace invalid caracters
        char* valid_key = NULL;
        format_key(&valid_key, key[i]);

        char* urlKey = NULL;
        stringConcat(&urlKey, valid_key, "/");
        char* url = NULL;
        stringConcat(&url, connection->request->url, urlKey);

        chunk[i].memory = calloc(1,1);  // will be grown as needed by the realloc above
        chunk[i].size = 0;    // no data at this point


        restRequest_copy(&newRequest[i], connection->request);
        restRequest_setUrl(&newRequest[i], url);

        free(urlKey);
        free(url);
    }
#ifdef MEASURE_TIME_SELECT
        gettimeofday(&begin_request, NULL);
#endif // MEASURE_TIME_SELECT

    for(int i = 0; i < (*queryTable)->numberOfRows; i++)
    {
        GET( newRequest[i], &chunk[i]);
    }

#ifdef MEASURE_TIME_SELECT
        gettimeofday(&end_request, NULL);
        request_time += (end_request.tv_sec - begin_request.tv_sec) +
              ((end_request.tv_usec - begin_request.tv_usec)/1000000.0);
#endif // MEASURE_TIME_SELECT


#ifdef PRINT_RESULTS_SELECT
        printf("\nKey: %s\n", keys[toGET[i]]);
        //printf("value: %s \n", chunk.memory);
#endif
    for(int i = 0; i < (*queryTable)->numberOfRows; i++)
    {

        int nValues = (*queryTable)->header->length - (*queryTable)->numberOfPrimaryKeys;
        if(nValues > 0)
        {
            char* values = NULL;
            getValuesFromJSON(&values, key[i], chunk[i].memory, nValues);
            rows[i] = NULL;
            row_create(&rows[i], (*queryTable), key[i], values);
            nRows++;
            free(values);
        }

        restRequest_free(newRequest[i]);
        free(chunk[i].memory);
        free(key[i]);


    }
    for(int i = 0; i < (*queryTable)->numberOfRows; i++)
        row_free((*queryTable)->rows[i]);
    free((*queryTable)->rows);
    (*queryTable)->rows = NULL;
    (*queryTable)->numberOfRows = 0;

    if(nRows > 0)
        queryTable_addNRows(queryTable, (*queryTable), rows, nRows);


    return NO_ERROR;
}

STATUS_DB getKeys(QueryTable** queryTable, DBConnection* connection, char* table)
{

    char keys[MAX_DATA_CURSOR][MAX_STRING_SIZE];
    MemoryStruct chunk;
    chunk.memory = calloc(1,1);
    chunk.size = 0;

    char* urlKey = NULL;
    char* url = NULL;
    if(connection->request->cursor != NULL)
    {
        stringConcat(&urlKey, "?cursor=", connection->request->cursor);
    }
    else
    {
        stringConcat(&urlKey, "?key_prefix=", table);
    }
    stringConcat(&url, connection->request->url, urlKey);

#ifdef DEBUG_SELECT
    printf("url: %s\n\n", url);
#endif //DEBUG_SELECT


    RESTRequest* newRequest = NULL;
    restRequest_copy(&newRequest, connection->request);
    restRequest_setUrl(&newRequest, url);

#ifdef MEASURE_TIME_SELECT
        gettimeofday(&begin_request, NULL);
#endif // MEASURE_TIME_SELECT

        GET( newRequest, &chunk);

#ifdef MEASURE_TIME_SELECT
        gettimeofday(&end_request, NULL);
        request_time += (end_request.tv_sec - begin_request.tv_sec) +
              ((end_request.tv_usec - begin_request.tv_usec)/1000000.0);
#endif // MEASURE_TIME_SELECT



#ifdef DEBUG_SELECT
    printf("value: %s \n", chunk.memory);
#endif //DEBUG_SELECT

    int nKeys = 0;
    getKeysFromJSON(&nKeys, chunk.memory, keys);
    char* cursor = NULL;
    getCursorFromJSON(&cursor, chunk.memory);
    restRequest_setCursor(&connection->request, cursor);

    if((*queryTable)->rows != NULL)
    {
        for(int i = 0; i < (*queryTable)->numberOfRows; i++)
            row_free((*queryTable)->rows[i]);

        free((*queryTable)->rows);

    }
    (*queryTable)->rows = malloc(sizeof(Row)*nKeys);
    for(int i = 0; i < nKeys; i++)
    {
        (*queryTable)->rows[i] = NULL;
        row_create(&(*queryTable)->rows[i], (*queryTable), keys[i], NULL);
    }
    (*queryTable)->numberOfRows = nKeys;

    restRequest_free(newRequest);
    free(urlKey);
    free(url);
    free(chunk.memory);
    free(cursor);

    return NO_ERROR;
}

int getAttributeFromKey(char** ret, char* key, int index)
{
    if(key == NULL)
        return -1;
    if(index < 0)
        return -1;

    int keySize = strlen(key);
    int cont = 0;
    int keyStart = 0;
    int keyEnd = keySize;
    int startFound = 0;

    for(int i = 0; i < keySize; i++)
    {
        if(key[i] == ';')
            cont++;

        if(cont == index + 1)
        {
            keyEnd = i-1;
            break;
        }

        if(cont == index && startFound == 0)
        {
            keyStart = i+1;
            startFound = 1;
        }
    }
    int newStringSize = keyEnd - keyStart+1;
    char* newString = malloc(sizeof(char)*(newStringSize + 1));
    strncpy(newString, key + keyStart, newStringSize);
    newString[newStringSize] = '\0';

    *ret = newString;
    return 0;
}



int analyseLoading(QueryTable* queryTableProjection, QueryTable** queryTable, DBConnection* connection, Results* results, char* sql, int nRowsToLoad, int maxReturnElements)
{
    Row** tempRows = malloc(sizeof(Row*)*(nRowsToLoad));
    int rowsOffset = queryTableProjection->numberOfRows - nRowsToLoad;
    if(rowsOffset < 0)
    {
        rowsOffset = 0;
        nRowsToLoad = queryTableProjection->numberOfRows;
    }
    
    for(int i = rowsOffset,  j = 0; i < rowsOffset+ nRowsToLoad; i++, j++)
    {
        tempRows[j] = queryTableProjection->rows[i];
    }

    for(int i = 0; i < rowsOffset; i++)
        row_free(queryTableProjection->rows[i]);
    free(queryTableProjection->rows);
    queryTableProjection->rows = NULL;

    queryTable_addNRows(queryTable, *queryTable, tempRows, nRowsToLoad);
    queryTable_free(queryTableProjection);


    if(nRowsToLoad == maxReturnElements)
    {

        if(connection->request->cursor != NULL)
        {
            JSONFromSelect("select.data", connection->request->cursor, maxReturnElements, sql);//connection->request->cursor);
            free(results->has_more);
            results->has_more = NULL;
            stringAppend(&results->has_more, "True");
        }
        return 1;
    }
    else
    {
        remove("select.data");
    }

    return 0;

}

int analyseSaving(QueryTable* queryTableProjection, QueryTable** queryTable, DBConnection* connection, Results* results, char* sql, int maxReturnElements, char* previousCursor)
{
    if((*queryTable)->numberOfRows + queryTableProjection->numberOfRows < maxReturnElements)
    {

        queryTable_addNRows(queryTable, *queryTable, queryTableProjection->rows, queryTableProjection->numberOfRows);

        queryTableProjection->rows = NULL;
        queryTable_free(queryTableProjection);
        return 0;
    }
    else if((*queryTable)->numberOfRows + queryTableProjection->numberOfRows == maxReturnElements)
    {

        queryTable_addNRows(queryTable, *queryTable, queryTableProjection->rows, queryTableProjection->numberOfRows);

        queryTableProjection->rows = NULL;
        queryTable_free(queryTableProjection);


        if(connection->request->cursor != NULL)
        {
            JSONFromSelect("select.data", connection->request->cursor, maxReturnElements, sql);//connection->request->cursor);
            free(results->has_more);
            results->has_more = NULL;
            stringAppend(&results->has_more, "True");
        }
        return 1;

    }
    else
    {
        int nRowsToReturn = maxReturnElements - (*queryTable)->numberOfRows;
        int nRowsToSave = queryTableProjection->numberOfRows - nRowsToReturn;

        queryTable_addNRows(queryTable, *queryTable, queryTableProjection->rows, nRowsToReturn);
        queryTableProjection->rows = NULL;
        queryTable_free(queryTableProjection);


        if(connection->request->cursor != NULL)
        {
            JSONFromSelect("select.data", previousCursor, nRowsToSave, sql);//connection->request->cursor);
            free(results->has_more);
            results->has_more = NULL;
            stringAppend(&results->has_more, "True");
        }
        return 1;
    }
}
