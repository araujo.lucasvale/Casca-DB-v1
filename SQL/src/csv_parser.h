/**
 * @file
 * @author Davi Boberg
 * @date 24/01/2018
 *
 * \brief A simple CSV parser.
 *
 */

#ifndef CSV_PARSER_H
#define CSV_PARSER_H

#include <stdio.h>
#include <stdlib.h>
#include "my_lib.h"
#include "rest.h"

/**
 * @brief Get a specif value given its index.
 *
 *
 * @author Davi Boberg
 * @date 28/2/2009
 *
 * @param ret   Field value required.
 * @param line  One CSV line.
 * @param ret   Desired field index.
 */
STATUS_DB getField(char** ret, char* line, int num);

/**
 * @brief Return all fields on a line of CSV.
 *
 * @author Davi Boberg
 * @date 28/2/2009
 *
 * @param ret   Array of strings with all fields values.
 * @param line  One CSV line.
 */
STATUS_DB getAllFields(char*** ret, char* line);

/**
 * @brief Return the number o fields in a given line of a CSV.
 *
 * @author Davi Boberg
 * @date 28/2/2009
 *
 * @param line  One CSV line.
 */
STATUS_DB getNumberOfFields(int* ret, char* line);




#endif //CSV_PARSER_H