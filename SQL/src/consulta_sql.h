/**
 * @file
 * @author Davi Boberg
 * @date 24/01/2018
 *
 * \brief Parser.
 *
 * Parser from SQL to a internal struct.
 *
 */


#ifndef CONSULTA_SQL_H
#define CONSULTA_SQL_H


#include <stdio.h>
#include <stdlib.h>
#include <regex.h>
#include <string.h>
#include <assert.h>
#include <string.h>
#include "my_lib.h"
#include "dic_dados.h"
#define _GNU_SOURCE
char *strcasestr(const char *haystack, const char *needle);


#define MAX_ATTRIBUTES_MATCH 4
#define MAX_TABLES_MATCH 4
#define MAX_CONDITIONS_MATCH 4

enum type
{
    SQL_SELECT = 0,
    SQL_INSERT,
    SQL_CREATE_TABLE,
    SQL_DELETE,
    SQL_NO_MATCH

};


typedef struct sql_select{

    //Vari�vel: Atributos
    //Vetor de strings, cada posi��o cont�m um atributo extra�do do SQL-> SELECT x1,x2,x3...
    //No formato Tabela.Atributo
    char* attributes[MAX_ATTRIBUTES_MATCH];
    int numberOfAttributes;
    //Vari�vel: Tabelas
    //Vetor de strings, cada posi��o cont�m uma tabela extra�da do SQL-> FROM x1,x2,x3...
    char* tables[MAX_ATTRIBUTES_MATCH];
    int numberOfTables;

    //Vari�vel: Condicoes
    //Primeira posi��o: Atributo da primeira tabela
    //Segunda posi��o: Condi��o
    //Terceira posi��o: Atributo da segunda tabela
    char* conditions[MAX_CONDITIONS_MATCH][3];
    int numberOfConditions;

    // Variavel: Operador
    char* operators[MAX_CONDITIONS_MATCH-1];

}consultaSelect;

typedef struct sql_insert{

    //Vari�vel: Tabela
    //String contendo a tabela candidata para a insercao de dados
    char* table;
    //Vari�vel: Valores
    //Vetor de strings, cada posi��o cont�m um valor extra�do do SQL-> Values ('x1','x2','x3'...)
    char** values;
    int numberOfValues;

}consultaInsert;

/**
 * @brief Parser of Select statements.
 *
 *
 * @author Davi Boberg
 * @date 28/2/2009
 *
 * @param ret   struct of a SQL select processed.
 * @param sql   SQL statement.
 */
int parse_sql_select(consultaSelect** ret, char* sql);

/**
 * @brief Parser of Insert statements.
 *
 *
 * @author Davi Boberg
 * @date 28/2/2009
 *
 * @param ret   struct of a SQL Insert processed.
 * @param sql   SQL statement.
 */
int parse_sql_insert(consultaInsert** ret, char* sql);

/**
 * @brief Parser of Create Table statements.
 *
 *
 * @author Davi Boberg
 * @date 28/2/2009
 *
 * @param ret   struct of a SQL Create Table processed.
 * @param sql   SQL statement.
 */
int parse_sql_create_table(Table** ret, char* sql);

void print_consulta_select(consultaSelect c);

void print_consulta_insert(consultaInsert c);

/**
 * @brief Check what type of SQL Statements is.
 *
 *
 * @author Davi Boberg
 * @date 28/2/2009
 *
 * @param sql   SQL statement.
 */
STATUS_DB check_type(int* ret,char* sql);

int consultaSelect_free(consultaSelect* consulta);

int consultaInsert_free(consultaInsert* consulta);



char* str_between_delimiter(char* str, const char delim);

List* str_split_between(char* a_str, const char a_delim);

List* str_split(char* a_str, const char a_delim);

char* substring(char* str, int ini, int fim, char delimiter);

int get_match(regex_t* reg, char* sql, char** user_ptr, int group);

#endif // CONSULTA_SQL_H
