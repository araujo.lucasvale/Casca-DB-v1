/**
 * @file
 * @author Davi Boberg
 * @date 24/01/2018
 *
 * \brief Process insertions.
 *
 *
 */

#ifndef SQL_INSERT_H
#define SQL_INSERT_H

#include "db.h"
#include "dic_dados.h"
#include "rest.h"

/// Execute query Insert Into
STATUS_DB sql_insert(Results** ret, DBConnection* connection, char* sql);

//char* formatKeyAttribute(char* keyAttribute);

STATUS_DB format_key(char**ret, char* key);

STATUS_DB applySpecialOperation(DBConnection* connection, char*** values, Table* table);

// Not complete, do not parse query
// Execute query Delete From
void sql_delete(DBConnection* connection, char* sql);

#endif // SQL_INSERT_H
