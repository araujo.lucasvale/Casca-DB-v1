#include "request.h"

#include "../../API.h"
#include <time.h>


long process_RestReq(void*  socket,void* msg, long tam)
{

    info_socket_t* iS = (info_socket_t*) socket;

    char* string = msg;
    if(tam==-1) //(se for desconectado pela outra ponta)
    {
        iS->socket_data_input.lastCompleteMessage = calloc((iS->socket_data_input.buffer_size+2),sizeof(char));

        memcpy(iS->socket_data_input.lastCompleteMessage,iS->socket_data_input.buffer ,iS->socket_data_input.buffer_size);
        
         string = iS->socket_data_input.lastCompleteMessage;
        string[iS->socket_data_input.buffer_size] ='\0';
        sem_post(iS->GPsem);

        while(iS->socket_data_input.lastCompleteMessage!=NULL)
        {
          usleep(10000);// esperando a mensagem ser copiada

        }
       
        
        return tam;
    }

    return 0;
}   








/// Consulta rest GET com o banco de dados
/// Retorno: CURLcode
/// Parametros:
///      request:
///          Tipo: RESTRequest*
///          Descrição: Armazena os parametros para realizar a comunicação com o banco
///                     (url, headers)
///      chunk:
///          Tipo: struct MemoryStruct*
///          Descrição: Armazena a mensagem de retorno do banco de dados
int GET(RESTRequest* connection, struct MemoryStruct* chunk)
{
    
    return REST("GET", connection, NULL, chunk);

}

/// Consulta rest POST com o banco de dados
/// Retorno: void
/// Parametros:
///      request:
///          Tipo: RESTRequest*
///          Descrição: Armazena os parametros para realizar a comunicação com o banco
///                     (url, headers)
///      data:
///          Tipo: char*
///          Descrição: chave a ser adicionada ao banco de dados
///      chunk:
///          Tipo: struct MemoryStruct*
///          Descrição: Armazena a mensagem de retorno do banco de dados
int POST(RESTRequest* connection, char* data, struct MemoryStruct* chunk)
{
  return  REST("POST", connection, data, chunk);
}

int DELETE(RESTRequest* connection, struct MemoryStruct* chunk)
{
   
    return REST("DELETE", connection, NULL, chunk); 
}

int REST(char* requestType, RESTRequest* connection, char* data, struct MemoryStruct* chunk)
{

    char stringToSend[10000];
    char page[1000];
    char ip2[100];
    int port;
    int connection_tries = 10;
    struct timespec ts; //for timed wait
    sscanf(connection->url, "http://%99[^:]:%99d/%99[^\n]", ip2, &port, page);

   
    char ip[1000];
    
 
   
    if(strlen(requestType)==3)
    {
        //GET
        sprintf(stringToSend,"%s /%s HTTP/1.1\r\nAccept: */*\r\n%s\r\n%s\r\n\r\n",requestType,page,connection->header[0],connection->header[1]);
    }
    else
    {
        //POST
        sprintf(stringToSend,"%s /%s HTTP/1.1\r\nAccept: */*\r\n%s\r\nContent-Type: application/json\r\nContent-Length:%d\r\n\r\n%s\r\n\r\n",requestType,page,connection->header[0],strlen(data),data);
    }

    
    info_socket_t* iS =API_create_socket("141.76.46.169", port,NULL, &(process_RestReq), NULL);
    
  
    while(!iS->isConnected){
       usleep(100000);
      
      if(iS->socket == 0 && connection_tries>0)
      {
        printf("connection failed\n");
        iS = API_create_socket("141.76.46.169", port,NULL, &(process_RestReq), NULL);
        connection_tries--;
        if(connection_tries == 0)
        {
            return -1; // failed to connect
        }
      }
    }

    printf("string to send:\n%s\n",stringToSend );

   API_write(iS,stringToSend,strlen(stringToSend),NULL); 

   sem_init(iS->GPsem, 1, 0);   

    if (clock_gettime(CLOCK_REALTIME, &ts) == -1)
    {
        //error    
    }
    ts.tv_sec += 10;//esperar resposta do database por no max 10s
    //sem_timedwait(iS->GPsem, &ts);
   
    sem_wait(iS->GPsem);//esperando mensagem chegar no isocket

    //avaliar se a resposta recebida está completa

    char * string = iS->socket_data_input.lastCompleteMessage;
    int tmptam = strlen(string);
    if (string == NULL||tmptam == 0)
    {

        return -1;
    }
    printf("lastCompleteMessage:\n%s\ntam:\n%d\n", iS->socket_data_input.lastCompleteMessage,strlen(string));
    char* memtocpy = calloc(strlen(string),sizeof(char));


for (int i = 0; i <strlen(string); ++i)
{
  if(string[i]=='{')
  {
    memcpy(memtocpy,&(string[i]),strlen(string)-i);
    break;
  }
}
iS->socket_data_input.lastCompleteMessage=NULL;
free(iS->socket_data_input.lastCompleteMessage);


   
 

    chunk->memory = realloc(chunk->memory, chunk->size + strlen(memtocpy) + 1);
    memcpy(&(chunk->memory[chunk->size]),memtocpy,strlen(memtocpy));
    chunk->size +=strlen(memtocpy);
    chunk->memory[chunk->size] = 0;
    free(memtocpy);
    return 0;


    

}

