/**
 * @file
 * @author Davi Boberg
 * @date 24/01/2018
 *
 * \brief .
 */

#ifndef RELATIONAL_H
#define RELATIONAL_H


#include "results.h"
#include "consulta_sql.h"

// Return a queryTable modified, with only the selected columns
int projection(QueryTable** ret, QueryTable* queryTable, char* attributes[MAX_ATTRIBUTES_MATCH], int numberOfAttributes);


// Same as projection, but the columns to keep are defined by columnsToSave
//QueryTable* projectionByIndex(QueryTable* queryTable, int* columnsToSave);

// Return a queryTable modified, with only the selected rows
int selection(QueryTable** ret, QueryTable* queryTable, char* conditions[MAX_CONDITIONS_MATCH][3], int numberOfConditions);


int operation(char *conditionValue, char *key, char* sqlOperation, char* type);


#endif /* RELATIONAL_H */

