#include "db.h"

int db_connectRest(DBConnection** ret, char* localDataBase, RESTRequest* request)
{
    DBConnection* connection = malloc(sizeof(DBConnection));
    retrieveDataBase(&connection->database, localDataBase, request);
    connection->request = NULL;
    db_setRestRequest(&connection, connection, request);

    int stringLength = strlen(localDataBase);
    connection->name = malloc(sizeof(char)*(stringLength+1));
    strncpy(connection->name, localDataBase, stringLength);
    connection->name[stringLength] = '\0';

    // curl
    //curl_connect(&connection->curlConnection);

    *ret = connection;
    return 0;
}

int db_disconnect(DBConnection* connection)
{
    // curl
  //  curl_disconnect(connection->curlConnection);

    JSONFromDataBase(connection->database, connection->name);
    freeDataBase(connection->database);
    free(connection->name);
    restRequest_free(connection->request);
    free(connection);

    return 0;
}

int retrieveDataBase(DataBase** ret, char* localDataBase, RESTRequest* request)
{
    DataBase* newDataBase = NULL;
    DataBaseFromJSON(&newDataBase, localDataBase);
    if(newDataBase == NULL)
    {
        create_DataBase(&newDataBase);
    }

    *ret = newDataBase;
    return 0;
}

int db_setRestRequest(DBConnection** ret, DBConnection* connection, RESTRequest* newRequest)
{
    if(connection == NULL)
    {
        printf("DBConnection Null.\n");
        return -1;
    }
    if(newRequest == NULL)
    {
        printf("New Request is null. Nothing done.\n");

        *ret = connection;
        return -2;
    }
    if(connection->request != NULL)
        restRequest_free(connection->request);

    connection->request = newRequest;
    *ret = connection;
    return 0;
}
