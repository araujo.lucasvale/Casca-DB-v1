#include "sql_insert.h"

//#define MEASURE_TIME_INSERT
//#define PRINT_RESULTS_INSERT
//#define PRINT_RESULTS_RETURN

#ifdef MEASURE_TIME_INSERT
    double request_time;
    struct timeval begin_request, end_request;
    struct timeval begin_insert, end_insert;
#endif // MEASURE_TIME_INSERT


STATUS_DB sql_insert(Results** ret, DBConnection* connection, char* sql)
{

#ifdef MEASURE_TIME_INSERT
    request_time = 0;
    gettimeofday(&begin_insert, NULL);
#endif // MEASURE_TIME_INSERT


    consultaInsert* consulta;
    parse_sql_insert(&consulta, sql);
    //consultaInsert_print(*consulta);

    Results* insert_results;
    insert_results = malloc(sizeof(Results));
    insert_results->message = NULL;
    insert_results->has_more = NULL;
    //free(insert_results->status);
    insert_results->status = NULL;
    stringAppend(&insert_results->status, "ERROR");
    insert_results->affected_rows = 0;
    insert_results->results = NULL;



    Table* table = NULL;
    findTable(&table, connection->database, consulta->table);
    if(table == NULL)
    {
        stringAppend(&insert_results->message, "Table ");
        stringAppend(&insert_results->message, consulta->table);
        stringAppend(&insert_results->message, " not found");
        //insert_results->status = "ERROR";
        insert_results->affected_rows = 0;
        insert_results->results = NULL;

        consultaInsert_free(consulta);
        freeTable(table);

        *ret = insert_results;
        return TABLE_NOT_FOUND;

    }


    if(consulta->numberOfValues != (table->numberOfAttributes))
    {
        stringConcat(&insert_results->message, insert_results->message, "Invalid number os attributes.");
        //insert_results->status = "ERROR";
        insert_results->affected_rows = 0;
        insert_results->results = NULL;

        freeTable(table);
        consultaInsert_free(consulta);

        *ret =  insert_results;
        return INSERT_INVALID_NUMBER_ATTRIBUTES;

    }


    applySpecialOperation(connection, &consulta->values, table);


    // Create Key and Value
    char* key = NULL;
    stringConcat(&key, "", table->name);
    for(int i = 0; i < table->numberOfPrimaryKeys; i++)
    {
        int pkindex = table->indexPrimaryKeys[i];

        stringAppend(&key, ";");
        stringAppend(&key, consulta->values[pkindex]);
    }


    char** values = malloc(sizeof(char*)*(consulta->numberOfValues - table->numberOfPrimaryKeys));


    int nValue = 0;
    for(int i = 0; i < table->numberOfAttributes; i++)
    {
        int isPK = 0;
        for(int j = 0; j < table->numberOfPrimaryKeys; j++)
        {
            if(i == table->indexPrimaryKeys[j])
            {
                isPK = 1;
                break;
            }
        }

        if(isPK == 0)
        {
            values[nValue] = consulta->values[i];
            nValue++;
        }
    }


    char* value = NULL;
    createValuesJSON(&value, values, consulta->numberOfValues - table->numberOfPrimaryKeys);


    if(value == NULL)
    {
        printf("Can not create json value.\n");
        free(key);

        for(int i = 0; i < consulta->numberOfValues; i++)
            free(consulta->values[i]);
        free(consulta->values);
        free(consulta->table);
        free(consulta);

        freeTable(table);
        consultaInsert_free(consulta);

        stringConcat(&insert_results->message, insert_results->message, "Could not create json value, invalid value.");
        //insert_results->status = "ERROR";
        insert_results->affected_rows = 0;
        insert_results->results = NULL;

        *ret = insert_results;
        return -1;
    }

    char* validKey = NULL;
    format_key(&validKey, key);


#ifdef PRINT_RESULTS_INSERT
        printf("Key: %s\nValue: \n%s\n", validKey, value);
#endif


    char* urlKey = NULL;
    stringConcat(&urlKey, connection->request->url, validKey);
    stringAppend(&urlKey, "/");

    RESTRequest* newRequest = NULL;
    restRequest_copy(&newRequest, connection->request);
    restRequest_setUrl(&newRequest, urlKey);


#ifdef DEBUG_INSERT
    printf("url: %s\n\n", newRequest->url);
#endif


    MemoryStruct chunk;
    chunk.memory = calloc(1,1);  // will be grown as needed by the realloc above
    chunk.size = 0;    // no data at this point


#ifdef MEASURE_TIME_INSERT
    gettimeofday(&begin_request, NULL);
#endif // MEASURE_TIME_INSERT

    POST( newRequest, value, &chunk);

#ifdef MEASURE_TIME_INSERT
    gettimeofday(&end_request, NULL);
    request_time += (end_request.tv_sec - begin_request.tv_sec) +
                    ((end_request.tv_usec - begin_request.tv_usec)/1000000.0);
#endif // MEASURE_TIME_INSERT



    free(key);
    free(value);
    free(chunk.memory);
    free(urlKey);
    free(validKey);


    freeTable(table);
    consultaInsert_free(consulta);
    // consultaInsert_free desalocas values[i]
    free(values);

    restRequest_free(newRequest);


    insert_results->message = NULL;
    free(insert_results->status);
    insert_results->status = NULL;
    stringAppend(&insert_results->status, "OK");
    insert_results->affected_rows = 1;
    insert_results->results = NULL;


#ifdef MEASURE_TIME_INSERT

    gettimeofday(&end_insert, NULL);

    double total_time = (end_insert.tv_sec - begin_insert.tv_sec) +
              ((end_insert.tv_usec - begin_insert.tv_usec)/1000000.0);
    double insert_time = total_time - request_time;

    //printf("\nInsert Time: %f\n", insert_time);
    //printf("Request Time: %f\n", request_time);
    //printf("Total Time: %f\n", request_time);

    char time_insert[100];
    sprintf(time_insert, "%f", insert_time);

    char time_request[100];
    sprintf(time_request, "%f", request_time);

    char* insert_file = "insert_insertTime.txt";
    append_string_to_file(insert_file, time_insert);

    char* request_file = "insert_requestTime.txt";
    append_string_to_file(request_file, time_request);

#endif // MEASURE_TIME_INSERT

    *ret = insert_results;
    return INSERT_NO_ERROR;

}

void sql_delete(DBConnection* connection, char* key)
{
    RESTRequest* newRequest = NULL;
    restRequest_copy(&newRequest, connection->request);

    MemoryStruct chunk;

    chunk.memory = calloc(1,1);  // will be grown as needed by the realloc above
    chunk.size = 0;    // no data at this point

    char* validKey = NULL;
    format_key(&validKey, key);
    char* newURL = NULL;
    stringConcat(&newURL, connection->request->url, validKey);

    restRequest_setUrl(&newRequest, newURL);

    // curl
    DELETE( newRequest, &chunk);

    //printf("value: %s \n", chunk.memory);

}

STATUS_DB applySpecialOperation(DBConnection* connection, char*** values, Table* table)
{
    MemoryStruct chunk;

    chunk.memory = calloc(1,1);
    chunk.size = 0;

    for(int i = 0; i < table->numberOfAttributes; i++)
    {
        if(table->attributeOperation[i] == (int)AUTO_INCREMENT)
        {
            RESTRequest *newRequest = NULL;
            restRequest_copy(&newRequest, connection->request);

            char *url = NULL;
            stringAppend(&url, newRequest->url);
            stringAppend(&url, "?key_prefix=");
            stringAppend(&url ,table->name);

            restRequest_setUrl(&newRequest, url);

            GET( newRequest, &chunk);


            printf("%s\n", chunk.memory);

            int id;
            getNumberOfKeysFromJSON(&id, chunk.memory);
            id++;
            printf("\nID: %d\n", id);

            char str[12];
            sprintf(str, "%d", id);

            free(*values[i]);
            *values[i] = malloc(sizeof(char)*12);
            strncpy(*values[i], str, 12);
        }
    }

    return NO_ERROR;
}
