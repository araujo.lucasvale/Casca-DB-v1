/**
 * @file
 * @author Davi Boberg
 * @date 24/01/2018
 *
 * \brief Define and manage local database.
 *
 */

#ifndef DIC_DADOS_H
#define DIC_DADOS_H

#include <stdlib.h>
#include "request.h"
#include "rest.h"
#include "my_lib.h"

#include "../defines.h"


typedef enum NULL_ATTRIBUTE
{
    NOT_NULL = 0,
    DEFAULT_NULL

} NULL_ATTRIBUTE;

typedef enum SPECIAL_OPERATION
{
    NO_OPERATION = 0,
    AUTO_INCREMENT

} SPECIAL_OPERATION;

typedef struct Table
{

    char* name;
    char** attributes;
    char** attributeType;
    int* attributeNull;
    int* attributeOperation;
    int numberOfAttributes;
    char** primaryKeys;
    int* indexPrimaryKeys;
    int numberOfPrimaryKeys;
    char** foreignKeys;
    int* indexforeignKeys;
    int numberOfForeignKeys;

}Table;


typedef struct Row
{
    char** values;
    int length;
}Row;


typedef struct QueryTable
{
    char* tableName;
    Row* header;
    Row* type;
    int* primaryKeysIndex;
    int numberOfPrimaryKeys;
    Row** rows;
    int numberOfRows;


}QueryTable;

typedef struct Results
{
    int affected_rows;
    char* message;
    char* status;
    QueryTable* results;
    char* has_more;

}Results;


typedef struct dataBase
{
    Table** tables;
    int numberOfTables;

}DataBase;


typedef struct dbConnection
{
    char* name;
    DataBase* database;
    RESTRequest* request;

    // curl
    //CURL* curlConnection;

}DBConnection;



/**
 * @brief Create a new DataBase Struct.
 *
 *
 * @author Davi Boberg
 * @date 28/2/2009
 *
 * @param ret   Database created.
 */
STATUS_DB create_DataBase(DataBase** ret);

/**
 * @brief Delete some table.
 *
 *
 * @author Davi Boberg
 * @date 28/2/2009
 *
 * @param ret   Database with table to delete.
 * @param table Table to delete.
 *
 * @todo Not implemented.
 */
STATUS_DB deleteTableFromDataBase(DataBase* database, Table* table);

/**
 * @brief Add Table schema into local database.
 *
 * @author Davi Boberg
 * @date 28/2/2009
 *
 * @param ret       Database schemas with added table.
 * @param database  Databae to add table schema.
 * @param table     Table to add.
 */
STATUS_DB addTableToDataBase(DataBase** ret, DataBase* database, Table* table);

/**
 * @brief Copy a table.
 *
 * @author Davi Boberg
 * @date 28/2/2009
 *
 * @param ret   Copy of table.
 * @param table Table to be copied.
 */
STATUS_DB copyTable(Table** ret, Table* table);


/**
 * @brief Verifies if a table exist and returned, if exists.
 *
 * @author Davi Boberg
 * @date 28/2/2009
 *
 * @param ret       Table founded.
 * @param database  Schemas database to search.
 * @param name      Table name to find.
 */
STATUS_DB findTable(Table** ret, DataBase* database, char* name);


STATUS_DB table_getAttributeIndex(int* ret, Table* table, char* attribute);

// Libera memória de uma tabela Table*
int freeTable(Table* table);

// Imprime informações de todas as tabelas de um banco de dados db
void printDataBase(DataBase* database);

// Libera memória de uma banco de dads DataBase*
int freeDataBase(DataBase* database);

// ====== Uso interno





#endif