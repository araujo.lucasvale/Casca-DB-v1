/**
 * @file
 * @author Davi Boberg
 * @date 24/01/2018
 *
 * \brief Process Queries.
 *
 *
 */

#ifndef SQL_QUERY_H
#define SQL_QUERY_H

#include "dic_dados.h"
#include "consulta_sql.h"
#include "sql_select.h"
#include "sql_create.h"
#include "sql_insert.h"

/// Identify query and call the correct executer
void query(DBConnection* connection, char* sql, char * buf);

/// Identify query and call the correct executer from multiple queries
void nQueries(DBConnection* connection, char** sql, int n);

#endif /* SQL_QUERY_H */

