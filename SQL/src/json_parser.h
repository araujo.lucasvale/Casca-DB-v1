/**
 * @file
 * @author Davi Boberg
 * @date 24/01/2018
 *
 * \brief Function to handle JSON strings.
 *
 */

#ifndef JSON_PARSER_H
#define JSON_PARSER_H

#include "../lib/cJSON/cJSON.h"
#include <string.h>
#include "dic_dados.h"
#include "config.h"
#include <stdio.h>
#include <stdlib.h>

/**
 * @brief Get Keys from a JSON string.
 *
 *
 * @author Davi Boberg
 * @date 28/2/2009
 *
 * @param ret   Number of keys.
 * @param data  JSON string.
 * @param keys  To load keys.
 */
STATUS_DB getKeysFromJSON(int* ret, const char* data, char keys[MAX_DATA_CURSOR][MAX_STRING_SIZE]);

/**
 * @brief Create and save a JSON from Database schemas.
 *
 *
 * @author Davi Boberg
 * @date 28/2/2009
 *
 * @param database  Database schemas.
 * @param fileName  File name to save.
 */
STATUS_DB JSONFromDataBase(DataBase* database, char* fileName);

STATUS_DB JSONFromQueryTable(char** ret, QueryTable* queryTable);

/**
 * @brief Create a JSON from requisitions results.
 *
 *
 * @author Davi Boberg
 * @date 28/2/2009
 *
 * @param ret       JSON fro Results.
 * @param result    Struct with results informations and data.
 */
STATUS_DB JSONFromResults(char** ret, Results* result);

/**
 * @brief Load a Database schemas from a JSON file.
 *
 *
 * @author Davi Boberg
 * @date 28/2/2009
 *
 * @param ret       Database.
 * @param filename  File name
 */
STATUS_DB DataBaseFromJSON(DataBase** ret, char* fileName);

/**
 * @brief Converts a json array of strings into matrix.
 *
 *
 * @author Davi Boberg
 * @date 28/2/2009
 *
 */
STATUS_DB getArrayOfStringFromJSON(char*** ret, cJSON* json, char* itemName, int itemSize);


/**
 * @brief Converts a json array of strings into matrix.
 *
 *
 * @author Davi Boberg
 * @date 28/2/2009
 *
 */
STATUS_DB getArrayOfIntFromJSON(int** ret, cJSON* json, char* itemName, int itemSize);


STATUS_DB createValuesJSON(char** ret, char** values, int numberOfValues);

STATUS_DB getValuesFromJSON(char** ret, char* key, char* data, int nValues);

STATUS_DB getCursorFromJSON(char** ret, char* data);

STATUS_DB getNumberOfKeysFromJSON(char** ret, char* data);

STATUS_DB JSONFromSelect(char* fileName, char* cursor, int nRows, char* sql);

STATUS_DB selectFromJSON(char* fileName, char** cursor, int* nRows, char** sql);

#endif // JSON_PARSER_H
