/**
 * @file
 * @author Davi Boberg
 * @date 24/01/2018
 *
 * \brief .
 *
 *
 *
 *
 */

#ifndef MYSTRING_H
#define MYSTRING_H


/*
    Define some function there are useful to manipulate strings and array of strings.
    Also define a struct List (array of strings).

*/

#include <stdlib.h>
#include <stdio.h>
#include <string.h>


typedef struct List
{
    char** values;
    int listSize;
}List;


/// Return a new string without delimiter that wraps string
/// Ex.: 'string'  =>   string
int eliminateDelimiter(char** ret, char* string, const char delimiter);

char* eliminateCharacter(char* string, const char character);

/// Return a new string with string2 concatenate to string1
/// newString = strin1 + string2 = string1string2
int stringConcat(char** concatString, char* string1, char* string2);

/// Append string2 to string1
/// string1 = string1 + string2 = string1string2
int stringAppend(char** string1, char* string2);

/// Return a new string equal to string
int copyString(char** ret, char* string);

/// Return a new array of string equal to array
int copyArrayOfString(char*** ret, char** array, int n);

/// Return a new array of ints equal to array
int copyArrayOfInt(int **ret, int* array, int n);

/// Return a new List equal to list
int copyList(List** ret, List* list);

/// Append a new string to List
int listAppend(List** list, char* string);

int freeList(List* list);

// Replace invalid caracters
int format_key(char** ret, char* key);

#endif // MYSTRING_H
