/**
 * @file
 * @author Davi Boberg
 * @date 24/01/2018
 *
 * \brief Process Create Table.
 *
 *
 */

#ifndef SQL_CREATE_H
#define SQL_CREATE_H

#include "db.h"

/// Execute query Create Table
int sql_createTable(Results** ret, DBConnection* connection, char* sql);

#endif //SQL_CREATE_H
