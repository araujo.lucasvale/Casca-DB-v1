
#include "csv_parser.h"

STATUS_DB getAllFields(char*** ret, char* line)
{
    if(line == NULL)
    {
        return -1;
    }
    int num = 0;
    getNumberOfFields(&num, line);
    char** fields = malloc(sizeof(char*)*num);
    char* tmp = NULL;
    copyString(&tmp, line);

    int stringSize;
    const char* tok;
    tok = strtok(tmp, ";");
    for(int i = 0; i < num; i++)
    {
        stringSize = strlen(tok);
        fields[i] = malloc(sizeof(char)*(stringSize+1));
        strncpy(fields[i], tok, stringSize);
        fields[i][stringSize] = '\0';
        tok = strtok(NULL,";\n");
    }
    free(tok);
    free(tmp);
    *ret = fields;
    return 0;
}

STATUS_DB getField(char** ret, char* line, int num)
{
    char* tok;
    int count = 0;
    char* tmp = strdup(line);
    for (tok = strtok(tmp, ";,"); tok != NULL; tok = strtok(NULL, ";\n"))
    {
        count++;
        if (count == num)
        {
            tok = strdup(tok);
            free(tmp);
            *ret = tok;
            return 0;
        }
    }
    free(tmp);
    return -1;
}

STATUS_DB getNumberOfFields(int* ret, char* line)
{
    if(line == NULL)
    {
        return ;
    }

    const char* tok;
    int count = 0;
    char* tmp = strdup(line);
    // For( primeiro token, enquanto token é o primeiro elemento, até
    for(tok = strtok(tmp,";"); tok != NULL; tok = strtok(NULL, ";\n"))
    {
        count++;
    }
    free(tmp);
    *ret = count;
}


