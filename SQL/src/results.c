#include "results.h"

STATUS_DB row_create(Row** ret, QueryTable* queryTable, char* keys, char* values)
{
    char** keysFields = NULL;
    int nKeys = 0;
    if(keys == NULL)
        nKeys = 0;
    else
    {
        getAllFields(&keysFields, keys);
        getNumberOfFields(&nKeys, keys);
    }

    char** valuesFields = NULL;
    int nValues = 0;
    // Indice dos atributos que não sao PK
    int* noPKIndexes = NULL;
    int noPKIndex = 0;
    if(values == NULL)
        nValues = 0;
    else
    {
        getAllFields(&valuesFields, values);
        getNumberOfFields(&nValues, values);

        noPKIndexes = malloc(sizeof(int)*nValues);
        int pks[queryTable->header->length];
        for(int i = 0; i < queryTable->header->length; i++)
            pks[i] = i;
        for(int i = 0; i < queryTable->numberOfPrimaryKeys; i++)
        {
            pks[queryTable->primaryKeysIndex[i]] = -1;
        }

        for(int i = 0; i < queryTable->header->length; i++)
        {
            if(pks[i] >= 0)
            {
                noPKIndexes[noPKIndex] = i;
                noPKIndex++;
            }
        }
    }

    Row* newRow = malloc(sizeof(Row));
    newRow->values = malloc(sizeof(char*)*queryTable->header->length);
    for(int i = 0; i < queryTable->header->length; i++)
        newRow->values[i] = NULL;

    // começa em 1 pois tem o nome da tabela na primeira posição
    for(int i = 1; i < nKeys; i++)
    {
        newRow->values[queryTable->primaryKeysIndex[i-1]] = keysFields[i];
    }
    free(keysFields[0]);

    for(int i = 0; i < nValues; i++)
    {
        newRow->values[noPKIndexes[i]] = valuesFields[i];
    }
    newRow->length = queryTable->header->length;

    if(noPKIndexes != NULL)
        free(noPKIndexes);
    if(valuesFields != NULL)
        free(valuesFields);
    if(keysFields != NULL)
        free(keysFields);

    *ret = newRow;
    return NO_ERROR;
}

STATUS_DB row_copy(Row** ret, Row* row)
{
    if(row == NULL)
    {
        printf("Invalid row.\n");
        return -1;
    }

    Row* newRow = malloc(sizeof(Row));
    newRow->values = malloc(sizeof(char*)*row->length);
    for(int i = 0; i < row->length; i++)
    {
        int size = strlen(row->values[i]);
        newRow->values[i] = malloc(sizeof(char)*(size+1));
        strncpy(newRow->values[i], row->values[i], size);
        newRow->values[i][size] = '\0';
    }
    newRow->length = row->length;

    *ret = newRow;
    return NO_ERROR;
}

// Return a new Row modified
STATUS_DB row_modify(Row** ret, Row* original, int* indexesToSave, int nIndexes)
{
    if(original == NULL)
    {
        printf("Invalid Row.\n");
        return -1;
    }
    if(indexesToSave == NULL)
    {
        printf("invalid indexes.\n");
        return -1;
    }

    if(nIndexes < 0 || nIndexes > original->length)
    {
        printf("Invalid number os indexes.\n");
        return -1;
    }


    Row* newRow = malloc(sizeof(Row));
    newRow->values = malloc(sizeof(char*)*nIndexes);

    int saveIndex = 0;
    for(int i = 0; i < original->length; i++)
    {
        for(int j = 0; j < nIndexes; j++)
        {
            if(indexesToSave[j] == i)
            {
                int size = strlen(original->values[i]);
                newRow->values[saveIndex] = malloc(sizeof(char)*(size+1));
                strncpy(newRow->values[saveIndex], original->values[i], size);
                newRow->values[saveIndex][size] = '\0';
                saveIndex++;
            }
        }
    }

    newRow->length = nIndexes;

    *ret = newRow;
    return NO_ERROR;
}

STATUS_DB row_free(Row* row)
{
    if(row == NULL)
        return -1;

    if(row->values == NULL)
        return -1;

    for(int i = 0; i < row->length; i++)
        free(row->values[i]);
    free(row->values);
    row->values = NULL;
    row->length = 0;
    free(row);
    row = NULL;
    return NO_ERROR;
}

STATUS_DB queryTable_addRow(QueryTable** ret, QueryTable* queryTable, Row* row)
{
    Row** newRow = malloc(sizeof(Row));
    newRow[0] = row;
    QueryTable* newQueryTable = NULL;
    queryTable_addNRows(&newQueryTable, queryTable, newRow, 1);
    free(newRow);

    *ret = newQueryTable;
    return NO_ERROR;
}


STATUS_DB queryTable_addNRows(QueryTable** ret, QueryTable* queryTable, Row** rows, int nRows)
{
    int newSize;
    int size;

    if(queryTable == NULL)
    {
        printf("QueryTable does not exist\n");
        return -1;
    }

    newSize = queryTable->numberOfRows + nRows;
    size = queryTable->numberOfRows;
    Row** newRows;
    if(queryTable->rows == NULL || size == 0)
    {
        newRows = malloc(sizeof(Row*)*nRows);
    }
    else
        newRows = (Row**)realloc(queryTable->rows, sizeof(Row*)*newSize);

    if(newRows  == NULL) {
        //out of memory!
        printf("\nFailed to realloc, Row not add to QueryTable\n");
        return -1;
    }

    queryTable->rows = newRows;
    for(int i = 0; i < nRows; i++)
    {
        queryTable->rows[size + i] = rows[i];
    }
    queryTable->numberOfRows = newSize;
    free(rows);

    *ret = queryTable;
    return NO_ERROR;
}

STATUS_DB queryTable_copyNRowsToQueryTable(QueryTable** ret, QueryTable* queryTable, Row** rows, int nRows)
{
    if(queryTable == NULL)
    {
        printf("QueryTable does not exist\n");
        return -1;
    }

    if(rows == NULL)
    {
        printf("\nrows does not exist.\n");
    }

    Row** newRows = malloc(sizeof(Row*)*nRows);

    for(int i = 0; i < nRows; i++)
    {
        newRows[i] = NULL;
        row_copy(&newRows[i], rows[i]);
    }

    queryTable_addNRows(&queryTable, queryTable, newRows, nRows);

    *ret = queryTable;
    return NO_ERROR;
}

STATUS_DB queryTable_create(QueryTable** ret, Table* table, Row** rows, int nRows)
{
    if(table == NULL)
    {
        printf("Invalid table.\n");
        return TABLE_NULL;
    }

    QueryTable* newQueryTable = malloc(sizeof(QueryTable));

    int nameSize = strlen(table->name);
    newQueryTable->tableName = malloc(sizeof(char)*(nameSize+1));
    strncpy(newQueryTable->tableName, table->name, nameSize);
    newQueryTable->tableName[nameSize] = '\0';

    newQueryTable->header = malloc(sizeof(Row));
    newQueryTable->header->values = NULL;
    copyArrayOfString(&newQueryTable->header->values, table->attributes, table->numberOfAttributes);
    newQueryTable->header->length = table->numberOfAttributes;

    newQueryTable->type = malloc(sizeof(Row));
    newQueryTable->type->values = NULL;
    copyArrayOfString(&newQueryTable->type->values, table->attributeType, table->numberOfAttributes);
    newQueryTable->type->length = table->numberOfAttributes;

    newQueryTable->primaryKeysIndex = malloc(sizeof(int)*table->numberOfPrimaryKeys);
    for(int i = 0; i < table->numberOfPrimaryKeys; i++)
    {
        newQueryTable->primaryKeysIndex[i] = table->indexPrimaryKeys[i];
    }
    newQueryTable->numberOfPrimaryKeys = table->numberOfPrimaryKeys;

    newQueryTable->numberOfRows = 0;
    newQueryTable->rows = NULL;



    if(rows == NULL || nRows <= 0)
    {
        *ret = newQueryTable;
        return NO_ERROR;
    }


    queryTable_addNRows(&newQueryTable, newQueryTable, rows, nRows);

    *ret = newQueryTable;
    return NO_ERROR;
}

STATUS_DB queryTable_copy(QueryTable** ret, QueryTable* queryTable)
{
    if(queryTable == NULL)
    {
        printf("Invalid QueryTable.\n");
        return ;
    }

    QueryTable* newQueryTable = malloc(sizeof(QueryTable));

    int nameSize = strlen(queryTable->tableName);
    newQueryTable->tableName = malloc(sizeof(char)*(nameSize+1));
    strncpy(newQueryTable->tableName, queryTable->tableName, nameSize);
    newQueryTable->tableName[nameSize] = '\0';

    newQueryTable->header = malloc(sizeof(Row));
    newQueryTable->header->values = NULL;
    copyArrayOfString(&newQueryTable->header->values, queryTable->header->values, queryTable->header->length);
    newQueryTable->header->length = queryTable->header->length;

    newQueryTable->type = malloc(sizeof(Row));
    newQueryTable->type->values = NULL;
    copyArrayOfString(&newQueryTable->type->values, queryTable->header->values, queryTable->header->length);
    newQueryTable->type->length = queryTable->header->length;

    newQueryTable->primaryKeysIndex = malloc(sizeof(int)*queryTable->numberOfPrimaryKeys);
    for(int i = 0; i < queryTable->numberOfPrimaryKeys; i++)
    {
        newQueryTable->primaryKeysIndex[i] = queryTable->primaryKeysIndex[i];
    }
    newQueryTable->numberOfPrimaryKeys = queryTable->numberOfPrimaryKeys;

    newQueryTable->numberOfRows = 0;
    newQueryTable->rows = NULL;



    if(queryTable->rows == NULL || queryTable->numberOfRows <= 0)
    {
        *ret = newQueryTable;
        return NO_ERROR;
    }


    queryTable_copyNRowsToQueryTable(&newQueryTable, newQueryTable, queryTable->rows, queryTable->numberOfRows);

    *ret = newQueryTable;
    return NO_ERROR;
}


STATUS_DB queryTable_modifyHeader(QueryTable** ret ,QueryTable* queryTable, int* indexes, int numberOfIndexes)
{
    if(queryTable == NULL)
    {
        printf("Invalid queryTable.\n");
        return ;
    }

    if(indexes == NULL)
    {
        printf("Invalid indexes.\n");
        return;
    }

    if(numberOfIndexes < 0)
    {
        printf("Invalid number of indexes.\n");
        return;
    }


    int newPrimaryKeys[numberOfIndexes];
    int numberOfPKs = 0;
    Row* newHeader = malloc(sizeof(Row));
    newHeader->values = malloc(sizeof(char*)*numberOfIndexes);
    for(int i = 0; i < numberOfIndexes; i++)
    {
        for(int j = 0; j < queryTable->numberOfPrimaryKeys; j++)
        {
            if(indexes[i] == queryTable->primaryKeysIndex[j])
            {
                newPrimaryKeys[numberOfPKs] = i;
                numberOfPKs++;
            }
        }
    }

    for(int i = 0; i < queryTable->header->length; i++)
        free(queryTable->header->values[i]);
    free(queryTable->header);

    row_modify(&newHeader, queryTable->header, indexes, numberOfIndexes);
    row_free(queryTable->header);
    queryTable->header = newHeader;
    queryTable->header->length = numberOfIndexes;

    if(queryTable->numberOfPrimaryKeys != numberOfPKs)
    {
        free(queryTable->primaryKeysIndex);
        queryTable->primaryKeysIndex = malloc(sizeof(int)*numberOfPKs);
    }

    for(int i = 0; i < numberOfPKs; i++)
    {
        queryTable->primaryKeysIndex[i] = newPrimaryKeys[i];
    }

    *ret = queryTable;
    return NO_ERROR;
}


int queryTable_setHeader(QueryTable** ret, QueryTable* queryTable, Row* newHeader)
{
    if(queryTable == NULL)
    {
        printf("Invalid query table.\n");
        return -1;
    }
    if(newHeader == NULL)
    {
        printf("Invalid newHeader.\n");
        return -1;
    }

    if(queryTable->header != NULL)
    {
        for(int i = 0; i < queryTable->header->length; i++)
        {
            free(queryTable->header->values[i]);
        }
        free(queryTable->header);
    }

    queryTable->header = newHeader;

    *ret = queryTable;
    return 0;
}

void queryTable_print(QueryTable* queryTable)
{
    if(queryTable == NULL)
        return;

    if(queryTable->header == NULL)
        return;

    printf("Row ");
    for(int i = 0; i < queryTable->header->length; i++)
    {
        printf("%s ", queryTable->header->values[i]);
    }


    if(queryTable->rows == NULL)
    {
        printf("\n");
        return;
    }

    for(int i = 0; i < queryTable->numberOfRows; i++)
    {
        printf("\n => %d ", i);
        for(int j = 0; j < queryTable->rows[i]->length; j++)
        {
            if(queryTable->rows[i]->values[j] != NULL)
                printf("%s ", queryTable->rows[i]->values[j]);
            else
                printf("-- ");
        }

    }
    printf("\n");
    return;
}

int results_free(Results* result)
{
    if(result == NULL)
        return -1;

    if(result->results != NULL)
        queryTable_free(result->results);

    if(result->message != NULL)
        free(result->message);

    if(result->has_more != NULL)
        free(result->has_more);

    if(result->status != NULL)
        free(result->status);

    free(result);
    return 0;
}

int queryTable_free(QueryTable* queryTable)
{
    if(queryTable == NULL)
        return -1;

    if(queryTable->primaryKeysIndex != NULL)
        free(queryTable->primaryKeysIndex);

    if(queryTable->header != NULL)
    {
        row_free(queryTable->header);
    }

    if(queryTable->type != NULL)
        row_free(queryTable->type);

    if(queryTable->rows != NULL)
    {
        for(int i = 0; i < queryTable->numberOfRows; i++)
        {
            if(queryTable->rows[i] != NULL)
                row_free(queryTable->rows[i]);
        }
        free(queryTable->rows);
        queryTable->rows = NULL;
    }

    if(queryTable->tableName != NULL)
        free(queryTable->tableName);

    free(queryTable);
    queryTable = NULL;
    return 0;
}
