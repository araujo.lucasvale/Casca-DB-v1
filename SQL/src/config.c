#include "config.h"
#include <pthread.h>


extern pthread_mutex_t dbfile_mutex;

STATUS_DB load_schema(char** ret, char* fileName)
{
    printf("mutex wait\n");
    printf("\nloading from %s\n", fileName);
    // casca
    pthread_mutex_lock(&dbfile_mutex);
    printf("mutex gained\n");
    FILE *f = fopen(fileName, "rb");
    if(f == NULL)
    {
        printf("\nFile %s does not exist.\n", fileName);
        pthread_mutex_unlock(&dbfile_mutex);

        return FILE_NOT_FOUND;
    }

    fseek(f, 0, SEEK_END);
    long fsize = ftell(f);
    fseek(f, 0, SEEK_SET);  //same as rewind(f);

    char *string = malloc(fsize + 1);
    int size = fread(string, fsize, 1, f);

    fclose(f);

    if(size == 0)
    {
        printf("size 0\n");
        pthread_mutex_unlock(&dbfile_mutex);

        return FILE_NOT_FOUND;
    }
    string[fsize] = 0;
printf("unlock\n");
    // casca
    pthread_mutex_unlock(&dbfile_mutex);

    *ret = string;
    return FILE_NO_ERROR;
}


int write_schema(char* fileName, const char* string)
{
    
     printf("\nwrinting into %s\n", fileName);

   pthread_mutex_lock(&dbfile_mutex);

 printf("mutex gained\n");
    if(fileName == NULL)
    {
        printf("\nwrite_schema failed, fileName not allocated\n");
        return -1;
    }
    if(string == NULL)
    {
        printf("\nwrite_schema failed, data to save not allocated\n");
        return -1;
    }

    FILE *f = fopen(fileName, "w");
    if(f == NULL)
    {
        printf("\nwrite_schema failed, can not open file %s\n", fileName);
        return -1;
    }

    fprintf(f, "%s", string);

    fclose(f);

    // casca
    pthread_mutex_unlock(&dbfile_mutex);

    return 0;
}



int append_string_to_file(char* fileName, char* string)
{
    // casca
   printf("mutex wait\n");
   printf("\nwrinting into %s\n", fileName);
    // casca
    pthread_mutex_lock(&dbfile_mutex);
    
 printf("mutex gained\n");
    if(fileName == NULL)
    {
        printf("\nappend string failed, fileName not allocated\n");
        return -1;
    }
    if(string == NULL)
    {
        printf("\nappend string failed, data to save not allocated\n");
        return -1;
    }

    FILE *f = fopen(fileName, "a+");
    if(f == NULL)
    {
        printf("\nappend string failed, can not open file %s\n", fileName);
        return -1;
    }

    fprintf(f, "%s", string);
    fprintf(f, "%s", "\n");

    fclose(f);

    // casca
    pthread_mutex_unlock(&dbfile_mutex);

    return 0;
}
