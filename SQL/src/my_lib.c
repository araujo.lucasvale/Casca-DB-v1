#include "my_lib.h"

int eliminateDelimiter(char** ret, char* string, const char delimiter)
{
    if(*ret != NULL)
    {
        free(*ret);
    }

    int startDelimFound = 0;
    int strSize = strlen(string);
    int start = 0;
    int end = 0;
    int match = 0;

    int i = 0;
    while(i < strSize)
    {
        if(string[i] == delimiter )
        {
            if(startDelimFound == 0)
            {
                start = i;
                startDelimFound = 1;
            }
            else if(startDelimFound == 1)
            {
                end = i;
                match = 1;
                break;
            }
        }
        i++;
    }

    if(match == 0)
        return -1;

    int length = end - start;
    char* newString = malloc(sizeof(char)*length);
    strncpy(newString, string + sizeof(char)*(start + 1), length);
    newString[length-1] = '\0';
    *ret = newString;
    return 0;
}

char* eliminateCharacter(char* string, const char character)
{
    int strSize = strlen(string);
    int match = 0;


    for(int i = 0; i < strSize; i++)
    {
        if(string[i] == character)
            match++;

    }

    if(match == 0)
        return string;

    int index = 0;
    int length = strSize - match + 1;
    char* newString = malloc(sizeof(char)*length);
    for(int i = 0; i < strSize; i++)
    {
        if(string[i] == character)
            continue;
        else
        {
            newString[index] = string[i];
            index++;
        }
    }
    newString[index] = '\0';
    return newString;

}

int stringConcat(char** concatString, char* string1, char* string2)
{
    int lengthString1, lengthString2;
    if(string1 == NULL)
        lengthString1 = 0;
    else
        lengthString1 = strlen(string1);
    if(string2 == NULL)
    {
        printf("String to append not allocated\n");
        return -1;
    }
    lengthString2 = strlen(string2);

    char* newString = malloc(sizeof(char)*(lengthString1 + lengthString2 + 1));

    if(string1 != NULL)
        memcpy(newString, string1, lengthString1);
    memcpy(newString + lengthString1, string2, lengthString2);
    newString[lengthString1+lengthString2] = '\0';

    *concatString = newString;
    return 0;
}

int stringAppend(char** string1, char* string2)
{
    int lengthString1, lengthString2;
    if(*string1 == NULL)
        lengthString1 = 0;
    else
        lengthString1 = strlen(*string1);

    if(string2 == NULL)
    {
        printf("String to append not allocated\n");
        return 0;
    }
    lengthString2 = strlen(string2);

    if(*string1 != NULL)
    {
        char* newString = realloc(*string1, sizeof(char)*(lengthString1+lengthString2+1));
        if(newString == NULL)
        {
            printf("Failed to realloc string.\n");
            return -1;
        }
        *string1 = newString;
    }
    else
    {
        *string1 = malloc(sizeof(char)*(lengthString2+1));
    }

    memcpy(*string1 + lengthString1, string2, lengthString2);
    (*string1)[lengthString1+lengthString2] = '\0';

    return 0;

}

int copyString(char** ret, char* string)
{
    char *newString = NULL;

    if(string != NULL)
    {
        int size = strlen(string);
        newString = malloc(sizeof(char)*(size+1));
        newString = strncpy(newString, string, size);
        newString[size] = '\0';
    }
    else
    {
        printf("\nCopyString failed, string NULL\n");
    }

    *ret = newString;
    return 0;
}

int copyArrayOfString(char*** ret, char** array, int n)
{

    char** newArray = NULL;
    if(n < 0)
    {
        printf("Invalid number of string.\n");
    }
    if(array == NULL)
    {
        //printf("Invalid array.\n");
        return NULL;
    }

    newArray = malloc(sizeof(char*)*n);
    for(int i = 0; i < n; i++)
    {
        newArray[i] = NULL;
        copyString(&newArray[i], array[i]);
    }

    *ret = newArray;
    return 0;
}

int copyArrayOfInt(int** ret, int* array, int n)
{
    int *newArray = NULL;

    if(n < 0)
    {
        printf("\ncopyMatrix failed, invalid n\n");
        return -1;
    }

    if(array != NULL)
    {
        newArray = malloc(sizeof(int)*n);
        for(int i = 0; i < n; i++)
        {
            newArray[i] = array[i];
        }
    }
    else
    {
        return -1;
    }

    *ret = newArray;
    return 0;
}

int copyList(List** ret, List* list)
{
    if(list == NULL)
    {
        printf("Invalid list.\n");
        return -1;
    }

    if(*ret != NULL)
    {
        freeList(*ret);
    }

    List* newList = NULL;

    newList->values = NULL;
    copyArrayOfString(&newList->values, list->values, list->listSize);
    newList->listSize = list->listSize;

    *ret = newList;
    return 0;
}

int listAppend(List** list, char* string)
{
    List* newList = NULL;

    if(string == NULL)
    {
        printf("String to append not allocated.\n");
        return list;
    }

    if(*list == NULL)
    {
        newList = calloc(1, sizeof(List));
        newList->values = calloc(1, sizeof(char*));
        newList->listSize = 0;
    }
    else
    {
        char** values = NULL;
        if((*list)->values == NULL)
        {
            values = calloc(1, sizeof(char*));
            (*list)->listSize = 0;
        }
        else
            values = realloc((*list)->values, sizeof(char*)*((*list)->listSize + 1));

        if(values == NULL)
        {
        //out of memory!
            printf("\nFailed to append\n");
            return -1;
        }
        newList = calloc(1, sizeof(List));
        newList->values = values;
        newList->listSize = (*list)->listSize;
        free(*list);
    }


    (*list) = newList;
    int stringSize = strlen(string);
    (*list)->values[(*list)->listSize] = calloc(stringSize + 1, sizeof(char));
    strncpy((*list)->values[(*list)->listSize], string, stringSize);
    (*list)->values[(*list)->listSize][stringSize] = '\0';
    (*list)->listSize++;

    return 0;

}

int freeList(List* list)
{
    if(list == NULL)
        return;
    for(int i = 0; i < list->listSize; i++)
    {
        if(list->values[i] == NULL)
            continue;
        free(list->values[i]);
    }
    free(list->values);
    free(list);

    return 0;
}


// Replace invalid caracters
int format_key(char** ret, char* key)
{
    char* validKey = NULL;
    int pos_init = 0;
    int pos_end = 0;
    for(int i = 0; i < strlen(key); i++)
    {
        if(key[i] == ' ')
        {
            char* substring = malloc(sizeof(char)*(pos_end - pos_init + 2));
            substring = strncpy(substring, key + pos_init, pos_end - pos_init + 1);
            substring[pos_end - pos_init + 1] = '\0';
            stringAppend(&validKey, substring);
            stringAppend(&validKey, "%20");
            pos_init = i+1;
            free(substring);
        }
        else if(key[i] == '/')
        {
            char* substring = malloc(sizeof(char)*(pos_end - pos_init + 2));
            substring = strncpy(substring, key + pos_init, pos_end - pos_init + 1);
            substring[pos_end - pos_init + 1] = '\0';
            stringAppend(&validKey, substring);
            stringAppend(&validKey, "%2F");
            pos_init = i + 1;
            free(substring);

        }
        else
        {
            pos_end = i;
        }
    }
    if(pos_end != pos_init)
    {
        char* substring = malloc(sizeof(char)*(pos_end - pos_init + 2));
        substring = strncpy(substring, key + pos_init, pos_end - pos_init + 1);
        substring[pos_end - pos_init + 1] = '\0';
        stringAppend(&validKey, substring);
        free(substring);
    }

    *ret = validKey;
    return 0;

}
