#include "json_parser.h"

STATUS_DB getKeysFromJSON(int* ret, const char* data, char keys[MAX_DATA_CURSOR][MAX_STRING_SIZE])
{

    cJSON* item, *key, *objects;
    char* newData = strdup(data);
    cJSON* json = cJSON_Parse(newData);

    int nKeys = 0;
    int stringSize = 0;

    objects = cJSON_GetObjectItem(json, "objects");

    int arraySize = cJSON_GetArraySize(objects);
    if(arraySize > MAX_DATA_CURSOR)
    {
        printf("Too much select results.\n");
        return -1;
    }

    for(int i = 0; i < arraySize; i++)
    {
        item = cJSON_GetArrayItem(objects, i);
        key = cJSON_GetObjectItem(item, "key");
        stringSize = strlen(key->valuestring) + 1;
        if(stringSize < MAX_STRING_SIZE)
        {
            strncpy(keys[i], key->valuestring, stringSize);
            nKeys++;
        }
    }

    cJSON_Delete(json);
    free(newData);
    *ret = nKeys;
    return 0;
}

int getJSONSize(const char* data)
{
    char* newData = strdup(data);
    cJSON* json = cJSON_Parse(newData);

    int size = cJSON_GetObjectItem(json, "total_num")->valueint;
    cJSON_Delete(json);
    free(newData);
    return size;
}

STATUS_DB JSONFromDataBase(DataBase* database, char* fileName)
{
    cJSON* banco, *table, *item, *array;
    banco = cJSON_CreateArray();

    for(int i = 0; i < database->numberOfTables; i++)
    {

        table = cJSON_CreateObject();

        item = cJSON_CreateString(database->tables[i]->name);
        cJSON_AddItemToObject(table, "Name", item);


        item = cJSON_CreateNumber(database->tables[i]->numberOfAttributes);
        cJSON_AddItemToObject(table, "AttributesSize", item);

        if(database->tables[i]->attributes != NULL)
        {
            array = cJSON_CreateStringArray((const char**)database->tables[i]->attributes, database->tables[i]->numberOfAttributes);
            cJSON_AddItemToObject(table, "Attributes", array);
        }

        if(database->tables[i]->attributeType != NULL)
        {
            array = cJSON_CreateStringArray((const char**)database->tables[i]->attributeType, database->tables[i]->numberOfAttributes);
            cJSON_AddItemToObject(table, "AttributesType", array);
        }

        if(database->tables[i]->attributeNull != NULL)
        {
            array = cJSON_CreateIntArray((const int*)database->tables[i]->attributeNull, database->tables[i]->numberOfAttributes);
            cJSON_AddItemToObject(table, "AttributesNULL", array);
        }

        if(database->tables[i]->attributeOperation!= NULL)
        {
            array = cJSON_CreateIntArray((const int*)database->tables[i]->attributeOperation, database->tables[i]->numberOfAttributes);
            cJSON_AddItemToObject(table, "AttributesOperation", array);
        }



        item = cJSON_CreateNumber(database->tables[i]->numberOfPrimaryKeys);
        cJSON_AddItemToObject(table, "PrimaryKeysSize", item);

        if(database->tables[i]->primaryKeys != NULL)
        {
            array = cJSON_CreateStringArray((const char**)database->tables[i]->primaryKeys, database->tables[i]->numberOfPrimaryKeys);
            cJSON_AddItemToObject(table, "PrimaryKeys", array);

            array = cJSON_CreateIntArray((const int*)database->tables[i]->indexPrimaryKeys, database->tables[i]->numberOfPrimaryKeys);
            cJSON_AddItemToObject(table, "IndexPrimaryKeys", array);
        }



        item = cJSON_CreateNumber(database->tables[i]->numberOfForeignKeys);
        cJSON_AddItemToObject(table, "ForeignKeysSize", item);

        if(database->tables[i]->foreignKeys != NULL)
        {
            array = cJSON_CreateStringArray((const char**)database->tables[i]->foreignKeys, database->tables[i]->numberOfForeignKeys);
            cJSON_AddItemToObject(table, "ForeignKeys", array);

            array = cJSON_CreateIntArray((const int*)database->tables[i]->indexforeignKeys, database->tables[i]->numberOfForeignKeys);
            cJSON_AddItemToObject(table, "IndexForeignKeys", array);
        }

        cJSON_AddItemToArray(banco, table);
    }


    char* out = cJSON_Print(banco);
    write_schema(fileName, out);

    free(out);
    cJSON_Delete(banco);

    return 1;
}

STATUS_DB JSONFromQueryTable(char** ret, QueryTable* queryTable)
{

/*
    {
    "Name" : "query_name",
    "Header_size": 2,
    "Header" : ["atribut1", "atributo2"],
    "AttributesType" : ["Varchar(30)", "Varchar(30)"],
    "PrimaryKeysSize" : 1,
    "IndexPrimaryKey" : [1],
    "NumberOfRows": 1,
    "Rows": {
            "0":    [values],
            "1":    [values2]
            }
    }
*/

    cJSON *table, *item, *array, *rows;
    table = cJSON_CreateArray();



    table = cJSON_CreateObject();

    item = cJSON_CreateString(queryTable->tableName);
    cJSON_AddItemToObject(table, "Name", item);

    item = cJSON_CreateNumber(queryTable->header->length);
    cJSON_AddItemToObject(table, "Header_size", item);


    if(queryTable->header != NULL)
    {
        array = cJSON_CreateStringArray((const char**)queryTable->header->values, queryTable->header->length);
        cJSON_AddItemToObject(table, "Header", array);
    }

    if(queryTable->type != NULL)
    {
        array = cJSON_CreateStringArray((const char**)queryTable->type->values, queryTable->type->length);
        cJSON_AddItemToObject(table, "AttributesType", array);
    }

    item = cJSON_CreateNumber(queryTable->numberOfPrimaryKeys);
    cJSON_AddItemToObject(table, "PrimaryKeySize", item);

    if(queryTable->primaryKeysIndex != NULL)
    {
        array = cJSON_CreateIntArray((const int*)queryTable->primaryKeysIndex, queryTable->numberOfPrimaryKeys);
        cJSON_AddItemToObject(table, "IndexPrimaryKey", array);
    }

    item = cJSON_CreateNumber(queryTable->numberOfRows);
    cJSON_AddItemToObject(table, "NumberOfRows", item);

    rows = cJSON_CreateObject();

    for(int i = 0; i < queryTable->numberOfRows; i++)
    {
        if(queryTable->rows[i] != NULL)
        {
            array = cJSON_CreateStringArray((const char**)queryTable->rows[i]->values, queryTable->rows[i]->length);
            char str[7];
            sprintf(str, "%d", i);
            cJSON_AddItemToObject(rows, str, array);
        }

    }

    cJSON_AddItemToObject(table, "Rows", rows);


    char* out = cJSON_Print(table);
    //printf(out);
    //write_schema(fileName, out);

    //free(out);
    cJSON_Delete(table);

    *ret = out;
    return 0;
}

STATUS_DB JSONFromResults(char** ret, Results* result)
{
    cJSON *table, *item, *array, *rows, *results;

    table = cJSON_CreateObject();


    item = cJSON_CreateNumber(result->affected_rows);
    cJSON_AddItemToObject(table, "Rows", item);

    item = cJSON_CreateString(result->status);
    cJSON_AddItemToObject(table, "Status", item);

    if(result->message != NULL)
    {
        item = cJSON_CreateString(result->message);

    }
    else
    {
        item = cJSON_CreateString("");
    }
    cJSON_AddItemToObject(table, "Message", item);
    if(result->has_more != NULL)
    {
        item = cJSON_CreateString(result->has_more);
        cJSON_AddItemToObject(table, "Has_more", item);
    }
    results = cJSON_CreateObject();

    if(result->results != NULL)
    {


        item = cJSON_CreateString(result->results->tableName);
        cJSON_AddItemToObject(results, "Name", item);


        if(result->results->header!= NULL)
        {
            array = cJSON_CreateStringArray((const char**)result->results->header->values, result->results->header->length);
            cJSON_AddItemToObject(results, "Header", array);
        }

        item = cJSON_CreateNumber(result->results->numberOfRows);
        cJSON_AddItemToObject(results, "NumberOfRows", item);

        rows = cJSON_CreateObject();

        for(int i = 0; i < result->results->numberOfRows; i++)
        {
            if(result->results->rows[i] != NULL)
            {
                array = cJSON_CreateStringArray((const char**)result->results->rows[i]->values, result->results->rows[i]->length);
                char str[7];
                sprintf(str, "%d", i);
                cJSON_AddItemToObject(rows, str, array);
            }

        }

        cJSON_AddItemToObject(results, "Rows", rows);
    }

    cJSON_AddItemToObject(table, "Results", results);




    char* out = cJSON_Print(table);
    //printf(out);
    //write_schema(fileName, out);

    //free(out);
    cJSON_Delete(table);
    *ret = out;
    return 0;
}

// Mudar para forma mais genercia que use a mesma função para selects e inserts
int JSON_insert_result(char** ret, char* message)
{

/*
    {
   "rows" : 1               // INT - number of affected/returned rows
   "status" : "exemplo"      // String (OK/ERROR)
   "message" : "msg"        // String - error message
   "results" :  {}
    }
*/


    cJSON *table, *item, *results;
    table = cJSON_CreateArray();



    table = cJSON_CreateObject();


    item = cJSON_CreateNumber(1);
    cJSON_AddItemToObject(table, "Rows", item);

    item = cJSON_CreateString("OK");
    cJSON_AddItemToObject(table, "Status", item);


    item = cJSON_CreateString(message);
    cJSON_AddItemToObject(table, "Message", item);


    results = cJSON_CreateObject();


    //cJSON_AddItemToObject(results, "Rows", rows);

    cJSON_AddItemToObject(table, "Results", results);




    char* out = cJSON_Print(table);
    //printf(out);
    //write_schema(fileName, out);

    //free(out);
    cJSON_Delete(table);

    *ret = out;
    return 0;
}


STATUS_DB DataBaseFromJSON(DataBase** ret, char* fileName)
{

    char* loaded = NULL;
    STATUS_DB db_ret = load_schema(&loaded, fileName);
    if(db_ret == FILE_NOT_FOUND)
    {
        printf("\nDatabase not found.\n");
        return DATABASE_NO_DATABASE;
    }

    cJSON* json = cJSON_Parse(loaded);
    cJSON* item, *key;
    DataBase* database = calloc(1, sizeof(DataBase));

    int stringSize = 0;

    if(cJSON_IsArray(json) == 1)
    {
        int arraySize = cJSON_GetArraySize(json);
        database->numberOfTables = arraySize;
        database->tables = calloc(arraySize, sizeof(Table*));

        for(int i = 0; i < database->numberOfTables; i++)
        {
            database->tables[i] = calloc(1, sizeof(Table));

            item = cJSON_GetArrayItem(json, i);
            key = cJSON_GetObjectItem(item, "Name");
            if(key == NULL)
            {
                printf("Key Name not found\n");
            }
            else
            {
                stringSize = strlen(key->valuestring);
                database->tables[i]->name = malloc(sizeof(char)*(stringSize+1));
                strncpy(database->tables[i]->name, key->valuestring, stringSize);
                database->tables[i]->name[stringSize] = '\0';
            }

            key = cJSON_GetObjectItem(item, "AttributesSize");
            if(key == NULL)
            {
                printf("Key AttributesSize not found\n");
            }
            else
            {
                database->tables[i]->numberOfAttributes = key->valueint;
                database->tables[i]->attributes = NULL;
                getArrayOfStringFromJSON(&database->tables[i]->attributes, item, "Attributes", database->tables[i]->numberOfAttributes);

                database->tables[i]->attributeType = NULL;
                getArrayOfStringFromJSON(&database->tables[i]->attributeType, item, "AttributesType", database->tables[i]->numberOfAttributes);

                database->tables[i]->attributeNull = NULL;
                getArrayOfIntFromJSON(&database->tables[i]->attributeNull, item, "AttributesNULL", database->tables[i]->numberOfAttributes);

                database->tables[i]->attributeOperation = NULL;
                getArrayOfIntFromJSON(&database->tables[i]->attributeOperation, item, "AttributesOperation", database->tables[i]->numberOfAttributes);
            }


            key = cJSON_GetObjectItem(item, "PrimaryKeysSize");
            if(key == NULL)
            {
                printf("Key PrimaryKeysSize not found\n");
            }
            else
            {
                database->tables[i]->numberOfPrimaryKeys = key->valueint;
                database->tables[i]->primaryKeys = NULL;
                getArrayOfStringFromJSON(&database->tables[i]->primaryKeys, item, "PrimaryKeys", database->tables[i]->numberOfPrimaryKeys);
                database->tables[i]->indexPrimaryKeys = NULL;
                getArrayOfIntFromJSON(&database->tables[i]->indexPrimaryKeys, item, "IndexPrimaryKeys", database->tables[i]->numberOfPrimaryKeys);
            }


            key = cJSON_GetObjectItem(item, "ForeignKeysSize");
            if(key == NULL)
            {
                printf("Key ForeignKeysSize not found\n");
            }
            else
            {
                database->tables[i]->numberOfForeignKeys = key->valueint;
                database->tables[i]->foreignKeys = NULL;
                getArrayOfStringFromJSON(&database->tables[i]->foreignKeys, item, "ForeignKeys", database->tables[i]->numberOfForeignKeys);
                database->tables[i]->indexforeignKeys = NULL;
                getArrayOfIntFromJSON(&database->tables[i]->indexforeignKeys, item, "IndexForeignKeys", database->tables[i]->numberOfForeignKeys);
            }

        }
    }

    cJSON_Delete(json);
    free(loaded);
    *ret = database;
    return DATABASE_NO_ERROR;

}

STATUS_DB getArrayOfStringFromJSON(char*** ret, cJSON* json, char* itemName, int itemSize)
{
    cJSON* item, *key;
    char** newMatrix = NULL;

    if(json == NULL)
    {
        printf("Invalid json\n");
        return -1;
    }

    if(itemName == NULL)
    {
        printf("Invalid item\n");
        return -1;
    }

    if(itemSize == 0)
        return -1;


    item = cJSON_GetObjectItem(json, itemName);

    int stringSize;
    newMatrix = calloc(itemSize+1, sizeof(char*));
    for(int i = 0; i < itemSize; i++)
    {
        key = cJSON_GetArrayItem(item, i);


        if(key == NULL)
        {
            printf("Item not found.\n");
            continue;
        }

        stringSize = strlen(key->valuestring) + 1;
        if(stringSize < MAX_STRING_SIZE)
        {
            newMatrix[i] = malloc(sizeof(char)*stringSize);
            strncpy(newMatrix[i], key->valuestring, stringSize);
        }
    }

    //cJSON_Delete(item);
    *ret = newMatrix;
    return 0;

}

STATUS_DB getArrayOfIntFromJSON(int** ret, cJSON* json, char* itemName, int itemSize)
{
    cJSON* item, *key;
    int* newMatrix = NULL;

    if(json == NULL)
    {
        printf("Invalid json\n");
        return -1;
    }

    if(itemName == NULL)
    {
        printf("Invalid item\n");
        return -1;
    }

    if(itemSize == 0)
        return -1;


    item = cJSON_GetObjectItem(json, itemName);

    newMatrix = calloc(itemSize+1, sizeof(int));
    for(int i = 0; i < itemSize; i++)
    {
        key = cJSON_GetArrayItem(item, i);


        if(key == NULL)
        {
            printf("Item not found.\n");
            continue;
        }

        newMatrix[i] = key->valueint;

    }

    //cJSON_Delete(item);
    *ret = newMatrix;
    return 0;
}

STATUS_DB createValuesJSON(char** ret, char** values, int numberOfValues)
{

    cJSON *array, *table;

    if(values == NULL)
    {
        printf("Invalid values.\n");
        return -1;
    }
    if(numberOfValues < 0)
    {
        printf("Invalid numberOfValues.\n");
        return -1;
    }

    table = cJSON_CreateObject();
    array = cJSON_CreateStringArray((const char**)values, numberOfValues);
    cJSON_AddItemToObject(table, "Value", array);

    char* value = cJSON_Print(table);
    cJSON_Delete(table);
    *ret = value;
    return 0;
}

STATUS_DB getValuesFromJSON(char** ret, char* key, char* data, int nValues)
{
    char* newData = strdup(data);
    cJSON* json = cJSON_Parse(newData);

    char** values = NULL;
    getArrayOfStringFromJSON(&values, json, "Value", nValues);

    char* value = NULL;

    if(nValues == 0)
        return -1;

    stringAppend(&value, values[0]);
    free(values[0]);
    for(int i = 1; i < nValues; i++)
    {
        stringAppend(&value, ";");
        stringAppend(&value, values[i]);
        free(values[i]);
    }

    free(values);
    cJSON_Delete(json);
    free(newData);

    *ret = value;
    return 0;
}

STATUS_DB getCursorFromJSON(char** ret, char* data)
{
    cJSON* key;
    char* newData = strdup(data);
    cJSON* json = cJSON_Parse(newData);


    int stringSize = 0;
    char* cursor = NULL;

    if(cJSON_IsObject(json) == 1)
    {
        key = cJSON_GetObjectItem(json, "has_more");
        if(key->valueint == 1)
        {
            key = cJSON_GetObjectItem(json, "cursor");
            stringSize = strlen(key->valuestring);
            if(stringSize < MAX_STRING_SIZE)
            {
                cursor = malloc(sizeof(char)*(stringSize + 1));
                strncpy(cursor, key->valuestring, stringSize);
                cursor[stringSize] = '\0';
            }
        }
    }
    cJSON_Delete(json);
    free(newData);

    *ret = cursor;
    return 0;
}

STATUS_DB getNumberOfKeysFromJSON(char** ret, char* data)
{
    cJSON* key;
    char* newData = strdup(data);
    cJSON* json = cJSON_Parse(newData);


    int stringSize = 0;
    char* cursor = NULL;

    if(cJSON_IsObject(json) == 1)
    {
        key = cJSON_GetObjectItem(json, "total_num");
        *ret = key->valueint;

    }
    cJSON_Delete(json);
    free(newData);

    return 0;
}


STATUS_DB JSONFromSelect(char* fileName, char* cursor, int nRows, char* sql)
{
    if(fileName == NULL)
    {
        printf("\nError: invalid fileName.\n");
        return -1;
    }

    cJSON *select, *item;

    select = cJSON_CreateObject();

    item = cJSON_CreateString(cursor);
    cJSON_AddItemToObject(select, "Cursor", item);


    item = cJSON_CreateNumber(nRows);
    cJSON_AddItemToObject(select, "Rows", item);

    item = cJSON_CreateString(sql);
    cJSON_AddItemToObject(select, "SQL", item);

    char* out = cJSON_Print(select);
    cJSON_Delete(select);


    //print_ocall("\nJSON Select:\n%s", out);
    write_schema(fileName, (const char*)out);
    return 0;
}


// values e somente para testes
STATUS_DB selectFromJSON(char* fileName, char** cursor, int* nRows, char** sql)
{
/*
    {
    "Cursor" : "cursor",
    "Name" : "query_name",
    "HeaderSize": 2,
    "Header" : ["atribut1", "atributo2"],
    "AttributesType" : ["Varchar(30)", "Varchar(30)"],
    "PrimaryKeysSize" : 1,
    "IndexPrimaryKey" : [1],
    "NumberOfRows": 1,
    "Rows": {
            "0":    [values],
            "1":    [values2]
            }
    }
*/

    if(fileName == NULL)
    {
        //printf("\nError: invalid fileName.\n");
        return FILE_FILENAME_ERROR;
    }

    char* loaded = NULL;

    STATUS_DB db_ret = load_schema(&loaded, fileName);

    if(db_ret == FILE_NOT_FOUND)
    {
        return FILE_NOT_FOUND;
    }

    cJSON* json = cJSON_Parse(loaded);

    cJSON* key;


    key = cJSON_GetObjectItem(json, "Cursor");
    int stringSize = strlen(key->valuestring);
    if(stringSize < MAX_STRING_SIZE)
    {
        *cursor = (char*)malloc(sizeof(char)*(stringSize + 1));
        strncpy(*cursor, key->valuestring, stringSize);
        (*cursor)[stringSize] = '\0';
    }

    key = cJSON_GetObjectItem(json, "Rows");
    *nRows = key->valueint;

    key = cJSON_GetObjectItem(json, "SQL");
    stringSize = strlen(key->valuestring);
    if(stringSize < MAX_STRING_SIZE)
    {
        *sql = (char*)malloc(sizeof(char)*(stringSize + 1));
        strncpy(*sql, key->valuestring, stringSize);
        (*sql)[stringSize] = '\0';
    }


    cJSON_Delete(json);
    free(loaded);

    return JSON_NO_ERROR;
}

