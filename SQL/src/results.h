/**
 * @file
 * @author Davi Boberg
 * @date 24/01/2018
 *
 * \brief .
 *
 */

#ifndef RESULTS_H
#define RESULTS_H

#include <stdlib.h>
#include "dic_dados.h"
#include "csv_parser.h"

/// Create a Row to a specific tableQuery
int row_create(Row** ret, QueryTable* queryTable, char* keys, char* values);

int row_copy(Row** ret, Row* row);

// Return a new row with only the collunms indicated by indexToSave
int row_modify(Row** ret, Row* original, int* indexesToSave, int nIndexes);

int row_free(Row* row);

int queryTable_addRow(QueryTable** ret, QueryTable* queryTable, Row* row);

int queryTable_addNRows(QueryTable** ret, QueryTable* queryTable, Row** rows, int nRows);

int queryTable_copyNRowsToQueryTable(QueryTable** ret, QueryTable* queryTable, Row** rows, int nRows);

int queryTable_create(QueryTable** ret, Table* table, Row** rows, int nRows);

int queryTable_copy(QueryTable** ret, QueryTable* queryTable);

int queryTable_modifyHeader(QueryTable** ret, QueryTable* queryTable, int* indexes, int numberOfIndexes);

int queryTable_setHeader(QueryTable** ret, QueryTable* queryTable, Row* newHeader);

void queryTable_print(QueryTable* queryTable);

int results_free(Results* result);

int queryTable_free(QueryTable* queryTable);

#endif // RESULTS_H
