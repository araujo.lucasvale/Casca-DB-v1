/**
 * @file
 * @author Davi Boberg
 * @date 24/01/2018
 *
 * \brief File handler.
 *
 * Load and write to files.
 *
 *
 */

#ifndef CONFIG_H
#define CONFIG_H

#include <stdio.h>
#include <stdlib.h>
#include "my_lib.h"
#include "dic_dados.h"



/**
 * \brief Load from file.
 *
 */
STATUS_DB load_schema(char** ret, char* fileName);

/**
 * \brief Write to file.
 *
 */
int write_schema(char* fileName, const char* string);

/**
 * \brief .
 *
 */
int append_string_to_file(char* fileName, char* string);


#endif // CONFIG_H
