#include "sql_create.h"

//#define PRINT_RESULTS_CREATE_TABLE

int sql_createTable(Results** ret, DBConnection* connection, char* sql)
{
    Table* table = NULL;
    parse_sql_create_table(&table, sql);

    Results* create_results;
    create_results = malloc(sizeof(Results));
    create_results->message = NULL;
    create_results->status = "ERROR";
    create_results->affected_rows = 0;
    create_results->results = NULL;

    if(table == NULL)
    {
        stringConcat(&create_results->message, create_results->message, "Table could not be created.");
        freeTable(table);
        *ret = create_results;
        return 0;
    }

    Table* tableFound = NULL;
    findTable(&tableFound, connection->database, table->name);
    if(tableFound != NULL)
    {
        stringConcat(&create_results->message, create_results->message, "Table already exists.");

        freeTable(tableFound);
        freeTable(table);

        *ret = create_results;
        return 0;
    }


    addTableToDataBase(&connection->database, connection->database, table);
    freeTable(table);

#ifdef PRINT_RESULTS_CREATE_TABLE
        printDataBase(connection->database);
#endif  //PRINT_RESULTS_CREATE_TABLE

    create_results->message = NULL;
    create_results->status = "OK";
    create_results->affected_rows = 0;
    create_results->results = NULL;
    *ret = create_results;
    return 0;
}
