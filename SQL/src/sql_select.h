/**
 * @file
 * @author Davi Boberg
 * @date 24/01/2018
 *
 * \brief Process Selects.
 *
 *
 */

#ifndef SQL_SELECT_H
#define SQL_SELECT_H

#include "json_parser.h"
#include "consulta_sql.h"
#include "dic_dados.h"
#include "results.h"
#include "relational.h"


/// Execute query Select
STATUS_DB sql_select(Results** ret, DBConnection* connection, char* sql);

/// Get keys from Table table of REST database
//int getKeys(DBConnection* connection, char* table, char keys[MAX_DATA_CURSOR][MAX_STRING_SIZE]);
STATUS_DB getKeys(QueryTable** queryTable, DBConnection* connection, char* table);

STATUS_DB getValues(QueryTable** queryTable, DBConnection* connection);

/// Get the index of attribute on key
STATUS_DB getAttributeFromKey(char** ret, char* key, int index);

STATUS_DB analyseSaving(QueryTable* queryTableProjection, QueryTable** queryTable, DBConnection* connection, Results* results, char* sql, int maxReturnElements, char* previousCursor);

STATUS_DB analyseLoading(QueryTable* queryTableProjection, QueryTable** queryTable, DBConnection* connection, Results* results, char* sql, int nRowsToLoad, int maxReturnElements);




#endif // SQL_SELECT_H