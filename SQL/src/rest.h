/**
 * @file
 * @author Davi Boberg
 * @date 24/01/2018
 *
 * /brief Request informations.
 *
 * Define struct RESTRequest to store requisition parameters and functions to use it.
 *
 */

#ifndef REST_H
#define REST_H


#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "../defines.h"

typedef enum
{
    ERROR = -1, // generic error
    NO_ERROR = 0,
    NO_MEMORY_AVAILABLE,

//connection
    CONNECTION_NO_ERROR,

//Requisition
    REQUEST_NO_ERROR,
    REQUEST_CONNECTION_FAIL,
    REQUEST_POST_NO_ERROR,
    REQUEST_GET_NO_ERROR,
    REQUEST_GET_EMPTY_RETURN,
    REQUEST_GET_INVALID_RETURN,
    REQUEST_POST_DUPLICATE_VALUE,


//Database
    DATABASE_NO_ERROR,
    DATABASE_NO_TABLES,
    DATABASE_NO_DATABASE,

//Tables
    TABLE_NOT_FOUND,
    TABLE_NAME_INVALID,
    TABLE_NULL,

//SELECT
    SELECT_NO_ERROR,
    SELECT_LOADING_NO_MORE_DATA,
    SELECT_LOADING_INVALID_CURSOR,

//INSERT
    INSERT_NO_ERROR,
    INSERT_INVALID_NUMBER_ATTRIBUTES,

//PARSER

    PARSER_NO_ERROR,
    PARSER_SINTAXE_ERROR,
    PARSER_TOO_MUCH_ATTRIBUTES,
    PARSER_TOO_MUCH_CONDITIONS,
    PARSER_TOO_MUCH_TABLES,
    PARSER_NAME_TOO_LONG,
    PARSER_ATTRIBUTE_TOO_LONG,
    PARSER_CONDITION_TOO_LONG,


//Files

    FILE_NO_ERROR,
    FILE_FILENAME_ERROR,
    FILE_NOT_FOUND,
    FILE_LOAD_FAILED,
    FILE_INVALID_DATA,

//JSON

    JSON_NO_ERROR,
    JSON_INCORRECT,
    JSON_NOT_EXIST,

//strings
    STRING_NO_ERROR,
    STRING_NULL,
    STRING_INVALID,
    STRING_TOO_LONG,

//Array
    ARRAY_NO_ERROR,
    ARRAY_INVALID,
    ARRAY_NULL,

//List
    LIST_NO_ERROR

} STATUS_DB;

typedef struct REST_Request{

    char** header;
    unsigned int numberOfHeaders;
    char* url;
    char* cursor;

}RESTRequest;

// Retorna um Rest_Reques*t com informações identicas
 RESTRequest*  restRequest_init();

STATUS_DB restRequest_copy(RESTRequest** ret, RESTRequest* request);

/**
 * @brief Set requisition url.
 *
 *
 * @author Davi Boberg
 * @date 28/2/2009
 *
 * @param request       Requisition informations.
 * @param url           Url to set.
 */
STATUS_DB restRequest_setUrl(RESTRequest** request, char* url);

/**
 * @brief Set requisition cursor (database).
 *
 *
 * @author Davi Boberg
 * @date 28/2/2009
 *
 * @param request       Requisition informations.
 * @param newCursor     Cursor to set.
 */
STATUS_DB restRequest_setCursor(RESTRequest** request, char* cursor);

/**
 * @brief Set requisition header.
 *
 *
 * @author Davi Boberg
 * @date 28/2/2009
 *
 * @param request       Requisition informations.
 * @param index         Index to set.
 * @param header        Header to set.
 */
STATUS_DB restRequest_setHeader(RESTRequest** request, int index, char* header);

/**
 * @brief Add requisition header.
 *
 *
 * @author Davi Boberg
 * @date 28/2/2009
 *
 * @param request       Requisition informations.
 * @param header        Header to append.
 */
STATUS_DB restRequest_appendHeader(RESTRequest** request, char* header);

int restRequest_free(RESTRequest *request);

int restRequest_freeHeaders(RESTRequest *request);

int restRequest_freeUrl(RESTRequest *request);

int restRequest_freeCursor(RESTRequest *request);

size_t WriteMemoryCallback(void *contents, size_t size, size_t nmemb, void *userp);

#endif	// REST_H
