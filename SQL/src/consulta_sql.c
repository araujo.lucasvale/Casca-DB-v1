
#include "consulta_sql.h"
#define REG_NOERROR 0
#define MAX_MATCHES 10

char* str_between_delimiter(char* str, const char delim)
{

    List* tmp = str_split_between(str, delim);
    char* toReturn  = NULL;

    if(tmp == NULL)
        return NULL;
    else
    {
        int strSize = strlen(tmp->values[0]);
        toReturn = malloc(sizeof(char)*(strSize + 1));
        toReturn = strncpy(toReturn, tmp->values[0], strSize);
        toReturn[strSize] = '\0';
    }

    freeList(tmp);

    return toReturn;
}

List* str_split_between(char* a_str, const char a_delim)
{
    int stringSize;
    int startFound = 0;
    int startIndex = 0;
    int endIndex = 0;
    int notFound = 1;
    List* myList = NULL;
    char* string = NULL;
    int a_strSize = strlen(a_str);

    for(int i = 0; i < a_strSize; i++)
    {
        if(a_str[i] == a_delim)
        {
            if(startFound == 0)
            {
                startFound = 1;
                startIndex = i + 1;
            }
            else
            {
                notFound = 0;
                endIndex = i - 1;
                startFound = 0;

                stringSize = endIndex - startIndex + 1;
                if(stringSize > MAX_STRING_SIZE)
                {
                    printf("At str_split_between, String too long.\n");
                    return NULL;
                }
                string = malloc(sizeof(char)*(stringSize + 1));
                strncpy(string, a_str + startIndex, stringSize);
                string[stringSize] = '\0';
                listAppend(&myList, string);
                free(string);
            }
        }
    }
    if(notFound)
        return NULL;

    return myList;
}

// TODO: Melhorar essa funçao (e essas outras de manipulação de strings), estao muito confusa
//Funcao para separar os componentes de uma string dado um delimitador
List* str_split(char* a_str, const char a_delim)
{
    List* result = NULL;
    size_t count = 0;
    int last_delim = 0;
    int delim_together = 0;
    char delim_found = 0;
    char delim[2];
    delim[0] = a_delim;
    delim[1] = 0;
    int delim_at_start = 0;

    for(int i = 0; a_str[i] != '\0'; i++)
    {
        if(a_str[i] == a_delim && delim_together == 0)
        {
            if(i == delim_at_start)
            {
                delim_at_start++;
                continue;
            }
            delim_together = 1;
            count++;
            last_delim = i;
            delim_found = 1;
        }
        else
            delim_together = 0;
    }

    if(delim_found == 1)
    {
        if((strlen(a_str) - 1) != last_delim)
            count++;
    }
    else
    {
        count++;
    }

    char* a_strTemp = strdup(a_str);
    char* token = strtok(a_strTemp, delim);
    for(int i = 0; i < count; i++)
    {
        // gambiarra para nao aparecer mensagem de append string vazia
        if(token == NULL)
            continue;
        listAppend(&result, token);
        token = strtok(NULL, delim);
    }
    free(a_strTemp);
    return result;
}

// Split " " without spliting " " inside strings
List* str_split_strings(char* a_str)
{
    int isString = 0;
    int init = 0;
    int end = 0;
    List* strings = NULL;
    int allSaved = 0;
    for(int i = 0; i < strlen(a_str); i++)
    {
        if(a_str[i] == '\'')
        {
            allSaved = 0;
            if(isString)
            {
                end = i;
                char* newString = malloc(sizeof(char)*(end-init+2));
                strncpy(newString, a_str + sizeof(char)*init, (end-init+1));
                newString[end-init+1] = '\0';
                listAppend(&strings, newString);
                isString = 0;
                init = i+1;
                allSaved = 1;
                free(newString);
            }
            else
            {
                end = i;
                if(end != init)
                {
                    char* newString = malloc(sizeof(char)*(end-init+1));
                    strncpy(newString, a_str + sizeof(char)*init, (end-init));
                    newString[end-init] = '\0';
                    listAppend(&strings, newString);
                    free(newString);
                }
                isString = 1;
                init = i;
                allSaved = 1;

            }
        }
    }

    // save last piece of string
    if(allSaved == 0)
    {
        end = strlen(a_str)-1;
        char* newString = malloc(sizeof(char)*(end-init+2));
        strncpy(newString, a_str + sizeof(char)*(init+1), (end-init+1));
        newString[end-init+2] = '\0';
        listAppend(&strings, newString);
        free(newString);
    }

    List* splits = NULL;

    for(int i = 0; i < strings->listSize; i++)
    {
        if(strings->values[i][0] == '\'')
        {
            listAppend(&splits, strings->values[i]);
        }
        else
        {
            List* listTemp = str_split(strings->values[i], ' ');
            if(listTemp == NULL)
                continue;
            for(int j = 0; j < listTemp->listSize; j++)
                 listAppend(&splits, listTemp->values[j]);
            freeList(listTemp);
        }
    }
    freeList(strings);
    return splits;

}

//Funcao para retirar delimitadores e retornar a substring tratada
char* substring(char* str, int ini, int fim, char delimiter)
{
    if(ini < 0 || fim < 0)
        return NULL;
    int tam = fim - ini + 1;
    char *subs = calloc(tam, sizeof(char)*tam);

    int j = 0;
    for(int i = ini; i<fim; i++)
    {
        if(str[i] != delimiter)
        {
            subs[j] = str[i];
            j++;
        }
    }
    //Caso o tamanho da string for menor do que o alocado
    if((j+1) < tam)
    {
        char *new_subs = realloc(subs, sizeof(char)*(j+1));
        if(new_subs == NULL)
        {
            printf("Problem at realloc\n");
            return subs;
        }
        return new_subs;
    }
    return subs;
}

// Remove espaços que não estejam em strings
char* eliminate_space_not_string(char* string)
{
    int string_size = strlen(string);
    int is_string = 0;
    char temp_string[string_size];
    int tmp_string_index = 0;
    int init_save = 0;
    int end_save = 0;

    for(int i = 0; i < strlen(string); i++)
    {
        if(string[i] == '\'' || string[i] == '\"')
        {
            if(is_string == 0)
                is_string = 1;
            else
                is_string = 0;
        }
        else if(is_string == 1)
        {
            temp_string[tmp_string_index] = string[i];
            tmp_string_index++;
            continue;
        }
        else if(string[i] == ' ')
        {
            continue;
        }

        temp_string[tmp_string_index] = string[i];
        tmp_string_index++;
    }

    temp_string[tmp_string_index] = '\0';
    tmp_string_index++;

    char* new_string = NULL;
    new_string = malloc(sizeof(char)*tmp_string_index);
    if(new_string == NULL)
    {
        printf("Failed to malloc, not enough memory\n");
    }
    new_string = strncpy(new_string, temp_string, tmp_string_index);

    return new_string;
}

//Processa o regex e retorna seu resultado
int get_match(regex_t* reg, char* sql, char** user_ptr, int group)
{

    regmatch_t matches[MAX_MATCHES];
    int regex__err = regexec(reg, sql, MAX_MATCHES, matches, group);
    if (regex__err == REG_NOERROR)
    {

        int inicio = matches[group].rm_so;
        int fim = matches[group].rm_eo;
        char* dest;
        dest = substring(sql,inicio,fim,'"');
        *user_ptr = dest;
        return REG_NOERROR;
    }
    else if(regex__err == REG_NOMATCH)
    {
        return REG_NOMATCH;
    }
    else
    {
        printf("\%s com erro de sintaxe\n", sql);

    }
    return -1;


}

void consultaSelect_print(consultaSelect c){

    printf("Print do Select: \n\n");
    printf("Atributos: \n");

    for(int i = 0; i < c.numberOfAttributes; i++)
    {
        printf("[%s] ", c.attributes[i]);
    }

    printf("\n\n");
    printf("Tabelas: \n");

    for(int i = 0; i < c.numberOfTables; i++)
    {
        printf("[%s] ", c.tables[i]);
    }

    printf("\n\n");
    printf("Condicoes: \n");

    for(int i = 0; i < c.numberOfConditions; i++)
    {
        for(int j = 0; j < 3; j++)
            printf("[%s] ", c.conditions[i][j]);
    }

    printf("\n\n");

}

void consultaInsert_print(consultaInsert c){

    printf("Print do Insert: \n\n");
    printf("Tabela: \n");
    printf("[%s] ", c.table);
    printf("\n\n");

    printf("Valores: \n");
    for(int i = 0; i < c.numberOfValues; i++)
    {
        printf("[%s] ", c.values[i]);
    }
    printf("\n\n");

}

STATUS_DB parse_sql_select(consultaSelect** ret, char* sql)
{

    consultaSelect* resultado = calloc(1,sizeof(consultaSelect));
    for(int i = 0; i < MAX_ATTRIBUTES_MATCH; i++)
        resultado->attributes[i] = NULL;
    resultado->numberOfAttributes = 0;
    for(int i = 0; i < MAX_CONDITIONS_MATCH; i++)
    {
        for(int j = 0; j < 3; j++)
            resultado->conditions[i][j] = NULL;
    }
    for(int i=0; i<MAX_CONDITIONS_MATCH-1; i++)
        resultado->operators[i] = NULL;
    resultado->numberOfConditions = 0;
    for(int i = 0; i < MAX_TABLES_MATCH; i++)
        resultado->tables[i] = NULL;
    resultado->numberOfTables = 0;

    regex_t atributosPatern;
    regcomp(&atributosPatern,"\\s?(\\s)([a-zA-Z0-9_\\.\\*\\-]+)?((\\,\\s)([a-zA-Z0-9_\\.\\*\\-]+))*.", REG_EXTENDED);
    char* atributos = NULL;
    int matchAtributos = get_match(&atributosPatern, sql, &atributos, 0);
    if(matchAtributos == REG_NOERROR)
    {
        char* atributosTratados = substring(atributos,1,strlen(atributos),' ');
        List* temp = str_split(atributosTratados,',');
        if(temp->listSize >= MAX_ATTRIBUTES_MATCH)
        {
            printf("Failed to parser select, too much attributes.\n");
        }
        else
        {
            for(int i = 0; i < temp->listSize; i++)
            {
                if(strlen(temp->values[i]) >= MAX_STRING_SIZE)
                {
                    printf("At parse_sql_select, attribute %s name too long\n", temp->values[i]);
                    free(temp->values);
                    free(temp);
                    free(atributosTratados);
                    regfree(&atributosPatern);
                    free(atributos);
                    consultaSelect_free(resultado);
                    return -1;
                }
                resultado->attributes[i] = temp->values[i];
            }
            resultado->numberOfAttributes = temp->listSize;
        }

        free(temp->values);
        free(temp);
        free(atributosTratados);
    }
    regfree(&atributosPatern);
    free(atributos);


    char *sqlTratado;
    sqlTratado = strcasestr(sql,"FROM");
    if(sqlTratado == NULL)
    {
        printf("Invalid sql\n");
    }
    int position = sqlTratado - sql;
    char *novoSql = substring(sql,position,strlen(sql),'"');
    regex_t tabelasPattern;
    regcomp(&tabelasPattern,"(\\s[a-zA-Z0-9\\.\\-\\_\\:]+)((\\,\\s)([a-zA-Z0-9\\.\\-\\_\\:]+))*.", REG_EXTENDED);
    char* tabelas = NULL;
    int matchTabelas = get_match(&tabelasPattern, novoSql, &tabelas, 0);
    if(matchTabelas == REG_NOERROR)
    {
        char* tabelasTratadas = substring(tabelas,1,strlen(tabelas),' ');
        List* temp = str_split(tabelasTratadas,',');
        if(temp->listSize >= MAX_TABLES_MATCH)
        {
            printf("Failed to parser select, too much tables.\n");
        }
        else
        {
            for(int i = 0; i < temp->listSize; i++)
            {
                if(strlen(temp->values[i]) >= MAX_STRING_SIZE)
                {
                    printf("At parse_sql_select, table %s too long\n", temp->values[i]);
                    free(temp->values);
                    free(temp);
                    free(tabelasTratadas);
                    regfree(&tabelasPattern);
                    free(tabelas);
                    free(novoSql);
                    consultaSelect_free(resultado);
                    return -1;
                }
                resultado->tables[i] = temp->values[i];
            }
            resultado->numberOfTables = temp->listSize;
        }

        free(temp->values);
        free(temp);
        free(tabelasTratadas);
    }
    regfree(&tabelasPattern);
    free(novoSql);
    free(tabelas);



// Conditions

    // Match between

    sqlTratado = strcasestr(sql,"WHERE");
    if(sqlTratado == NULL)
    {
        *ret = resultado;
        return 0;
    }
    position = sqlTratado - sql;
    novoSql = substring(sql,position + strlen("WHERE"),strlen(sql),'"');

    regex_t betweenPattern;
    regcomp(&betweenPattern,"([a-zA-Z0-9\\_\\.\\-]+(\\s?\\s[b,B][e,E][t,T][w,W][e,E][e,E][n,N]\\s?\\s)([0-9\\,\\/:' \\-]+)(\\s?\\sand\\s?\\s([0-9\\,\\/:' \\-]+)))", REG_EXTENDED);
    char* condicoes = NULL;
    int matchCondicoes = get_match(&betweenPattern, novoSql, &condicoes, 0);
    char* sqlConditions = NULL;

    // Se não tiver between
    if(matchCondicoes == REG_NOMATCH)
    {
        int size = strlen(novoSql);
        sqlConditions = malloc(sizeof(char)*(size + 1));
        strncpy(sqlConditions, novoSql, size);
        sqlConditions[size] = '\0';
    }
    else if(matchCondicoes == REG_NOERROR)
    {
        char *between;
        between = strcasestr(novoSql,condicoes);
        int positionInicio = between - novoSql;
        int positionFim = positionInicio + strlen(condicoes);
        sqlConditions = substring(novoSql,0, positionInicio,'"');


        List* temp = str_split_strings(condicoes);
        for(int i = 0; i < temp->listSize; i++)
        {
            if(strncasecmp(temp->values[i], "Between", strlen("Between")) == 0)
            {
                stringAppend(&sqlConditions, temp->values[i-1]); // Atributo
                stringAppend(&sqlConditions, " >= "); //
                stringAppend(&sqlConditions, temp->values[i+1]); // value1
                stringAppend(&sqlConditions, " and "); //
                stringAppend(&sqlConditions, temp->values[i-1]); // Atributo
                stringAppend(&sqlConditions, " <= "); //
                stringAppend(&sqlConditions, temp->values[i+3]); // value2
                i += 3;
            }

        }
        char* semBetween = substring(novoSql, positionFim, strlen(sql), '"');
        stringAppend(&sqlConditions, semBetween);
        free(semBetween);


        for(int i = 0; i < temp->listSize; i++)
            free(temp->values[i]);
        free(temp->values);
        free(temp);

    }
    free(condicoes);
    regfree(&betweenPattern);
    free(novoSql);

    List* temp = str_split_strings(sqlConditions);
    List* newTemp = NULL;
    for(int i = 0; i < temp->listSize; i++)
    {
        char* newString = str_between_delimiter(temp->values[i], '\'');

        // Se nao for string, então é numero e newstring == NULL
        if(newString == NULL)
            listAppend(&newTemp, temp->values[i]);
        else
        {
            listAppend(&newTemp, newString);
            free(newString);
        }

    }
    freeList(temp);

    if(newTemp->listSize/3 >= MAX_CONDITIONS_MATCH)
    {
        printf("Failed to parser select, too much tables.\n");
    }
    else
    {
        int operator=0;
        for(int i = 0; i < newTemp->listSize; i+=4)
        {
            if(strlen(newTemp->values[i]) >= MAX_STRING_SIZE)
            {
                printf("At parse_sql_select, condition %s too long\n", newTemp->values[i]);
                free(newTemp->values);
                free(newTemp);
                consultaSelect_free(resultado);
                return -1;
            }
            for(int j = 0; j < 3; j++)
            {
                resultado->conditions[i/4][j] = newTemp->values[i + j];
            }
            if(i > 0)
            {
                resultado->operators[operator] = newTemp->values[i-1];
                operator++;
            }
        }
        resultado->numberOfConditions = (newTemp->listSize + 1)/4;
    }

    free(newTemp->values);
    free(newTemp);
    free(sqlConditions);



    *ret = resultado;
    return 0;

}

int consultaSelect_free(consultaSelect* consulta)
{
    if(consulta == NULL)
        return -1;
    for(int i = 0; i < consulta->numberOfAttributes; i++)
    {
        if(consulta->attributes[i] != NULL)
        {
            free(consulta->attributes[i]);
            consulta->attributes[i] = NULL;
        }
    }
    for(int i = 0; i < consulta->numberOfConditions; i++)
    {
        if(consulta->conditions[i] != NULL)
        {
            for(int j = 0; j < 3; j++)
            {
                free(consulta->conditions[i][j]);
                consulta->conditions[i][j] = NULL;
            }

        }
    }
    for(int i = 0; i < consulta->numberOfConditions-1; i++)
    {
        if(consulta->conditions[i] != NULL)
        {
            free(consulta->operators[i]);
        }
    }

    for(int i = 0; i < consulta->numberOfTables; i++)
    {
        if(consulta->tables[i] != NULL)
        {
            free(consulta->tables[i]);
            consulta->tables[i] = NULL;
        }
    }

    free(consulta);
    return 0;
}

STATUS_DB parse_sql_insert(consultaInsert** ret, char* sql)
{

    consultaInsert* resultado = calloc(1, sizeof(consultaInsert));
    resultado->table = NULL;
    resultado->values = NULL;
    resultado->numberOfValues = 0;

    regex_t tabelasPatern;
    regcomp(&tabelasPatern,"([i,I][n,N][t,T][o,O].*)\\s?(\\s)([a-zA-Z0-9\\.\\-\\_\\:]+)?(\\s)", REG_EXTENDED);
    char* tabelas = NULL;
    int matchTabelas = get_match(&tabelasPatern, sql, &tabelas, 0);
    if(matchTabelas == REG_NOERROR)
    {
        List* temp = str_split(tabelas,' ');
        if(temp->listSize >= 1)
        {
            int stringSize = strlen(temp->values[1])+ 1;
            resultado->table = malloc(sizeof(char)*stringSize);
            strncpy(resultado->table, temp->values[1], stringSize);
        }
        for(int i = 0; i < temp->listSize; i++)
        {
            free(temp->values[i]);
        }
        free(temp->values);
        free(temp);
    }
    regfree(&tabelasPatern);
    free(tabelas);

    regex_t valoresPatern;
    regcomp(&valoresPatern,"(\\()(.*?)(\\))", REG_EXTENDED);
    char* valores = NULL;
    int matchValores = get_match(&valoresPatern, sql, &valores, 0);
    if(matchValores == REG_NOERROR)
    {
        char* valoresInsert = substring(valores, 1, strlen(valores), '\)');
        char* valores_sem_espacos = eliminate_space_not_string(valoresInsert);
        List* temp = str_split(valores_sem_espacos, '\'');
        List* semVirgulas = NULL;
        for(int i = 0; i < temp->listSize; i++)
        {
            List* tmp = str_split(temp->values[i], ',');
            if(tmp == NULL)
                continue;
            for(int i = 0; i < tmp->listSize; i++)
                listAppend(&semVirgulas, tmp->values[i]);
            freeList(tmp);
        }

        free(valores_sem_espacos);
        free(valoresInsert);
        freeList(temp);


        if(semVirgulas != NULL)
        {
            resultado->numberOfValues = semVirgulas->listSize;
            resultado->values = semVirgulas->values;
            free(semVirgulas);
        }
    }
    regfree(&valoresPatern);
    free(valores);

    *ret = resultado;
    return 0;

}

int consultaInsert_free(consultaInsert* consulta)
{
    if(consulta == NULL)
        return -1;
    for(int i = 0; i < consulta->numberOfValues; i++)
    {
        if(consulta->values != NULL)
        {
            free(consulta->values[i]);
            consulta->values[i] = NULL;
        }
    }
    free(consulta->values);

    if(consulta->table != NULL)
        free(consulta->table);

    free(consulta);

    return 0;
}


STATUS_DB parse_sql_create_table(Table** ret, char* sql)
{

    Table* table;
    table = calloc(1,sizeof(Table));

    // Nome da tabela
    regex_t tabelasPatern;
    regcomp(&tabelasPatern,"[t,T][a,A][b,B][l,L][e,E]((\\s+)[`a-zA-Z0-9\\.\\-\\_\\:]+)", REG_EXTENDED);
    char* tabela = NULL;
    int matchTabela = get_match(&tabelasPatern, sql, &tabela, 0);
    if(matchTabela == REG_NOERROR)
    {

        List* temp = str_split(tabela,' ');
        int stringSize = strlen(temp->values[1])+ 1;
        table->name = malloc(sizeof(char)*stringSize);
        strncpy(table->name, temp->values[1], stringSize);
        for(int i = 0; i < temp->listSize; i++)
        {
            free(temp->values[i]);
        }
        free(temp->values);
        free(temp);
    }
    regfree(&tabelasPatern);
    free(tabela);

     // Foreign Key
    regex_t foreignPatern;
    regcomp(&foreignPatern,"[f,F][o,O][r,R][e,E][i,I][g,G][n,N]\\s[k,K][e,E][y,Y]\\s*?\\((.+?)\\)", REG_EXTENDED);
    char* foreignKeys = NULL;
    int matchForeignKeys = get_match(&foreignPatern, sql, &foreignKeys, 1);
    if(matchForeignKeys == REG_NOERROR)
    {
        char* semParenteses = substring(foreignKeys,0, strlen(foreignKeys),')');


        char* keysTratadas = substring(semParenteses,0,strlen(semParenteses),' ');
        List* temp = str_split(keysTratadas,',');
        table->foreignKeys = temp->values;
        table->numberOfForeignKeys = temp->listSize;

        free(temp);
        free(keysTratadas);
        free(semParenteses);
    }
    regfree(&foreignPatern);
    free(foreignKeys);


    // Primary Key
    char *sqlFim, *sqlInicio;
    sqlFim = strcasestr(sql,"FOREIGN KEY");
    sqlInicio = strcasestr(sql,"PRIMARY KEY");
    int positionInicio = sqlInicio - sql;
    int positionFim = 0;
    if(sqlFim != NULL)
        positionFim = sqlFim - sql;
    else
        positionFim = strlen(sql);
    char *novoSql = substring(sql,positionInicio,positionFim,'"');
    regex_t keyPatern;
    regcomp(&keyPatern,"[p,P][r,R][i,I][m,M][a,A][r,R][y,Y]\\s[k,K][e,E][y,Y]\\s*?\\((.+?)\\)", REG_EXTENDED);
    char* keys = NULL;
    int matchKeys = get_match(&keyPatern, novoSql, &keys, 1);
    if(matchKeys == REG_NOERROR)
    {
        char* semParenteses = substring(keys,0, strlen(keys),')');
        char* keysTratadas = substring(semParenteses,0,strlen(keys),' ');
        List* temp = str_split(keysTratadas,',');
        table->primaryKeys = temp->values;
        table->numberOfPrimaryKeys = temp->listSize;

        free(temp);
        free(keysTratadas);
        free(semParenteses);
    }
    regfree(&keyPatern);
    free(novoSql);

    free(keys);


    // Atributos
    sqlFim = strcasestr(sql,"PRIMARY KEY");
    positionFim = sqlFim - sql;
    novoSql = substring(sql,0,positionFim-1,'"');
    regex_t attrPatern;
    regcomp(&attrPatern,"\\(.*,?", REG_EXTENDED);
    char* attributes = NULL;
    int matchAttributes = get_match(&attrPatern, novoSql, &attributes, 0);
    if(matchAttributes == REG_NOERROR)
    {
        int init = 1;
        int end = 1;
        int notInit = 1;
        for(int i = 1; i < strlen(attributes); i++)
        {
            if(attributes[i] == ' ')
            {
                if(notInit == 1)
                    init++;
            }
            else
            {
                notInit = 0;
                end = i;
            }

        }
        char* atributosTratados = substring(attributes, init, end+1,'\'');
        List* atributos = str_split(atributosTratados,',');

        regex_t valuesPatern;
        regcomp(&valuesPatern,"([a-zA-Z0-9\\*_\\.\\-`]+)(\\s*)([a-zA-Z0-9\\*\\.\\-\\(\\)]+)?", REG_EXTENDED);

        table->numberOfAttributes = atributos->listSize;
        table->attributes = (char**)calloc(table->numberOfAttributes,sizeof(char*));
        table->attributeType = (char**)calloc(table->numberOfAttributes, sizeof(char*));
        table->attributeNull = (int*)calloc(table->numberOfAttributes, sizeof(int));
        table->attributeOperation = (int*)calloc(table->numberOfAttributes, sizeof(int));


        table->numberOfAttributes = 0;
        for(int i = 0; i < atributos->listSize; i++)
        {
            table->numberOfAttributes++;
            char* atributo = NULL;
            char* tipo = NULL;
            int matchAtributo = get_match(&valuesPatern, atributos->values[i], &atributo,1);
            if(matchAtributo == REG_NOERROR)
            {
                table->attributes[i] = atributo;
            }
            else
            {
                table->numberOfAttributes--;
            }

            int matchTipo = get_match(&valuesPatern, atributos->values[i], &tipo,3);
            if(matchTipo == REG_NOERROR && tipo != NULL)
            {
                table->attributeType[i] = tipo;
            }
            else if((matchTipo == REG_NOERROR) && (tipo == NULL))
            {
                int stringSize;
                // Usa o tipo padrão
                char* defaultType = "VARCHAR(30)";
                stringSize = strlen(defaultType) + 1;
                table->attributeType[i] = malloc(sizeof(char)*stringSize);
                strncpy(table->attributeType[i], defaultType, stringSize);
            }
            //Para evitar refazer tudo, fiz essa gambiarra para pegar o "NOT NULL" e o "AUTO_INCREMENT"



            int parametersInit = 0;
            if(tipo != NULL)
            {
                char* otherParameters = strcasestr(atributos->values[i], tipo);
                parametersInit = otherParameters - atributos->values[i];
                parametersInit += strlen(tipo) + 1;
            }
            else
            {
                char* otherParameters = strcasestr(atributos->values[i], atributo);
                parametersInit = otherParameters - atributos->values[i];
                parametersInit += strlen(atributo) + 1;
            }

            int nSpaces = 0;
            int isNull = 0;
            int isSpecial = 0;
            int strStart = parametersInit;
            int strEnd = 0;
            int strToCopy = 0;

            for(int j = parametersInit; j < strlen(atributos->values[i]) - parametersInit; j++)
            {
                strEnd = j;
                if(atributos->values[i][j] == ' ')
                {
                    nSpaces++;
                }

                if(nSpaces == 0 || nSpaces == 1)
                {
                    if(strToCopy == 0)
                    {
                        isNull = 1;
                        strToCopy = 1;
                        strStart = j + 1;
                    }
                }

                if(nSpaces == 2 && isNull == 1)
                {
                    char* str = malloc(strEnd - strStart + 1);
                    strncpy(str, atributos->values[i] + strStart, strEnd - strStart);

                    if(strncasecmp("NOT NULL", atributos->values[i] + strStart, strEnd - strStart) == 0)
                    {
                        table->attributeNull[i] = (int)NOT_NULL;
                    }
                    else if(strncasecmp("DEFAULT NULL", atributos->values[i] + strStart, strEnd - strStart) == 0)
                    {
                        table->attributeNull[i] = (int)DEFAULT_NULL;
                    }

                    isNull = 0;
                }
                else if(nSpaces == 2)
                {
                    if(isSpecial == 0)
                    {
                        isSpecial = 1;
                        strStart = j;
                    }
                }
            }

            if(isNull == 1)
            {
                if(strncasecmp("NOT NULL", atributos->values[i] + strStart, strEnd - strStart) == 0)
                {
                    table->attributeNull[i] = (int)NOT_NULL;
                }
                else if(strncasecmp("DEFAULT NULL", atributos->values[i] + strStart, strEnd - strStart) == 0)
                {
                    table->attributeNull[i] = (int)DEFAULT_NULL;
                }
            }
            else if(isSpecial)
            {
                if(strncasecmp("AUTO_INCREMENT", atributos->values[i] + strStart, strEnd - strStart) == 0)
                {
                    table->attributeOperation[i] = (int)AUTO_INCREMENT;
                }
                else
                    table->attributeOperation[i] = (int)NO_OPERATION;
            }
        }
        free(atributosTratados);
        freeList(atributos);
        regfree(&valuesPatern);
    }

    if(table->numberOfPrimaryKeys > 0)
        table->indexPrimaryKeys = malloc(sizeof(int)*table->numberOfPrimaryKeys);
    int foundPK = 0;
    for(int i = 0; i < table->numberOfPrimaryKeys; i++)
    {
        foundPK = 0;
        for(int j = 0; j < table->numberOfAttributes; j++)
        {
            if(strcmp(table->primaryKeys[i], table->attributes[j]) == 0)
            {
                table->indexPrimaryKeys[i] = j;
                foundPK = 1;
                break;
            }
        }
        if(!foundPK)
        {
            printf("Invalid Primary Key, %s is not an attribute.\n", table->primaryKeys[i]);
            freeTable(table);
            regfree(&attrPatern);
            free(novoSql);
            free(attributes);
            return -1;
        }
    }

    if(table->numberOfForeignKeys > 0)
        table->indexforeignKeys = malloc(sizeof(int)*table->numberOfForeignKeys);
    int foundFK = 0;
    for(int i = 0; i < table->numberOfForeignKeys; i++)
    {
        foundFK = 0;
        for(int j = 0; j < table->numberOfAttributes; j++)
        {
            if(strcmp(table->foreignKeys[i], table->attributes[j]) == 0)
            {
                table->indexforeignKeys[i] = j;
                foundFK = 1;
            }
        }
        if(!foundFK)
        {
            printf("Invalid Foreign Key, %s is not an attribute.\n", table->foreignKeys[i]);
            freeTable(table);
            regfree(&attrPatern);
            free(novoSql);
            free(attributes);
            return -1;
        }
    }

    regfree(&attrPatern);
    free(novoSql);
    free(attributes);


    *ret = table;
    return 0;

}

STATUS_DB check_type(int* ret, char* sql)
{
    regex_t sqlPattern;
    regcomp(&sqlPattern,"(\\s*)?([a-zA-Z0-9]+)?", REG_EXTENDED);

    char* word = NULL;
    int type = -1;
    int matchOperation = get_match(&sqlPattern, sql, &word, 0);
    if(matchOperation == REG_NOERROR)
    {
        char* operation = substring(word,0,strlen(word),' ');

        if(strcasecmp(operation, "SELECT") == 0)
        {
            type = SQL_SELECT;
        }
        else if(strcasecmp(operation, "CREATE") == 0)
        {
            char* secondWord = NULL;
            int matchOperation2 = get_match(&sqlPattern, sql + strlen(word), &secondWord, 0);
            if(matchOperation2 == REG_NOERROR)
            {
                char* operation2 = substring(secondWord,0, strlen(secondWord), ' ');
                if(strcasecmp(operation2, "TABLE") == 0)
                    type = SQL_CREATE_TABLE;
                free(operation2);
            }
            free(secondWord);

        }
        else if(strcasecmp(operation, "INSERT") == 0)
        {
            type = SQL_INSERT;
        }
        else if(strcasecmp(operation, "DELETE") == 0)
        {
            type = SQL_DELETE;
        }
        else
            type = SQL_NO_MATCH;
        free(operation);
    }
    regfree(&sqlPattern);
    free(word);

    *ret = type;
    return ;
}
