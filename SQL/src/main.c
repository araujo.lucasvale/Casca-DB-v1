#include "db.h"

// Para testar
#include "csv_parser.h"
#include <string.h>

// Para benchmark
#include <time.h>
#include <math.h>

#define MEASURE

int main(int argc, char** argv)
{

    DBConnection* connection = NULL;;

    RESTRequest* request = NULL;
    restRequest_init(&request);

    if(argc < 5)
    {
        printf("Invalid number of parameters.\n");
        return;
    }
    char* url = argv[1];
    restRequest_setUrl(&request, url);
    restRequest_appendHeader(&request, argv[2]);
    restRequest_appendHeader(&request, argv[3]);
    db_connectRest(&connection, argv[4], request);

/*
    char* url = "https://sgx1.chocolate-cloud.cc/kvs/v3/datastores/consumption/";
    request = restRequest_setUrl(request, url);
    request = restRequest_appendHeader(request, "Authorization: ApiKey 0131Byd7N220T32qp088kIT53ryT113i");
    request = restRequest_appendHeader(request, "Content-Type: application/json");

    connection = db_connectRest("../database.db", request);
*/


#ifdef MEASURE
    if(strncmp(argv[5], "delete", strlen("delete")) == 0)
    {
        deleteKeys(connection);
        db_disconnect(connection);
        return 0;
    }
#endif // MEASURE
    char* buffer;

    query(connection, argv[5], buffer);


    db_disconnect(connection);
    //restRequest_free(request);


    return 0;
}

void deleteKeys(DBConnection* connection)
{

    Results* query_results;

    sql_select(&query_results, connection, "SELECT * FROM fasor_aneel_inmetro");



    for(int i = 0; i < query_results->results->numberOfRows; i++)
    {
        char* key = NULL;
        stringConcat(&key, "", query_results->results->tableName);
        for(int j = 0; j < query_results->results->numberOfPrimaryKeys; j++)
        {
            int pkindex = query_results->results->primaryKeysIndex[j];

            stringAppend(&key, ";");
            stringAppend(&key, query_results->results->rows[i]->values[pkindex]);
        }

        sql_delete(connection, key);
        free(key);
    }

}
