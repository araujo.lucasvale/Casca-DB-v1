/**
 * @file
 * @author Davi Boberg
 * @date 24/01/2018
 *
 * \brief Manage requisition and connection informations.
 *
 * Datailed explanation.
 *
 * @todo Put schemas on the real database, not local.
 */


#ifndef DB_H
#define DB_H

#include "dic_dados.h"
#include "sql_query.h"
#include "json_parser.h"


/**
 * @brief Load database schema and set requisition parameters.
 *
 *
 * @author Davi Boberg
 * @date 28/2/2009
 *
 * @param ret           Struct with all the parameters necessary to make a requisition.
 * @param localDataBase Local schema file name.
 * @param ret           REST requisition parameters.
 */
int db_connectRest(DBConnection** ret, char* localDataBase, RESTRequest* request);


/**
 * @brief Free database schema and requisition parameters.
 *
 *
 * @author Davi Boberg
 * @date 28/2/2009
 *
 * @param database DBConnection to be freed.
 */
int db_disconnect(DBConnection* database);

/**
 * @brief Set a new REST requisition parameters to DBConnection.
 *
 *
 * @author Davi Boberg
 * @date 28/2/2009
 *
 * @param ret           DBConnection with new request parameters.
 * @param connection    Current connection.
 * @param  newRequest   Request parameters to be set.
 */
int db_setRestRequest(DBConnection** ret, DBConnection* connection, RESTRequest* newRequest);

/**
 * @brief Load all database schemas.
 *
 *
 * @author Davi Boberg
 * @date 28/2/2009
 *
 * @param ret               DBConnection with new schemas loaded.
 * @param localDataBase     Database file name.
 * @param request           .
 */
int retrieveDataBase(DataBase** ret, char* localDataBase, RESTRequest* request);



#endif /* DB_H */

