#include "sql_query.h"

//#define PRINT_TABLES
//#define PRINT_RESULTS
//#define PRINT_EXECUTING

void query(DBConnection* connection, char* sql, char * buf)
{

#ifdef PRINT_EXECUTING
    printf("\n\nExecuting: %s \n", sql);
#endif // PRINT_EXECUTING

    int type; 
    check_type(&type,sql);
    Results* query_results;

    if(type == SQL_SELECT)
    {
        sql_select(&query_results, connection, sql);
    }
    else if(type == SQL_CREATE_TABLE)
    {
        sql_createTable(&query_results, connection, sql);
    }
    else if(type == SQL_INSERT)
    {
        sql_insert(&query_results, connection, sql);
    }
    // Para remover do banco
    /*
    else if(type == SQL_DELETE)
    {
        sql_delete(connection, sql);
    }
    */

    char* results = NULL;

    // O resultado do select esta em select_result
    if(query_results != NULL)
    {
        JSONFromResults(&results, query_results);
        results_free(query_results);
        query_results = NULL;
    }


#ifdef PRINT_RESULTS

    if(results == NULL)
    {
        printf("\nFailed to generate results.\n");
    }
    else
        printf(results);

#endif //PRINT_RESULTS_INSERT

    memcpy(buf,results,strlen(results));

    if(query_results != NULL)
        free(results);

    db_disconnect( connection);

}

/*
void nQueries(DBConnection* connection, char** sql, int n)
{
    for(int i = 0; i < n; i++)
    {
        query(connection, sql[i]);
    }
}
*/

