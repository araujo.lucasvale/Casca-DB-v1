#ifndef __SOCKET__
#define __SOCKET__

#include "data_structs.h"
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <netdb.h>
#include <arpa/inet.h>
#include <stdio.h>
#include <syslog.h>
#include <pthread.h>
/*
Essa funcao verifica se o socket eh do tipo input, output ou listen, e realiza uma tarefa  se necessario
O parametro de entrada é um info_socket_t
*/
void
check_socket(info_socket_t * socket_info, socket_type_t type);

/*
Essa funcao eh responsavel por inicializar o socket
*/
int
socket_init(info_socket_t * socket_info, int port);


#endif